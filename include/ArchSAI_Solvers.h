 /*! \file ArchSAI_Solvers.h
    \brief This file contains the solvers implemented by ArchSAI.

    ArchSAI solvers.
*/

 #ifndef ARCHSAI_SOLVERs_h_
    #define ARCHSAI_SOLVERs_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_LinAlg.h>
    #include <ArchSAI_ApplyPC.h>
    #include <ArchSAI_Print.h>

    extern const ARCHSAI_DOUBLE fp_one;
    extern const ARCHSAI_DOUBLE fp_none;
    extern const ARCHSAI_DOUBLE fp_zero;

    extern const ARCHSAI_INT   int_zero;
    extern const ARCHSAI_INT    int_one;

    extern ARCHSAI_CHAR *trans;
    extern ARCHSAI_CHAR *matdescra;

    /*! \fn ARCHSAI_VEC ArchSAI_SolveLinearSystem(ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_HANDLER *handler);
        \brief Solves a linear system according to parameters

        \param A Input matrix
        \param b RHS
        \param PC Preconditioner
        \param handler ArchSAI handler
    */
    ARCHSAI_VEC ArchSAI_SolveLinearSystem(ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_HANDLER *handler);

    /*! \fn ARCHSAI_SOLVER ArchSAI_InitSolver(ARCHSAI_PARAMS params, ARCHSAI_MATCSR mat, ARCHSAI_PRECOND pc);
        \brief Initializes solver structure to store execution data.

        \param params ArchSAI set of parameters
        \param A Input matrix
        \param PC Preconditioner
    */
    ARCHSAI_SOLVER ArchSAI_InitSolver(ARCHSAI_PARAMS params, ARCHSAI_MATCSR mat, ARCHSAI_PRECOND pc);

    /*! \fn ARCHSAI_VEC ArchSAI_GMRES(ARCHSAI_SOLVER *solver, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);
        \brief Solves a linear system using GMRES(solver_aux)

        \param solver Pointer to solver structure. Stores solver data.
        \param A Input matrix
        \param b RHS
        \param PC Preconditioner
        \param params ArchSAI set of parameters
    */
    ARCHSAI_VEC ArchSAI_GMRES(ARCHSAI_SOLVER *solver, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_VEC ArchSAI_GMRES(ARCHSAI_SOLVER *solver, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);
        \brief Solves a linear system PCG

        \param solver Pointer to solver structure. Stores solver data.
        \param A Input matrix
        \param b RHS
        \param PC Preconditioner
        \param params ArchSAI set of parameters
    */
    ARCHSAI_VEC ArchSAI_PCG(ARCHSAI_SOLVER *GMRES, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);

	/*! \fn ARCHSAI_VEC ArchSAI_PCG_gpu(ARCHSAI_SOLVER *PCG, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);
		\brief Solves a linear system PCG

		\param solver Pointer to solver structure. Stores solver data.
		\param A Input matrix
		\param b RHS
		\param PC Preconditioner
		\param params ArchSAI set of parameters
	*/
	ARCHSAI_VEC ArchSAI_PCG_gpu(ARCHSAI_SOLVER *PCG, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params);

#endif // __ARCHSAI_SOLVERs_h__



