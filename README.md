# ArchSAI
Main repository of the ArchSAI project. ArchSAI is a library that implements architecture-aware pattern extensions for SAI preconditioners. ArchSAI also implements Linear System solvers to test the preconditioners.

## Description
ArchSAI implements architecture-aware pattern extensions for SAI preconditioners. ArchSAI also implements Linear System solvers to test the preconditioners.

Preconditioners:
- [x] SAI
- [x] FSAI
- [x] Architecture-Aware SAI
- [x] Architecture-Aware FSAI
- [x] Communication-Aware SAI
- [x] Communication-Aware FSAI
- [ ] GPU-Friendly SAI
- [ ] GPU-Friendly FSAI

Solvers:
- [x] PCG
- [x] GMRES

Context:
- [x] CPU
- [x] CPU Distributed memory
- [ ] GPU

## Prerequisites
For ArchSAI:

- cmake 3.13++

Useful libraries:

- mkl, lapack, blas

## Installation

### Building ArchSAI

To build ArchSAI, you can use the standard CMake procedure.

```sh
mkdir build; cd build
cmake .. && make all
```

By default ArchSAI is built with the best configuration for the available architecture. It builds the ArchSAI application (appArchSAI), which implements a Linear System solver to test cases.

To compile documentation use:

```sh
make docs
```

#### Building Options

Chech CMakeLists Configuration options:

* SINGLE/DOUBLE precision
* VERBOSITY
* TIMING
* BLAS/LAPACK libraries
* APP
* SHARED/STATIC lib


### Using ArchSAI application

After building and compiling ArchSAI the application is also compiled.
To run use from your build path:

```sh
./apps/src/appArchSAI <arg>
mpirun -np x /apps/src/appArchSAI <arg>
```

appArchSAI accepts several arguments, <arg> in the form: --arg=val

List of arguments:


*   _mat_ To specify path to a .mtx file storing a matrix.
*   _rhs_ To specify path to a .mtx file storing a RHS.

*   _solver_ Solver to use. Values:
    * _0_ No solver
    * _1_ GMRES
    * _3_ PCG
    * Default _1_

*   _solver_aux_ Auxiliary value for solver. For _GMRES_ it is used for restart.
    * Default _100_

*   _pc_ Preconditioner to use. Values:
    * _0_ No preconditioner
    * _1_ SAI
    * _2_ FSAI
    * Default _0_

*   _pattern_ Pattern over which preconditioner is computed. Values:
    * _0_ Base input matrix pattern
    * _1_ Power L of input matrix
    * Default _0_

*   _pattern_aux_ Auxiliary pattern value. For pattern=1 it is used for power level L:
    * Default _0_

*   _extension_ Extension to be performed to the pattern. Values:
    * _0_ No extension
    * _1_ Cache-Aware and Communication-Aware extension.
    * _2_ GPU-friendly (not impl)
    * Default _0_

*   _spat_ Auxiliary extension value. Corresponds to the spatial extension size. Should be adapted to Cache/Warp sizes:
    * Default _1_

*   _temp_ Auxiliary extension value. Corresponds to the temporal extension size. Should be adapted to Cache/Warp sizes:
    * Default _1_

*   _filter_ Value for filtering-out. Typical values: 0.1-0.5
    * Default _0.0_

*   _filter_sel_ Type of filtering-out method. Values:
    * _0_ No filtering-out of pattern extension
    * _1_ Filtering-out of added entries compared to maximum absolute value in inverse precomputation. Does not filter initial pattern entries.
    * _2_ Filtering-out of added entries compared to maximum absolute value in inverse precomputation. Filters initial pattern entries.
    * _3_ Scale-independent filter of added entries with respect from diagonal. Does not filter initial pattern entries.
    * _4_ Scale-independent filter of added entries with respect from diagonal. Filters initial pattern entries.
    * Default _0_

Solver inputs:

*   _imax_ Maximum amount of iterations. Default 10000.
*   _reps_ Solver repetitions. Default 1.
*   _rel_tol_ Relative tolerance for convergence: Default 1e-08.
*   _abs_tol_ Absolute tolerance for convergence: Default 0.


## Authors and acknowledgment
This project is part of the PhD research of Sergi Laut Turon (sergi.lautturon@bsc.es), which is supervised by Marc Casas Guix and Ricard Borrell Pol.


## Project status
This project is under development. Some features may not yet be implemented or may be incomplete.

<!--## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.bsc.es/SLaTuBSC/fsaie.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.bsc.es/SLaTuBSC/fsaie/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.-->

<!--
## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.-->
