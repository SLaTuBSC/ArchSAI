
 /*! \file ArchSAI_Time.h
    \brief This file contains the time utilities and functions used by ArchSAI.

    ArchSAI time utilities.
*/

 #ifndef ArchSAI_Time_h_
    #define ArchSAI_Time_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_MPI.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <time.h>
    #include <unistd.h>
    #include <sys/times.h>

    #ifndef ARCHSAI_USING_MPI
        #include <omp.h>
    #else
        #include <mpi.h>
    #endif


    archsai_double time_getWallclockSeconds( void );
    archsai_double time_getCPUSeconds( void );
    archsai_double time_get_wallclock_seconds_( void );
    archsai_double time_get_cpu_seconds_( void );

    #ifndef ARCHSAI_TIMING

        #define ArchSAI_InitializeTiming(name) 0
        #define ArchSAI_FinalizeTiming(index)
        #define ArchSAI_IncFLOPCount(inc)
        #define ArchSAI_BeginTiming(i)
        #define ArchSAI_EndTiming(i)
        #define ArchSAI_ClearTiming()

        #define ArchSAI_PrintTiming(heading, comm)
        #define ArchSAI_GetTiming(heading, comm, time)
    #else

        typedef struct {
            ARCHSAI_CHAR   **name;
            archsai_double  *wall_time;
            archsai_double  *cpu_time;
            archsai_double  *flops;
            ARCHSAI_INT     *state;     /* boolean flag to allow for recursive timing */
            ARCHSAI_INT     *num_regs;  /* count of how many times a name is registered */

            ARCHSAI_INT      num_names;
            ARCHSAI_INT      size;

            archsai_double   wall_count;
            archsai_double   CPU_count;
            archsai_double   FLOP_count;
        } ArchSAI_TimingType;


        extern ArchSAI_TimingType *ArchSAI_Timing;

        /*-------------------------------------------------------
        * Accessor functions
        *-------------------------------------------------------*/

        #define ArchSAI_TimingWallTime(i) (ArchSAI_Timing -> wall_time[(i)])
        #define ArchSAI_TimingCPUTime(i)  (ArchSAI_Timing -> cpu_time[(i)])
        #define ArchSAI_TimingFLOPS(i)    (ArchSAI_Timing -> flops[(i)])
        #define ArchSAI_TimingName(i)     (ArchSAI_Timing -> name[(i)])
        #define ArchSAI_TimingState(i)    (ArchSAI_Timing -> state[(i)])
        #define ArchSAI_TimingNumRegs(i)  (ArchSAI_Timing -> num_regs[(i)])
        #define ArchSAI_TimingWallCount   (ArchSAI_Timing -> wall_count)
        #define ArchSAI_TimingCPUCount    (ArchSAI_Timing -> CPU_count)
        #define ArchSAI_TimingFLOPCount   (ArchSAI_Timing -> FLOP_count)

        ARCHSAI_INT ArchSAI_InitializeTiming( const char *name );
        ARCHSAI_INT ArchSAI_FinalizeTiming( ARCHSAI_INT time_index );
        ARCHSAI_INT ArchSAI_FinalizeAllTimings( void );
        ARCHSAI_INT ArchSAI_IncFLOPCount( ARCHSAI_BIGINT inc );
        ARCHSAI_INT ArchSAI_BeginTiming( ARCHSAI_INT time_index );
        ARCHSAI_INT ArchSAI_EndTiming( ARCHSAI_INT time_index );
        ARCHSAI_INT ArchSAI_ClearTiming( void );

        ARCHSAI_INT ArchSAI_PrintTiming( const char *heading, MPI_Comm comm );
        ARCHSAI_INT ArchSAI_GetTiming( const char *heading, archsai_double *wall_time_ptr, MPI_Comm comm );
    #endif


#endif // __ArchSAI_Time_h__



