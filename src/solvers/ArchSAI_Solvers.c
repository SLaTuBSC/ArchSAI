#include <ArchSAI_Solvers.h>


const ARCHSAI_DOUBLE fp_one = 1.0;
const ARCHSAI_DOUBLE fp_none = -1.0;
const ARCHSAI_DOUBLE fp_zero = 0.0;

const ARCHSAI_INT   int_zero = 0;
const ARCHSAI_INT    int_one = 1;

ARCHSAI_CHAR *trans = "N";
ARCHSAI_CHAR *matdescra = "GLNC";


ARCHSAI_VEC ArchSAI_SolveLinearSystem(ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_HANDLER *handler){

    #ifdef ARCHSAI_TIMING
        ARCHSAI_INT timeindex = ArchSAI_InitializeTiming("SOLVE");
        ArchSAI_BeginTiming(timeindex);
    #endif

    #ifdef ARCHSAI_VERBOSE
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);
        ArchSAI_ParPrintf(A.comm, "\n************** ArchSAI Solve *************\n");
    #endif

    ARCHSAI_VEC sol;

    switch(handler->params.sol_sel){
        /* NO SOLVER */
        case 0:
            break;
        case 1:
            sol = ArchSAI_GMRES(&(handler->solver), A, b, PC, handler->params);
            break;
        case 2:
        #ifdef ARCHSAI_USING_GPU
//             sol = ArchSAI_PCG_gpu(solver, A, b, PC, params);
        #endif
            break;
        case 3:
            sol = ArchSAI_PCG(&(handler->solver), A, b, PC, handler->params);
            break;
        case 4:
            sol = ArchSAI_PCG_gpu(&(handler->solver), A, b, PC, handler->params);
            break;
        default:
            ArchSAI_ParPrintf(A.comm, "Error: Not Implemented\n");
            ArchSAI_Error(10);
            break;
    }

    #ifdef ARCHSAI_VERBOSE
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);

    #endif // VERBOSE

    #ifdef ARCHSAI_TIMING
        ArchSAI_EndTiming(timeindex);
    #endif // TIMING

    handler->times[2] = handler->solver.time[handler->solver.bestrep];

    return sol;
}


ARCHSAI_SOLVER ArchSAI_InitSolver(ARCHSAI_PARAMS params, ARCHSAI_MATCSR mat, ARCHSAI_PRECOND pc){

    ARCHSAI_SOLVER solver;
    solver.reps = params.reps;
    solver.iter = ArchSAI_CTAlloc(ARCHSAI_INT, solver.reps, ArchSAI_MEMORY_HOST);
    solver.time = ArchSAI_CTAlloc(archsai_double, solver.reps, ArchSAI_MEMORY_HOST);
    solver.residual  = ArchSAI_TAlloc(ARCHSAI_DOUBLE*, solver.reps, ArchSAI_MEMORY_HOST);
    solver.rel_res   = ArchSAI_TAlloc(ARCHSAI_DOUBLE*, solver.reps, ArchSAI_MEMORY_HOST);
    solver.iter_time = ArchSAI_TAlloc(archsai_double*, solver.reps, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < solver.reps; i++){
        solver.residual[i] = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, params.imax + 1, ArchSAI_MEMORY_HOST);
        solver.rel_res[i] = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, params.imax + 1, ArchSAI_MEMORY_HOST);
        solver.iter_time[i] = ArchSAI_CTAlloc(archsai_double, params.imax + 1, ArchSAI_MEMORY_HOST);
    }

    // System matrix

    solver.sys_nnz = mat.gnnz;
	solver.sys_ncols = mat.gncols;
	solver.sys_nrows = mat.gnrows;
	#ifdef ARCHSAI_USING_GPU
		solver.sys_nnz = mat.d_gnnz;
		solver.sys_nrows = mat.d_gnrows;
		solver.sys_ncols = mat.d_gncols;
	#endif

	// PC

    if (pc.type == 0){
		solver.pc_nnz = 0;
		solver.pc_ncols = 0;
		solver.pc_nrows = 0;
    }
    else {
		solver.pc_nnz = M(pc).gnnz;
		solver.pc_ncols = M(pc).gncols;
		solver.pc_nrows = M(pc).gnrows;
		#ifdef ARCHSAI_USING_GPU
			solver.pc_nnz = M(pc).d_gnnz;
			solver.pc_nrows = M(pc).d_gnrows;
			solver.pc_ncols = M(pc).d_gncols;
		#endif

    }

    return solver;

}
