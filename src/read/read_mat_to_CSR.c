
#include <ArchSAI_Read.h>

ARCHSAI_MATCSR ArchSAI_ReadMTXtoMatCSR(ARCHSAI_PARAMS params, MPI_Comm context){

    ARCHSAI_INT ret_code;
    MM_typecode matcode;
    FILE *f;

    ARCHSAI_CHAR     *path = params.matname;

    ARCHSAI_MATCSR FullMat, Mat;

    ARCHSAI_INT M, N, nz;
    ARCHSAI_INT i, *col_indx, *row_indx;
    ARCHSAI_DOUBLE *dval;

    // Open File to read
    if ((f = fopen(path, "r")) == NULL) {
        ArchSAI_ErrorWithMsg(4, "Error: Could not read Matrix File. Check <PATH> in --mat=<PATH>.");
    }

    if (mm_read_banner(f, &matcode) != 0) {
        ArchSAI_ErrorWithMsg(1, "Error: Wrong MatrixMarket banner structure.");
    }

    /*  This is how one can screen matrix types if their application */
    /*  only supports a subset of the Matrix Market data types.      */

    if (mm_is_complex(matcode) /*&& mm_is_matrix(matcode) && mm_is_sparse(matcode)*/ ) {
        ArchSAI_printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
        ArchSAI_ErrorWithMsg(10, "Implementation Error: Type not supported.");
    }

    /* Find out size of sparse matrix .... */

    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
        ArchSAI_ErrorWithMsg(1, "Error: Matrix Market size and nz not specified.");

    /* Reseve memory for matrices */
    col_indx = ArchSAI_CTAlloc(ARCHSAI_INT, nz, ArchSAI_MEMORY_HOST);
    row_indx = ArchSAI_CTAlloc(ARCHSAI_INT, nz, ArchSAI_MEMORY_HOST);
    dval = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, nz, ArchSAI_MEMORY_HOST);

    /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
    /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
    /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */

    long double readval = 0.0;
    for (i=0; i<nz; i++){
        fscanf(f, "%d %d %llf\n", &col_indx[i], &row_indx[i], &readval);
        dval[i] = (ARCHSAI_DOUBLE) readval;
        col_indx[i]--;  /* adjust from 1-based to 0-based */
        row_indx[i]--;
    }

    if (f !=stdin) fclose(f);

    /************************/
    /* now write out matrix */
    /************************/

    if (mm_is_symmetric(matcode)){
        FullMat = ArchSAI_DAllocCreateSymmetricMatCSRfromCOO(col_indx, row_indx, dval, M, N, nz);
    }
    else {
        FullMat = ArchSAI_DAllocCreateMatCSRfromCOO(col_indx, row_indx, dval, M, N, nz);
    }

    ArchSAI_TFree(col_indx, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(row_indx, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(dval, ArchSAI_MEMORY_HOST);

    /* Once we have the size of the matrix, distribute it among processes in context */

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(context, &rank);
    MPI_Comm_size(context, &nprocs);

    /* We create an array that specifies which process has each row for each process */
    FullMat.rowdist = ArchSAI_CreateRowDistributionMatCSR(FullMat, params, nprocs);

    Mat = ArchSAI_AllocCreateDistMatCSRFromFullMatCSR(FullMat, context);

    ArchSAI_DestroyMatCSR(&FullMat, ArchSAI_MEMORY_HOST);

    return Mat;
}

