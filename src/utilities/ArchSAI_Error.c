
 /*! \file ArchSAI_Error.c
    \brief Source file for the error management in ArchSAI.

    ArchSAI error source file.
*/

#include "ArchSAI_Error.h"

ARCHSAI_INT archsai_global_error = 0;

void ArchSAI_ErrorHandler(const char *filename, ARCHSAI_INT line, ARCHSAI_INT ierr, const char *msg)
{
    archsai_error_flag |= ierr;

    if (msg){
        ArchSAI_fprintf(stderr, "\nArchSAI error in file \"%s\", line %d, error code = %d\n%s\n\n", filename, line, ierr, msg);}
    else{
        ArchSAI_fprintf(stderr, "\nArchSAI error in file \"%s\", line %d, error code = %d\n\n", filename, line, ierr);}

    MPI_Abort(MPI_COMM_WORLD, archsai_error_flag);
}



ARCHSAI_INT ArchSAI_GetError(void){
   return archsai_error_flag;
}



ARCHSAI_INT ArchSAI_CheckError(ARCHSAI_INT ierr, ARCHSAI_INT ArchSAI_error_code){
   return ierr & ArchSAI_error_code;
}



void ArchSAI_DescribeError(ARCHSAI_INT ierr, char *msg){
    if (ierr == 0){
        sprintf(msg, "[No error] ");
    }

    if (ierr & ARCHSAI_ERROR_GENERIC){
        sprintf(msg, "[Generic error] ");
    }

    if (ierr & ARCHSAI_ERROR_MEMORY){
        sprintf(msg, "[Memory error] ");
    }

    if (ierr & ARCHSAI_ERROR_ARG){
        sprintf(msg, "[Error in argument %d] ", ArchSAI_GetErrorArg());
    }

    if (ierr & ARCHSAI_ERROR_CONV){
        sprintf(msg, "[Method did not converge] ");
    }
}

ARCHSAI_INT ArchSAI_GetErrorArg(void){
   return (archsai_error_flag >> 3 & 31);
}

ARCHSAI_INT ArchSAI_ClearAllErrors(void){
   archsai_error_flag = 0;
   return (archsai_error_flag != 0);
}

ARCHSAI_INT ArchSAI_ClearError(ARCHSAI_INT ArchSAI_error_code){
   archsai_error_flag &= ~ArchSAI_error_code;
   return (archsai_error_flag & ArchSAI_error_code);
}


