/*! \file ArchSAI_GPULinAlg.h
    \brief This file contains the types and data structures used by GPU ArchSAI.

    ArchSAI types and structures.
*/

 #ifndef ArchSAI_GPULinAlg_h_
    #define ArchSAI_GPULinAlg_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Memory.h>

	#if defined ARCHSAI_USING_CUDA

		static cusparseOperation_t nt = CUSPARSE_OPERATION_NON_TRANSPOSE;
		static cudaDataType dtf = CUDA_R_32F;
		static cudaDataType dtd = CUDA_R_64F;
		static cusparseSpMVAlg_t alg = CUSPARSE_CSRMV_ALG1;

		#if defined ARCHSAI_SINGLE
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res); \
								cublasSdot(blashandle, size, v1, inc1, v2, inc2, res)

			#define ArchSAI_GPU_Axpy(blashandle, size, alpha, v1, inc1, v2, inc2); \
								cublasSaxpy(blashandle, size, alpha, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Copy(blashandle, size, v1, inc1, v2, inc2); \
								cublasScopy(blashandle, size, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Scal(blashandle, size, alpha, v1, inc1); \
								cublasSscal(blashandle, size, alpha, v1, inc1)

			#define ArchSAI_GPU_Csrmv(mat, alpha, x, beta, y, buffer); \
								cusparseSpMV(mat.sphandle,  nt, &alpha, mat.matspdescr, x.dndescr, &beta, y.dndescr, dtf, alg, buffer)

		#else
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res); \
								cublasDdot(blashandle, size, v1, inc1, v2, inc2, res)

			#define ArchSAI_GPU_Axpy(blashandle, size, alpha, v1, inc1, v2, inc2); \
								cublasDaxpy(blashandle, size, alpha, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Copy(blashandle, size, v1, inc1, v2, inc2); \
								cublasDcopy(blashandle, size, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Scal(blashandle, size, alpha, v1, inc1); \
								cublasDscal(blashandle, size, alpha, v1, inc1)

			#define ArchSAI_GPU_Csrmv(mat, alpha, x, beta, y, buffer); \
								cusparseSpMV(mat.sphandle,  nt, &alpha, mat.matspdescr, x.dndescr, &beta, y.dndescr, dtd, alg, buffer)

		#endif

	#elif defined ARCHSAI_USING_HIP

		#if defined ARCHSAI_SINGLE
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res)		cublasSdot(blashandle, size, v1, inc1, v2, inc2, res)
		#else
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res)		cublasDdot(blashandle, size, v1, inc1, v2, inc2, res)
		#endif

	#else

		#if defined ARCHSAI_SINGLE
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res)		archsai_GPU_sDotP(size, v1, inc1, v2, inc2, res)
			#define ArchSAI_GPU_Axpy(blashandle, size, alpha, v1, inc1, v2, inc2)	archsai_GPU_sAxpy(blashandle, size, alpha, v1, inc1, v2, inc2)
			#define ArchSAI_GPU_Copy(blashandle, size, v1, inc1, v2, inc2)			archsai_GPU_sCopy(blashandle, size, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Scal(blashandle, size, alpha, v1, inc1); \
								archsai_GPU_sScal(blashandle, size, alpha, v1, inc1)

			#define ArchSAI_GPU_Csrmv(mat, alpha, x, beta, y, buffer); \
                        archsai_GPU_sSpMV()
		#else
			#define ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, res)		archsai_GPU_dDotP(size, v1, inc1, v2, inc2, res)
			#define ArchSAI_GPU_Axpy(blashandle, size, alpha, v1, inc1, v2, inc2)	archsai_GPU_dAxpy(blashandle, size, alpha, v1, inc1, v2, inc2)
			#define ArchSAI_GPU_Copy(blashandle, size, v1, inc1, v2, inc2)			archsai_GPU_dCopy(blashandle, size, v1, inc1, v2, inc2)

			#define ArchSAI_GPU_Scal(blashandle, size, alpha, v1, inc1); \
								archsai_GPU_dScal(blashandle, size, alpha, v1, inc1)

			#define ArchSAI_GPU_Csrmv(mat, alpha, x, beta, y, buffer); \
                        archsai_GPU_dSpMV()

		#endif
	#endif


    void archsai_GPU_sDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2, ARCHSAI_FLOAT *res);
	void archsai_GPU_dDotP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, ARCHSAI_DOUBLE *res);

	void archsai_GPU_sAxpy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2);
	void archsai_GPU_dAxpy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2);

	void archsai_GPU_sCopy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2);
	void archsai_GPU_dCopy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2);

	void archsai_GPU_sScal(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1);
	void archsai_GPU_dScal(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1);

	void archsai_GPU_sSpMV();
	void archsai_GPU_dSpMV();

	void ArchSAI_GPU_CSRMV(ARCHSAI_DOUBLE alpha, ARCHSAI_MATCSR mat, ARCHSAI_VEC x, ARCHSAI_DOUBLE beta, ARCHSAI_VEC y, void *buffer, ARCHSAI_INT nprocs);
	ARCHSAI_DOUBLE ArchSAI_GPU_DOTP(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, MPI_Comm comm, ARCHSAI_INT nprocs);


#endif // __ArchSAI_GPULinAlg_h__



