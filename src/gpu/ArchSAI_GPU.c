#include <ArchSAI_GPU.h>

void ArchSAI_MatToGPU(ARCHSAI_MATCSR *mat, ARCHSAI_PARAMS params){

	#if defined ARCHSAI_USING_GPU

	ARCHSAI_INT *rowind	= NULL;
	ARCHSAI_INT *colind = NULL;
	ARCHSAI_DOUBLE *val	= NULL;

	ARCHSAI_INT mb, nb;

	cusparseMatDescr_t hlpdescr;

	#if defined ARCHSAI_USING_CUDA

	cusparseStatus_t status = CUSPARSE_STATUS_SUCCESS;
	cudaError_t         cudaStat1 = cudaSuccess;

	// CSR

	if (params.format == 0) {

		mat->gformat = 0;

		cusparseCreate(&(mat->sphandle));
		cusparseCreateMatDescr(&(mat->matdescr));
		cusparseSetMatType(mat->matdescr, CUSPARSE_MATRIX_TYPE_GENERAL);
		cusparseSetMatIndexBase(mat->matdescr, CUSPARSE_INDEX_BASE_ZERO);

		mat->d_nnz = mat->nnz;
		mat->d_nrows = mat->nrows;
		mat->d_ncols = mat->ncols;
		mat->d_gnnz = mat->gnnz;
		mat->d_gnrows = mat->gnrows;
		mat->d_gncols = mat->gncols;

		mat->matdescr 		= NULL;
		mat->d_rowind	= ArchSAI_CTAlloc(ARCHSAI_INT, mat->nrows + 1,	ArchSAI_MEMORY_DEVICE);
		mat->d_colind	= ArchSAI_CTAlloc(ARCHSAI_INT, mat->nnz,		ArchSAI_MEMORY_DEVICE);
		mat->d_val 		= ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat->nnz, 	ArchSAI_MEMORY_DEVICE);

		mat->d_spmvr	= ArchSAI_InitGPUVec(mat->d_nrows);
		mat->d_spmvc	= ArchSAI_InitGPUVec(mat->d_ncols);

		ArchSAI_Memcpy(mat->d_rowind, mat->rowind, (mat->nrows+1)*sizeof(ARCHSAI_INT), 	ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);
		ArchSAI_Memcpy(mat->d_colind, mat->colind, (mat->nnz)*sizeof(ARCHSAI_INT), 		ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);
		ArchSAI_Memcpy(mat->d_val, mat->val, (mat->nnz)*sizeof(ARCHSAI_DOUBLE), 		ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);

		(mat->commscheme)->send = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, (mat->commscheme)->sendsize,	ArchSAI_MEMORY_HOST_PINNED);
		(mat->commscheme)->recv = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, (mat->nrows + (mat->commscheme)->recvsize),	ArchSAI_MEMORY_HOST_PINNED);


		#ifdef ARCHSAI_SINGLE
		cusparseCreateCsr(&(mat->matspdescr), mat->d_nrows, mat->d_ncols, mat->d_nnz, mat->d_rowind, mat->d_colind, mat->d_val, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_BASE_ZERO, CUDA_R_32F);
		#else
		cusparseCreateCsr(&(mat->matspdescr), mat->d_nrows, mat->d_ncols, mat->d_nnz, mat->d_rowind, mat->d_colind, mat->d_val, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I, CUSPARSE_INDEX_BASE_ZERO, CUDA_R_64F);
		#endif


	}

	// BSR

	else if (params.format == 1) {

		mat->gformat = 1;
		mat->format_aux = params.format_aux;

		mat->matdescr 		= NULL;
		cusparseCreate(&(mat->sphandle));
		cusparseCreateMatDescr(&(mat->matdescr));
		cusparseSetMatType(mat->matdescr, CUSPARSE_MATRIX_TYPE_GENERAL);
		cusparseSetMatIndexBase(mat->matdescr, CUSPARSE_INDEX_BASE_ZERO);

		cusparseCreateMatDescr(&hlpdescr);
		cusparseSetMatType(hlpdescr, CUSPARSE_MATRIX_TYPE_GENERAL);
		cusparseSetMatIndexBase(hlpdescr, CUSPARSE_INDEX_BASE_ZERO);

		rowind	= ArchSAI_CTAlloc(ARCHSAI_INT, mat->nrows + 1,	ArchSAI_MEMORY_DEVICE);
		colind	= ArchSAI_CTAlloc(ARCHSAI_INT, mat->nnz,		ArchSAI_MEMORY_DEVICE);
		val 	= ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat->nnz, 	ArchSAI_MEMORY_DEVICE);

		ArchSAI_Memcpy(rowind, mat->rowind, (mat->nrows+1)*sizeof(ARCHSAI_INT), 	ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);
		ArchSAI_Memcpy(colind, mat->colind, (mat->nnz)*sizeof(ARCHSAI_INT), 		ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);
		ArchSAI_Memcpy(val, mat->val, 		(mat->nnz)*sizeof(ARCHSAI_DOUBLE), 		ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);

		mat->d_nrows = (mat->nrows + mat->format_aux - 1) / mat->format_aux;
		mat->d_ncols = (mat->ncols + mat->format_aux - 1) / mat->format_aux;

		mat->d_spmvr	= ArchSAI_InitGPUVec(mat->d_nrows);
		mat->d_spmvc	= ArchSAI_InitGPUVec(mat->d_ncols);

		mat->d_rowind = ArchSAI_CTAlloc(ARCHSAI_INT, mat->d_nrows + 1,	ArchSAI_MEMORY_DEVICE);

		cusparseXcsr2bsrNnz(mat->sphandle, CUSPARSE_DIRECTION_ROW, mat->nrows, mat->ncols, hlpdescr, rowind, colind, mat->format_aux, mat->matdescr, mat->d_rowind, &(mat->d_nnz));

		mat->d_colind	= ArchSAI_CTAlloc(ARCHSAI_INT, mat->d_nnz,		ArchSAI_MEMORY_DEVICE);
		mat->d_val 		= ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat->d_nnz, 	ArchSAI_MEMORY_DEVICE);

		#ifdef ARCHSAI_SINGLE
		cusparseScsr2bsr(mat->sphandle, CUSPARSE_DIRECTION_ROW, mat->nrows, mat->ncols, hlpdescr, val, rowind, colind, mat->format_aux, mat->matdescr, mat->d_val, mat->d_rowind, mat->d_colind);
		#else
		cusparseDcsr2bsr(mat->sphandle, CUSPARSE_DIRECTION_ROW, mat->nrows, mat->ncols, hlpdescr, val, rowind, colind, mat->format_aux, mat->matdescr, mat->d_val, mat->d_rowind, mat->d_colind);
		#endif

		// COMMUNICATE GLOBAL VALUES NROWS NNZ

		MPI_Allreduce(&(mat->d_gnnz), &(mat->d_nnz), 1, ARCHSAI_MPI_INT, MPI_SUM, mat->comm);
		MPI_Allreduce(&(mat->d_gnrows), &(mat->d_nrows), 1, ARCHSAI_MPI_INT, MPI_SUM, mat->comm);
		MPI_Allreduce(&(mat->d_gncols), &(mat->d_ncols), 1, ARCHSAI_MPI_INT, MPI_SUM, mat->comm);

	}

	ArchSAI_TFree(rowind, ArchSAI_MEMORY_DEVICE);
	ArchSAI_TFree(colind, ArchSAI_MEMORY_DEVICE);
	ArchSAI_TFree(val, ArchSAI_MEMORY_DEVICE);

	#endif
	#endif
}


ARCHSAI_VEC ArchSAI_InitGPUVec(ARCHSAI_INT size){

	ARCHSAI_VEC vec;

	#if defined ARCHSAI_USING_GPU

	vec.size = size;
	vec.val = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, size, 64, ArchSAI_MEMORY_DEVICE);

	#ifdef ARCHSAI_SINGLE
	cusparseCreateDnVec(&(vec.dndescr), size, vec.val, CUDA_R_32F);
	#else
	cusparseCreateDnVec(&(vec.dndescr), size, vec.val, CUDA_R_64F);
	#endif

	#endif


	return vec;
}

size_t ArchSAI_GetSpMVBufferSize(ARCHSAI_MATCSR mat, ARCHSAI_VEC v1, ARCHSAI_VEC v2, ARCHSAI_DOUBLE fp1, ARCHSAI_DOUBLE fp2){
	size_t buffer = 0;

	#if defined ARCHSAI_USING_GPU

	#ifdef ARCHSAI_SINGLE
	cusparseSpMV_bufferSize(mat.sphandle, CUSPARSE_OPERATION_NON_TRANSPOSE, &fp1, mat.matspdescr, v1.dndescr, &fp2, v2.dndescr, CUDA_R_32F, CUSPARSE_CSRMV_ALG1, &buffer);
	#else
	cusparseSpMV_bufferSize(mat.sphandle, CUSPARSE_OPERATION_NON_TRANSPOSE, &fp1, mat.matspdescr, v1.dndescr, &fp2, v2.dndescr, CUDA_R_64F, CUSPARSE_CSRMV_ALG1, &buffer);
	#endif

	#endif

	return buffer;
}

void ArchSAI_CreateBLASHandle(ARCHSAI_GPU_BLASHANDLE *blashandle){
	#ifdef ARCHSAI_USING_CUDA
		cublasCreate(blashandle);
	#endif


}
