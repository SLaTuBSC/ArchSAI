
#include <ArchSAI_Print.h>

#define archsai_printf_buffer_len 4096
ARCHSAI_CHAR archsai_printf_buffer[archsai_printf_buffer_len];


ARCHSAI_INT ArchSAI_PrintMatCSR(ARCHSAI_MATCSR ptr, ARCHSAI_INT val){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr.comm, &rank);
    MPI_Comm_size(ptr.comm, &nprocs);

    ARCHSAI_INT counter   = 0;
    ARCHSAI_INT max_rows  = ArchSAI_Min(ptr.gnrows, 25);
    ARCHSAI_INT max_cols  = ArchSAI_Min(ptr.gncols, 25);

    ArchSAI_ParPrintf(ptr.comm, "\nNROWS: %i | NCOLS: %i | NNZ: %i\n\n", ptr.gnrows, ptr.gncols, ptr.gnnz);

    // Print Column numbers
    ArchSAI_ParPrintf(ptr.comm, "   ");
    for (int i = 0; i < max_cols; i++){
        if ((i%5)==0){
            if (i < 10) ArchSAI_ParPrintf(ptr.comm, "%i ", i);
            else ArchSAI_ParPrintf(ptr.comm, "%i", i);
        }
        else ArchSAI_ParPrintf(ptr.comm, "  ");
    }

    ArchSAI_ParPrintf(ptr.comm, "\n");

    // Print rows
    for (int i = 0; i < max_rows; i++){
        ArchSAI_ParPrintf(ptr.comm, "%i", i);
        if (i < 10) ArchSAI_ParPrintf(ptr.comm, "  ");
        else ArchSAI_ParPrintf(ptr.comm, " ");

        fflush(stdout);
        MPI_Barrier(ptr.comm);

        if (ptr.rowdist[i] == rank) {
            ARCHSAI_INT row = ptr.gl2loc[i];
            for (int j = 0; j < max_cols; j++){
                counter = 0;
                for (int k = ptr.rowind[row]; k < ptr.rowind[row + 1]; k++){
                    ARCHSAI_INT col = ptr.loc2gl[ptr.colind[k]];
                    if (col == j) {
                        counter++;
                        break;
                    }
                }

                if (counter) ArchSAI_printf("* ");
                else ArchSAI_printf("  ");
            }
            ArchSAI_printf("\n");
            fflush(stdout);
        }
        fflush(stdout);
        MPI_Barrier(ptr.comm);
    }

    if(val == 1){

        fflush(stdout);
        MPI_Barrier(ptr.comm);
        ArchSAI_ParPrintf(ptr.comm, "\n");

        for (int i = 0; i < max_rows; i++){
            fflush(stdout);
            MPI_Barrier(ptr.comm);
            ArchSAI_ParPrintf(ptr.comm, "%i", i);
            if (i < 10) ArchSAI_ParPrintf(ptr.comm, "  ");
            else ArchSAI_ParPrintf(ptr.comm, " ");

            fflush(stdout);
            MPI_Barrier(ptr.comm);

            if (ptr.rowdist[i] == rank) {
                ARCHSAI_INT row = ptr.gl2loc[i];
                for (int j = 0; j < max_cols; j++){
                    counter = -1;
                    for (int k = ptr.rowind[row]; k < ptr.rowind[row + 1]; k++){
                        ARCHSAI_INT col = ptr.loc2gl[ptr.colind[k]];
                        if (col == j) {
                            counter = k;
                            break;
                        }
                    }

                    if (ptr.val) if (counter > -1) ArchSAI_printf("%.4e\t", ptr.val[counter]);
                    if (ptr.fval) if (counter > -1) ArchSAI_printf("%.4e\t", ptr.fval[counter]);

                }
                fflush(stdout);
                ArchSAI_printf("\n");
                fflush(stdout);
            }
            fflush(stdout);
            MPI_Barrier(ptr.comm);
        }
    }

    return archsai_error_flag;
}



ARCHSAI_INT ArchSAI_OutPrintMatCSR(ARCHSAI_MATCSR ptr){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    ARCHSAI_CHAR buffig[1024];

    snprintf(buffig, sizeof buffig, "ArchSAI_Out");

    struct stat st = {0};

    if (stat(buffig, &st) == -1) {
        mkdir(buffig, 0700);
    }

    snprintf(buffig, sizeof buffig, "ArchSAI_Out/out_%i.mtx", nprocs);
    FILE *outmnfig = fopen(buffig, "a");
    if (outmnfig == NULL){
        ArchSAI_ParPrintf(ptr.comm, "Could not open writing file.");
    }

    for (int i = 0; i < ArchSAI_Min(ptr.gnrows, 2500); i++){
        if (ptr.rowdist[i] == rank){
            for (int j = ptr.rowind[ptr.gl2loc[i]]; j < ptr.rowind[ptr.gl2loc[i] + 1]; j++){
                if (ptr.val) fprintf(outmnfig, "%i\t%i\t%.16e\n", i + 1, ptr.loc2gl[ptr.colind[j]] + 1, ptr.val[j]);
                else if (ptr.fval) fprintf(outmnfig, "%i\t%i\t%.16e\n", i + 1, ptr.loc2gl[ptr.colind[j]] + 1, ptr.fval[j]);
                else fprintf(outmnfig, "%i\t%i\t%i\n", i + 1, ptr.colind[j] + 1, 1);
            }
        }
        fflush(outmnfig);
        MPI_Barrier(ptr.comm);
    }
    fclose(outmnfig);
    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_OutPrintRHS(ARCHSAI_VEC vector, ARCHSAI_CHAR *name){

    ARCHSAI_CHAR str[256];
    ARCHSAI_CHAR matrix_id[256];
    const ARCHSAI_CHAR ch = '/';

    strcpy(str, name);
    if (strrchr(str, ch) == NULL) strcpy(matrix_id, name);
    else strcpy(matrix_id, strrchr(str, ch) + 1);

    ARCHSAI_CHAR buffig[1024];

    snprintf(buffig, sizeof buffig, "ArchSAI_Out");

    struct stat st = {0};

    if (stat(buffig, &st) == -1) {
        mkdir(buffig, 0700);
    }

    snprintf(buffig, sizeof buffig, "ArchSAI_Out/b_%s", matrix_id);
    FILE *outmnfig = fopen(buffig, "w");
    if (outmnfig == NULL){
        printf("Could not open writing file.\n");
        ArchSAI_Error(1);
    }

    // HEADER
    fprintf(outmnfig, "%%%% MatrixMarket matrix array real general\n");
    fprintf(outmnfig, "%%-------------------------------------------------------------------------------\n");
    fprintf(outmnfig, "%% UF Sparse Matrix Collection, Tim Davis\n");
    fprintf(outmnfig, "%% http://www.cise.ufl.edu/research/sparse/matrices/\n");
    fprintf(outmnfig, "%% name: %s : b matrix\n", matrix_id);
    fprintf(outmnfig, "%%-------------------------------------------------------------------------------\n");

    // SIZE
    fprintf(outmnfig, "%i\t%i\n", vector.size, 1);

    // VALUES

    for (int i = 0; i < vector.size; i++){
        fprintf(outmnfig, "%lf\n", vector.val[i]);
    }

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_ExportSolverMetrics(ARCHSAI_SOLVER solver, ARCHSAI_PARAMS params, MPI_Comm comm){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &nprocs);

    char str[256];
    char matrix_id[256];
    char rhs_id[256];
    char path[256];
    char convfile[256];
    char expsfile[256];
    const char ch = '/';
    FILE *f;

    ARCHSAI_INT my_id;
    ARCHSAI_INT ierr = archsai_MPI_Comm_rank(comm, &my_id);

    if (ierr){
        return ierr;
    }

    if (!my_id) {

        // Create folder to store results

        strcpy(str, params.matname);
        if (strrchr(str, ch) == NULL) strcpy(matrix_id, params.matname);
        else strcpy(matrix_id, strrchr(str, ch) + 1);

        strcpy(str, params.rhs);
        if (strrchr(str, ch) == NULL) strcpy(rhs_id, params.rhs);
        else strcpy(rhs_id, strrchr(str, ch) + 1);

        snprintf(path, sizeof path, "ArchSAI_Out");

        struct stat st = {0};

        if (stat(path, &st) == -1) {
            mkdir(path, 0700);
        }

        snprintf(path, sizeof path, "ArchSAI_Out/%s", matrix_id);

        struct stat dt = {0};

        if (stat(path, &dt) == -1) {
            mkdir(path, 0700);
        }

        snprintf(convfile, sizeof convfile, "%s/%i%i_%i%i%i.ArchSAIconv", path, params.sol_sel,  params.precond, params.ext_sel, params.ext_spa, params.ext_tmp);
        snprintf(expsfile, sizeof expsfile, "%s/%s_%s.ArchSAIexps", path, matrix_id, rhs_id);

        // First we create the convergence file

        f = fopen(convfile, "w");

        if (f == NULL){
            printf("Could not open writing file.");
            return 0;
        }

        // Get best exp

        fprintf(f, "%% Convergence results for matrix:\n");
        fprintf(f, "%s %s %i %i %i\n", matrix_id, rhs_id, solver.iter[solver.bestrep], nprocs, omp_get_max_threads());
        fprintf(f, "%% Params: \n");
        fprintf(f, "%i %i %i %i %i %i %i %i %i %.2e %.2e %.2e %.2e %i\n", params.sol_sel, params.sol_aux, params.precond, params.pat_sel, params.pat_aux, params.ext_sel, params.ext_spa, params.ext_tmp, params.imax, params.rel_tol, params.abs_tol, params.kap_epsilon, params.filter, params.filter_sel);

        fprintf(f, "%% Iter\ttIter\tAccTime\tRes\tRelRes\n");

        ARCHSAI_DOUBLE acctime = 0.0;
        for (int i = 0; i < solver.iter[solver.bestrep] + 1; i++){
            acctime += solver.iter_time[solver.bestrep][i];
            fprintf(f, "%i\t%.4e\t%.4e\t%.4e\t%.4e\n", i, solver.iter_time[solver.bestrep][i], acctime, solver.residual[solver.bestrep][i], solver.rel_res[solver.bestrep][i]);
        }

        fclose(f);

        // Second we create the experiment average file

        f = fopen(expsfile, "a");

        if (f == NULL){
            printf("Could not open writing file.");
            return 0;
        }

        fprintf(f, "%i\t%i\t", nprocs, omp_get_max_threads());

        fprintf(f, "%i\t%.4e\t", solver.iter[solver.bestrep], solver.time[solver.bestrep]);

        fprintf(f, "%i\t%.2e\t", params.filter_sel, params.filter);

        fprintf(f, "%i\t%i\t%i\t%i\t%i\t%i\t", solver.pc_nnz, solver.pc_nrows, solver.pc_ncols, solver.sys_nnz, solver.sys_nrows, solver.sys_ncols);

        fprintf(f, "%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%.2e\t%.2e\t%.2e\n", params.sol_sel, params.sol_aux, params.precond, params.pat_sel, params.pat_aux, params.ext_sel, params.ext_spa, params.ext_tmp, params.imax, params.rel_tol, params.abs_tol, params.kap_epsilon);

        fclose(f);
    }

    return archsai_error_flag;
}


ARCHSAI_INT new_format( const char *format, char **newformat_ptr ){
    const char *fp;
    char       *newformat, *nfp;
    ARCHSAI_INT   newformatlen;
    ARCHSAI_INT   copychar;
    ARCHSAI_INT   foundpercent = 0;

    newformatlen = 2 * strlen(format) + 1; /* worst case is all %d's to %lld's */

    if (newformatlen > archsai_printf_buffer_len){
        newformat = ArchSAI_TAlloc(char, newformatlen, ArchSAI_MEMORY_HOST);
    }
    else {
        newformat = archsai_printf_buffer;
    }

    nfp = newformat;
    for (fp = format; *fp != '\0'; fp++)
    {
        copychar = 1;
        if (*fp == '%')
        {
            foundpercent = 1;
        }
        else if (foundpercent)
        {
            if (*fp == 'l')
            {
                fp++; /* remove 'l' and maybe add it back in switch statement */
                if (*fp == 'l')
                {
                fp++; /* remove second 'l' if present */
                }
            }
            switch (*fp)
            {
                case 'b': /* used for BigInt type in hypre */
    #if defined(ARCHSAI_BIGINT) || !defined(ARCHSAI_SMALLINT)
                *nfp = 'l'; nfp++;
                *nfp = 'l'; nfp++;
    #endif
                *nfp = 'd'; nfp++; copychar = 0;
                foundpercent = 0; break;
                case 'd':
                case 'i':
    #if defined(ARCHSAI_BIGINT)
                *nfp = 'l'; nfp++;
                *nfp = 'l'; nfp++;
    #endif
                foundpercent = 0; break;
                case 'f':
                case 'e':
                case 'E':
                case 'g':
                case 'G':
    #if defined(ARCHSAI_SINGLE)          /* no modifier */
    #elif defined(ARCHSAI_LONG_DOUBLE)   /* modify with 'L' */
                *nfp = 'L'; nfp++;
    #else                              /* modify with 'l' (default is _double_) */
                *nfp = 'l'; nfp++;
    #endif
                foundpercent = 0; break;
                case 'c':
                case 'n':
                case 'o':
                case 'p':
                case 's':
                case 'u':
                case 'x':
                case 'X':
                case '%':
                foundpercent = 0; break;
            }
        }
        if (copychar)
        {
            *nfp = *fp; nfp++;
        }
    }
    *nfp = *fp;

    *newformat_ptr = newformat;

    /*   printf("\nNEWFORMAT: %s\n", *newformat_ptr);*/

    return 0;
}

ARCHSAI_INT free_format( char *newformat ){
    if (newformat != archsai_printf_buffer){
        ArchSAI_TFree(newformat, ArchSAI_MEMORY_HOST);
    }
    return 0;
}


ARCHSAI_INT ArchSAI_ndigits( ARCHSAI_BIGINT number ){
    ARCHSAI_INT     ndigits = 0;

    while (number){
        number /= 10;
        ndigits++;
    }

    return ndigits;
}

    /* printf functions */

ARCHSAI_INT ArchSAI_printf( const char *format, ...){

    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    va_start(ap, format);
    new_format(format, &newformat);
    ierr = vprintf(newformat, ap);
    free_format(newformat);
    va_end(ap);

    fflush(stdout);

    return ierr;
}

ARCHSAI_INT ArchSAI_fprintf( FILE *stream, const char *format, ...){


    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    if (rank == 0){
        va_start(ap, format);
        new_format(format, &newformat);
        ierr = vfprintf(stream, newformat, ap);
        free_format(newformat);
        va_end(ap);
    }

    return ierr;
}

ARCHSAI_INT ArchSAI_sprintf( char *s, const char *format, ...){
    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    va_start(ap, format);
    new_format(format, &newformat);
    ierr = vsprintf(s, newformat, ap);
    free_format(newformat);
    va_end(ap);

    return ierr;

}

    /* scanf functions */
ARCHSAI_INT ArchSAI_scanf( const char *format, ...){
    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    va_start(ap, format);
    new_format(format, &newformat);
    ierr = vscanf(newformat, ap);
    free_format(newformat);
    va_end(ap);

    return ierr;

}

ARCHSAI_INT ArchSAI_fscanf( FILE *stream, const char *format, ...){
    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    va_start(ap, format);
    new_format(format, &newformat);
    ierr = vfscanf(stream, newformat, ap);
    free_format(newformat);
    va_end(ap);

    return ierr;
}

ARCHSAI_INT ArchSAI_sscanf( char *s, const char *format, ...){
    va_list   ap;
    char     *newformat;
    ARCHSAI_INT ierr = 0;

    va_start(ap, format);
    new_format(format, &newformat);
    ierr = vsscanf(s, newformat, ap);
    free_format(newformat);
    va_end(ap);

    return ierr;
}

ARCHSAI_INT ArchSAI_ParPrintf(MPI_Comm comm, const char *format, ...){
    ARCHSAI_INT my_id;
    ARCHSAI_INT ierr = archsai_MPI_Comm_rank(comm, &my_id);

    if (ierr){
        return ierr;
    }

    if (!my_id) {
        va_list ap;
        char   *newformat;

        va_start(ap, format);
        new_format(format, &newformat);
        ierr = vprintf(newformat, ap);
        free_format(newformat);
        va_end(ap);

        fflush(stdout);
    }

    fflush(stdout);
    MPI_Barrier(comm);

    return ierr;
}



























ARCHSAI_INT ArchSAI_ExportData(ARCHSAI_HANDLER handler, MPI_Comm comm){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &nprocs);

    char str[256];
    char matrix_id[256];
    char rhs_id[256];
    char path[256];
    char convfile[256];
    char expsfile[256];
    const char ch = '/';
    FILE *f;

    ARCHSAI_INT my_id;
    ARCHSAI_INT ierr = archsai_MPI_Comm_rank(comm, &my_id);

    if (ierr){
        return ierr;
    }

    if (!my_id) {

        // Create folder to store results

        strcpy(str, handler.params.matname);
        if (strrchr(str, ch) == NULL) strcpy(matrix_id, handler.params.matname);
        else strcpy(matrix_id, strrchr(str, ch) + 1);

        strcpy(str, handler.params.rhs);
        if (strrchr(str, ch) == NULL) strcpy(rhs_id, handler.params.rhs);
        else strcpy(rhs_id, strrchr(str, ch) + 1);

        snprintf(path, sizeof path, "ArchSAI_Out");

        struct stat st = {0};

        if (stat(path, &st) == -1) {
            mkdir(path, 0700);
        }

        snprintf(path, sizeof path, "ArchSAI_Out/%s", matrix_id);

        struct stat dt = {0};

        if (stat(path, &dt) == -1) {
            mkdir(path, 0700);
        }

        snprintf(convfile, sizeof convfile, "%s/%i%i_%i-%i-%i.ArchSAIconv", path, handler.params.sol_sel,  handler.params.precond, handler.params.ext_sel, handler.params.ext_spa, handler.params.ext_tmp);
        snprintf(expsfile, sizeof expsfile, "%s/%s_%s.ArchSAIexps", path, matrix_id, rhs_id);

        // First we create the convergence file

        f = fopen(convfile, "w");

        if (f == NULL){
            printf("Could not open writing file.");
            return 0;
        }

        // Get best exp

        fprintf(f, "%% Convergence results for matrix:\n");
        fprintf(f, "%s %s %i %i %i\n", matrix_id, rhs_id, handler.solver.iter[handler.solver.bestrep], nprocs, omp_get_max_threads());
        fprintf(f, "%% Params: \n");
        fprintf(f, "%i %i %i %i %i %i %i %i %i %.2e %.2e %.2e %.2e %i\n", handler.params.sol_sel, handler.params.sol_aux, handler.params.precond, handler.params.pat_sel, handler.params.pat_aux, handler.params.ext_sel, handler.params.ext_spa, handler.params.ext_tmp, handler.params.imax, handler.params.rel_tol, handler.params.abs_tol, handler.params.kap_epsilon, handler.params.filter, handler.params.filter_sel);

        fprintf(f, "%% Iter\ttIter\tAccTime\tRes\tRelRes\n");

        ARCHSAI_DOUBLE acctime = 0.0;
        for (int i = 0; i < handler.solver.iter[handler.solver.bestrep] + 1; i++){
            acctime += handler.solver.iter_time[handler.solver.bestrep][i];
            fprintf(f, "%i\t%.4e\t%.4e\t%.4e\t%.4e\n", i, handler.solver.iter_time[handler.solver.bestrep][i], acctime, handler.solver.residual[handler.solver.bestrep][i], handler.solver.rel_res[handler.solver.bestrep][i]);
        }

        fclose(f);

        // Second we create the experiment average file

        f = fopen(expsfile, "a");

        if (f == NULL){
            printf("Could not open writing file.");
            return 0;
        }

        fprintf(f, "%i\t%i\t", nprocs, omp_get_max_threads());
        fprintf(f, "%.4e\t%.4e\t%.4e\t", handler.times[0], handler.times[1], handler.times[2]);

        fprintf(f, "%i\t", handler.solver.iter[handler.solver.bestrep]);

        fprintf(f, "%i\t%i\t%i\t", handler.params.ext_sel, handler.params.ext_spa, handler.params.ext_tmp);

        fprintf(f, "%i\t%.2e\t", handler.params.filter_sel, handler.params.filter);

        fprintf(f, "%i\t%i\t%i\t%i\t", handler.solver.pc_nnz, handler.solver.sys_nnz, handler.solver.sys_nrows, handler.solver.sys_ncols);

        fprintf(f, "%i\t%i\t%i\t%i\t%i\t%i\t%.2e\t%.2e\t%.2e\n", handler.params.sol_sel, handler.params.sol_aux, handler.params.precond, handler.params.pat_sel, handler.params.pat_aux, handler.params.imax, handler.params.rel_tol, handler.params.abs_tol, handler.params.kap_epsilon);

        fclose(f);
    }

    return archsai_error_flag;
}
