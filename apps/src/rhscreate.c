#include <ArchSAI.h>
#include <ArchSAI_General.h>
#include <ArchSAI_ParseArgs.h>
#include <ArchSAI_Read.h>

#include <ArchSAI_Random.h>

ARCHSAI_INT main(ARCHSAI_INT argc, ARCHSAI_CHAR *argv[]){

    ARCHSAI_HANDLER handler;
    ARCHSAI_MATCSR    A;

    ARCHSAI_VEC       sol;

    ArchSAI_Init(&argc, &argv);                   // TODO Implement utilities such as memory tracking

    ArchSAI_ParseArgs(argc, argv, &handler);

    A = ArchSAI_ReadMTXtoMatCSR(handler.params, MPI_COMM_WORLD);

    printf("%i %i\n", A.sym, A.fulldiag);
    printf("%i %i\n", A.gnnz, A.gnrows);

    ARCHSAI_DOUBLE norm = ArchSAI_MatCSR_Norm1(A);
    ArchSAI_Seed(-norm, norm);

    ARCHSAI_VEC b;

    b.size = A.gnrows;
    b.val = ArchSAI_TAlloc(ARCHSAI_DOUBLE, b.size, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < b.size; i++){
        b.val[i] = ArchSAI_dRandom();
    }

    ArchSAI_OutPrintRHS(b, handler.params.matname);

    ArchSAI_TFree(b.val, ArchSAI_MEMORY_HOST);

    ArchSAI_End();

    return 0;
}



