
#include <ArchSAI_PCBuilder.h>

ARCHSAI_INT ArchSAI_BuildPC(ARCHSAI_MATCSR A, ARCHSAI_PRECOND *PC, ARCHSAI_HANDLER *handler){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    #ifdef ARCHSAI_TIMING
    ARCHSAI_INT timeindex = ArchSAI_InitializeTiming("PC Build");
    ArchSAI_BeginTiming(timeindex);
    #endif

    fflush(stdout);
    MPI_Barrier(A.comm);

    #ifdef ARCHSAI_VERBOSE
        fflush(stdout);
        MPI_Barrier(A.comm);
        ArchSAI_ParPrintf(A.comm, "\n************** ArchSAI PC    *************\n");
    #endif


    // SWITCH PC TYPE
    switch(handler->params.precond){
        /* NO PRECOND*/
        case 0:
            PC->type = 0;
            PC->sys_size = A.nrows;
            break;

        /* DIAGONAL */
        case 1:
            *PC = ArchSAI_DiagonalPC(A, handler->params);
            break;

        /* SAI */
        case 2:
            *PC = ArchSAI_SAI(A, handler->params);
            break;

        /* FSAI */
        case 3:
            *PC = ArchSAI_FSAI(A, handler->params);
            break;

        /* GFSAI */
        case 4:
            ArchSAI_Error(10);
            break;

        /* NO PRECOND */
        default:
            PC->type = 0;
            PC->sys_size = A.nrows;
            break;
    }

    #ifdef ARCHSAI_VERBOSE

        if (handler->params.precond != 0){
            fflush(stdout);
            MPI_Barrier(A.comm);
            ArchSAI_ParPrintf(A.comm, "\nPreconditioner (%i %i) Global data: %i %i %i\n", PC->mat.sym, PC->mat.fulldiag, PC->mat.gnrows, PC->mat.gnnz, PC->mat.gncols);
            ArchSAI_ParPrintf(A.comm, "Preconditioner (%i %i) Row Distribution\n\n", PC->mat.sym, PC->mat.fulldiag);
            fflush(stdout);
            MPI_Barrier(A.comm);

            for (int i = 0; i < nprocs; i++){
                if (rank == i){
                    if (handler->params.precond == 1) printf("\t%i\t| M:\t%i %i %i", rank, PC->mat.nrows, PC->mat.nnz, PC->mat.ncols);
                    if (handler->params.precond == 2) printf("\t%i\t| M:\t%i %i %i", rank, PC->mat.nrows, PC->mat.nnz, PC->mat.ncols);
                    if (handler->params.precond == 3) printf("\t%i\t| G:\t%i %i %i", rank, PC->mat.nrows, PC->mat.nnz, PC->mat.ncols);
                    printf("\t| Comm Send: %i %i | Comm Recv: %i %i\n", commscheme(PC->mat)->nsend, commscheme(PC->mat)->sendsize, commscheme(PC->mat)->nrecv, commscheme(PC->mat)->recvsize);

                    if (handler->params.precond == 3){
                        printf("\t\t| Gt:\t%i %i %i", PC->mattransp.nrows, PC->mattransp.nnz, PC->mattransp.ncols);
                        printf("\t| Comm Send: %i %i | Comm Recv: %i %i\n", commscheme(PC->mattransp)->nsend, commscheme(PC->mattransp)->sendsize, commscheme(PC->mattransp)->nrecv, commscheme(PC->mattransp)->recvsize);
                    }
                    printf("\n");
                    fflush(stdout);
                }
                MPI_Barrier(A.comm);
            }
        }

    #endif // VERBOSE

    #ifdef ARCHSAI_TIMING
    ArchSAI_EndTiming(timeindex);
    handler->times[1] = ArchSAI_TimingWallTime(timeindex);
    #endif // TIMING

    fflush(stdout);
    MPI_Barrier(A.comm);

    return archsai_error_flag;
}
