
 /*! \file ArchSAI_SAI.h
    \brief This file contains the SAI preconditioning computation used by ArchSAI.

    ArchSAI SAI computation.
*/

 #ifndef ArchSAI_SAI_h_
    #define ArchSAI_SAI_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Patterns.h>
    #include <ArchSAI_LinAlg.h>
    #include <ArchSAI_Utils.h>


    /*! \fn ARCHSAI_PRECOND ArchSAI_SAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params);
        \brief Builds a SAI preconditioner according to parameters

        \param A Input matrix
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_PRECOND ArchSAI_SAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_PRECOND ArchSAI_FSAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params);
        \brief Builds an FSAI preconditioner according to parameters

        \param A Input matrix
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_PRECOND ArchSAI_FSAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_INT ArchSAI_ComputeSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);
        \brief Computes SAI over a given pattern.

        \param A Input matrix
        \param pattern Pattern
    */
    ARCHSAI_INT ArchSAI_ComputeSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);

    /*! \fn ARCHSAI_INT ArchSAI_ComputeFSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);
        \brief Computes FSAI over a given lower triangular pattern.

        \param A Input matrix
        \param pattern Pattern
    */
    ARCHSAI_INT ArchSAI_ComputeFSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);


    /*! \fn ARCHSAI_INT ArchSAI_ComputeSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);
        \brief Computes SAI over a given pattern.

        \param A Input matrix
        \param pattern Pattern
    */
    ARCHSAI_INT ArchSAI_ComputeSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);

    /*! \fn ARCHSAI_INT ArchSAI_ComputeFSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);
        \brief Computes FSAI over a given lower triangular pattern.

        \param A Input matrix
        \param pattern Pattern
    */
    ARCHSAI_INT ArchSAI_ComputeFSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern);

#endif // __ArchSAI_Sai_h__




