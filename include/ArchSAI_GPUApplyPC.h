 /*! \file ArchSAI_GPUApplyPC.h
    \brief This file contains prototypes used by ArchSAI for preconditioning.

    ArchSAI preconditioner application.
*/

 #ifndef ArchSAI_GPUApplyPC_h_
    #define ArchSAI_GPUApplyPC_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_GPULinAlg.h>

/*! \fn void ArchSAI_GPU_PrecondSpMV(ARCHSAI_PRECOND PC, ARCHSAI_DOUBLE *In, ARCHSAI_DOUBLE *Out, ARCHSAI_DOUBLE *tmp)
    \brief Applies an ArchSAI preconditioner to a vector and produces a vector output via SpMV.

    \param PC The ArchSAI Preconditioner structure.
    \param In Input vector.
    \param Out Output vector.
    \param tmp Temporary vector to store middle result from FSAI.
    \param nprocs number of processes
*/
    void ArchSAI_GPU_PrecondSpMV(ARCHSAI_PRECOND PC, ARCHSAI_VEC In, ARCHSAI_VEC Out, ARCHSAI_VEC tmp, void* buffer, ARCHSAI_INT nprocs);

#endif // __ArchSAI_GPUApplyPC_h__




