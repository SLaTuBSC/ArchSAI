
 /*! \file ArchSAI_Error.h
    \brief This file contains the error utilities for ArchSAI.

    ArchSAI error utilities.
*/

 #ifndef ArchSAI_Error_h_
    #define ArchSAI_Error_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Print.h>
    #include <assert.h>
    #include <stdio.h>
    #include <stdlib.h>


    /*! \def archsai_error_flag
        \brief A macro that defines the error flag.

        Contains the global error.
    */
    extern ARCHSAI_INT archsai_global_error;
    #define archsai_error_flag  archsai_global_error

    /*! \def ARCHSAI_ERROR_GENERIC
        \brief A macro that describes the generic error.
    */
    #define ARCHSAI_ERROR_GENERIC         1

    /*! \def ARCHSAI_ERROR_MEMORY
        \brief A macro that describes the memory error.

        To be used with memory allocation failure.
    */
    #define ARCHSAI_ERROR_MEMORY          2

    /*! \def ARCHSAI_ERROR_ARG
        \brief A macro that describes argument error.

        To be used for incorrect arguments.
    */
    #define ARCHSAI_ERROR_ARG             4
    /* bits 4-8 are reserved for the index of the argument error */

    /*! \def ARCHSAI_ERROR_IMPL
        \brief A macro that describes an error for something not yet implemented.

        Used when no implementation available.
    */
    #define ARCHSAI_ERROR_IMPL          10

    /*! \def ARCHSAI_ERROR_CONV
        \brief A macro that describes the convergence error.

        Used when solvers do not converge.
    */
    #define ARCHSAI_ERROR_CONV          256

    /*! \def ARCHSAI_MAX_FILE_NAME_LEN
        \brief A macro that describes the file name error.
    */
    #define ARCHSAI_MAX_FILE_NAME_LEN  1024

    #define ARCHSAI_UNUSED_VAR(var) ((void) var)


    /*! \fn ArchSAI_ErrorHandler(const char *filename, ARCHSAI_INT line, ARCHSAI_INT ierr, const char *msg)
        \brief Process the error with code ierr raised in the given line of the given source file.

        \param filename The name of the file where the error is printed.
        \param line Line in code the error is produced.
        \param ierr Error code.
        \param msg Char buffer where error type is stored.
    */
    void ArchSAI_ErrorHandler(const char *filename, ARCHSAI_INT line, ARCHSAI_INT ierr, const char *msg);

    /*! \def ArchSAI_Error(IERR)
        \brief A macro that allows for a simplified use of ArchSAI_ErrorHandler.

        If -DArchSAI_PRINT_ERRORS is set at compile time it displays a message to the stderr buffer.
    */
    #define ArchSAI_Error(IERR)  ArchSAI_ErrorHandler(__FILE__, __LINE__, IERR, NULL)

    /*! \def ArchSAI_ErrorWithMsg(IERR, msg)
        \brief A macro that allows for a simplified use of ArchSAI_ErrorHandler.

        If -DArchSAI_PRINT_ERRORS is set at compile time it displays a message to the stderr buffer.
        It retrieves the error message.
    */
    #define ArchSAI_ErrorWithMsg(IERR, msg)  ArchSAI_ErrorHandler(__FILE__, __LINE__, IERR, msg)

    /*! \def ArchSAI_ErrorInArg(IARG)
        \brief A macro that allows for a simplified use of ArchSAI_ErrorHandler for arguments.

        If -DArchSAI_PRINT_ERRORS is set at compile time it displays a message to the stderr buffer.
    */
    #define ArchSAI_ErrorInArg(IARG)  ArchSAI_error(ARCHSAI_ERROR_ARG | IARG<<3)


    /*! \def ArchSAI_Assert(EX)
        \brief A macro for asserting and exitting.

    */
    #define ArchSAI_Assert(EX) do { if (!(EX)) { fprintf(stderr, "[%s, %d] ArchSAI_Assert failed: %s\n", __FILE__, __LINE__, #EX); ArchSAI_Error(1); assert(0); } } while (0)

    /*! \fn ARCHSAI_INT ArchSAI_GetError(void)
        \brief Returns the error code at any point this is called.

    */
    ARCHSAI_INT ArchSAI_GetError(void);

    /*! \fn ARCHSAI_INT ArchSAI_CheckError(ARCHSAI_INT ArchSAI_ierr, ARCHSAI_INT ArchSAI_error_code)
        \brief Check if the given error flag contains the given error code

        \param ArchSAI_ierr Error value.
        \param ArchSAI_error_code Error code.
    */
    ARCHSAI_INT ArchSAI_CheckError(ARCHSAI_INT ArchSAI_ierr, ARCHSAI_INT ArchSAI_error_code);

    /*! \fn ARCHSAI_INT ArchSAI_GetErrorArg(void)
        \brief Return the index of the argument (counting from 1) where argument error (ARCHSAI_ERROR_ARG) has occured

    */
    ARCHSAI_INT ArchSAI_GetErrorArg(void);


    /*! \fn void ArchSAI_DescribeError(ARCHSAI_INT ArchSAI_ierr, char *descr)
        \brief Describe the given error flag in the given string.

    */
    void ArchSAI_DescribeError(ARCHSAI_INT ArchSAI_ierr, char *descr);

    /*! \fn ARCHSAI_INT ArchSAI_ClearAllErrors(void)
        \brief Clears the ArchSAI error flag.

    */
    ARCHSAI_INT ArchSAI_ClearAllErrors(void);

    /*! \fn ARCHSAI_INT ArchSAI_ClearError(ARCHSAI_INT ArchSAI_error_code)
        \brief Clears the given error code from the ArchSAI error flag.

    */
    ARCHSAI_INT ArchSAI_ClearError(ARCHSAI_INT ArchSAI_error_code);

#endif // __ArchSAI_Error_h__


