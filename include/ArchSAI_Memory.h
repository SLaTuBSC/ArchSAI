
 /*! \file ArchSAI_Memory.h
    \brief This file contains the memory management utilities for ArchSAI.

    ArchSAI memory utilities.
*/

 #ifndef ArchSAI_Memory_h_
    #define ArchSAI_Memory_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <string.h>

    #ifdef ARCHSAI_USING_CUDA
        #include <cuda.h>
        #include <cuda_runtime.h>
    #endif

	#ifdef ARCHSAI_USING_AMD
		#include <hip/hip_runtime.h>
	#endif


    /*! \var typedef enum _ArchSAI_MemoryLocation
        \brief A type definition for Memory Locations.

        For the moment, only HOST memory is used as implementation includes only CPU routines.
        TODO: GPU
    */

    typedef enum _ArchSAI_MemoryLocation {
        ArchSAI_MEMORY_UNDEFINED = -1,
        ArchSAI_MEMORY_HOST,
        ArchSAI_MEMORY_HOST_PINNED,
        ArchSAI_MEMORY_DEVICE,
        ArchSAI_MEMORY_UNIFIED,
        ArchSAI_NUM_MEMORY_LOCATION
    } ArchSAI_MemoryLocation;


    /*-------------------------------------------------------
    * hypre_GetActualMemLocation
    *   return actual location based on the selected memory model
    *-------------------------------------------------------*/

    /*! \fn static inline ArchSAI_MemoryLocation ArchSAI_GetActualMemLocation(ArchSAI_MemoryLocation location)
        \brief Returns actual location based on the selected memory model.
        \param location ArchSAI location.

        TODO: Include other possible locations.
    */

    static inline ArchSAI_MemoryLocation ArchSAI_GetActualMemLocation(ArchSAI_MemoryLocation location){
        if (location == ArchSAI_MEMORY_HOST){
            return ArchSAI_MEMORY_HOST;
        }

        if (location == ArchSAI_MEMORY_HOST_PINNED){
            return ArchSAI_MEMORY_HOST_PINNED;
        }

        if (location == ArchSAI_MEMORY_DEVICE){
//             #if defined(HYPRE_USING_HOST_MEMORY)
//                 return hypre_MEMORY_HOST;
//             #elif defined(HYPRE_USING_DEVICE_MEMORY)
                return ArchSAI_MEMORY_DEVICE;
//             #elif defined(HYPRE_USING_UNIFIED_MEMORY)
//                 return hypre_MEMORY_UNIFIED;
//             #else
//                 #error Wrong HYPRE memory setting.
//             #endif
        }



        return ArchSAI_MEMORY_UNDEFINED;
    }

    /*! \def ArchSAI_TAlloc(type, count, location)
        \brief A macro to prepare code for memory tracking.

        Details.
    */

    /*! \def ArchSAI_CTAlloc(type, count, location)
        \brief A macro to prepare code for memory tracking.

        Details.
    */

    /*! \def ArchSAI_TFree(ptr, location)
        \brief A macro to prepare code for memory tracking.

        Details.
    */


    #if !defined(ARCHSAI_USING_MEMORY_TRACKER)

        #define ArchSAI_TAlloc(type, count, location) \
        ( (type *) ArchSAI_MAlloc((size_t)(sizeof(type) * (count)), location) )

        #define _ArchSAI_TAlloc(type, count, location) \
        ( (type *) _ArchSAI_MAlloc((size_t)(sizeof(type) * (count)), location) )

        #define ArchSAI_CTAlloc(type, count, location) \
        ( (type *) ArchSAI_CAlloc((size_t)(count), (size_t)sizeof(type), location) )

        #define ArchSAI_TAlignedAlloc(type, count, align, location) \
        ( (type *) ArchSAI_AlignedAlloc((size_t)(count), (size_t)sizeof(type), align, location) )

        #define ArchSAI_TReAlloc(ptr, type, count, location) \
        ( (type *) ArchSAI_ReAlloc((char *)ptr, (size_t)(sizeof(type) * (count)), location) )

        #define ArchSAI_TReAlloc_v2(ptr, old_type, old_count, new_type, new_count, location) \
        ( (new_type *) ArchSAI_ReAlloc_v2((char *)ptr, (size_t)(sizeof(old_type)*(old_count)), (size_t)(sizeof(new_type)*(new_count)), location) )

        #define ArchSAI_TMemcpy(dst, src, type, count, locdst, locsrc) \
        (ArchSAI_Memcpy((void *)(dst), (void *)(src), (size_t)(sizeof(type) * (count)), locdst, locsrc))

        #define ArchSAI_TFree(ptr, location) \
        ( ArchSAI_Free((void *)ptr, location), ptr = NULL )

        #define _ArchSAI_TFree(ptr, location) \
        ( _ArchSAI_Free((void *)ptr, location), ptr = NULL )

    #endif /* #if !defined(ArchSAI_USING_MEMORY_TRACKER) */


    /*--------------------------------------------------------------------------
    * Prototypes
    *--------------------------------------------------------------------------*/

    /*! \fn ARCHSAI_INT ArchSAI_GetMemoryLocationName(ArchSAI_MemoryLocation memory_location, char *memory_location_name);
        \brief Stores in pointer actual location based on the selected memory model.
        \param memory_location ArchSAI location.
        \param memory_location_name ArchSAI memory location name.
    */

    ARCHSAI_INT ArchSAI_GetMemoryLocationName(ArchSAI_MemoryLocation memory_location, char *memory_location_name);



    /*! \fn void * ArchSAI_MAlloc(size_t size, ArchSAI_MemoryLocation location);
        \brief Allocates memory in provided memory location.
        \param size Size in bytes to allocate.
        \param location Where to allocate memory.
    */

    void * ArchSAI_MAlloc(size_t size, ArchSAI_MemoryLocation location);

    /*! \fn void * ArchSAI_CAlloc(size_t size, ArchSAI_MemoryLocation location);
        \brief Allocates memory in provided memory location and initializes it to zeroes.
        \param count Amount of entries to allocate.
        \param elt_size Size of each entry.
        \param location Where to allocate memory.
    */

    void * ArchSAI_CAlloc(size_t count, size_t elt_size, ArchSAI_MemoryLocation location);


    /*! \fn void * ArchSAI_AlignedAlloc(size_t size, ArchSAI_MemoryLocation location);
        \brief Allocates memory in provided memory location and initializes it to zeroes.
        \param count Amount of entries to allocate.
        \param elt_size Size of each entry.
        \param align Alignment multiple  in memory.
        \param location Where to allocate memory.
    */

    void * ArchSAI_AlignedAlloc(size_t count, size_t elt_size, ARCHSAI_INT align, ArchSAI_MemoryLocation location);


    /*! \fn void * ArchSAI_ReAlloc(void *ptr, size_t size, ArchSAI_MemoryLocation location);
        \brief Reallocates memory in provided memory location.
        \param count Amount of entries to allocate.
        \param elt_size Size of each entry.
        \param location Where to allocate memory.
    */

    void * ArchSAI_ReAlloc(void *ptr, size_t size, ArchSAI_MemoryLocation location);

    /*! \fn void * ArchSAI_ReAlloc_v2(void *ptr, size_t old_size, size_t new_size, ArchSAI_MemoryLocation location);
        \brief Reallocates memory in provided memory location. Changes data types.
        \param ptr
        \param old_size
        \param new_size
        \param location
    */

    void * ArchSAI_ReAlloc_v2(void *ptr, size_t old_size, size_t new_size, ArchSAI_MemoryLocation location);

    /*! \fn void   ArchSAI_Memcpy(void *dst, void *src, size_t size, ArchSAI_MemoryLocation loc_dst, ArchSAI_MemoryLocation loc_src);
        \brief Copies data from source to destination.
        \param dst
        \param src
        \param size
        \param loc_dst
        \param loc_src
    */

    void   ArchSAI_Memcpy(void *dst, void *src, size_t size, ArchSAI_MemoryLocation loc_dst, ArchSAI_MemoryLocation loc_src);


    /*! \fn void   ArchSAI_Free(void *ptr, ArchSAI_MemoryLocation location);
        \brief Frees memory in provided memory location and initializes it to zeroes.
        \param ptr Pointer to free.
        \param location Where to free memory.
    */

    void   ArchSAI_Free(void *ptr, ArchSAI_MemoryLocation location);
    void * _ArchSAI_MAlloc(size_t size, ArchSAI_MemoryLocation location);
    void   _ArchSAI_Free(void *ptr, ArchSAI_MemoryLocation location);

    /*! \fn ARCHSAI_INT ArchSAI_GetPointerLocation(const void *ptr, ArchSAI_MemoryLocation *memory_location);
        \brief Returns where a pointer is allocated
        \param ptr Pointer to determine location.
        \param memory_location ArchSAI memory location.
    */

    ARCHSAI_INT ArchSAI_GetPointerLocation(const void *ptr, ArchSAI_MemoryLocation *memory_location);


    /*! \fn ArchSAI_Memset(void *ptr, ARCHSAI_INT value, size_t num, ArchSAI_MemoryLocation location);
        \brief Sets memory in given pointer to a value
        \param ptr
        \param value
        \param num
        \param location
    */
    void * ArchSAI_Memset(void *ptr, ARCHSAI_INT value, size_t num, ArchSAI_MemoryLocation location);

#endif // __ArchSAI_Memory_h__



