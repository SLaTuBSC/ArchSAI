
 /*! \file ArchSAI_Read.h
    \brief This file contains the utilities to read inputs for ArchSAI.

    ArchSAI system input utilities.
*/

 #ifndef ArchSAI_Read_h_
    #define ArchSAI_Read_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Matrix.h>
    #include <ArchSAI_Vector.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <mmio.h>

    /*! \fn ARCHSAI_INT ArchSAI_ReadSystem(ARCHSAI_MATCSR *A, ARCHSAI_VEC *b, ARCHSAI_HANDLER *handler)
        \brief Reads --mat and --rhs

        \param A Empty pointer to input matrix
        \param b Empty pointer to input rhs.
        \param context MPI Communicator.
        \param handler ArchSAI handler.
    */
    ARCHSAI_INT ArchSAI_ReadSystem(ARCHSAI_MATCSR *A, ARCHSAI_VEC *b, MPI_Comm context, ARCHSAI_HANDLER *handler);

    /*! \fn ARCHSAI_MATCSR ArchSAI_ReadMTXtoMatCSR(char* path)
        \brief Reads a matrix in path

        \param params ArchSAI set of parameters.
        \param context MPI Communicator.
    */
    ARCHSAI_MATCSR ArchSAI_ReadMTXtoMatCSR(ARCHSAI_PARAMS params, MPI_Comm context);

    /*! \fn ARCHSAI_VEC ArchSAI_ReadArray(char* path)
        \brief Reads a vector in path

        \param params ArchSAI set of parameters.
        \param context MPI Communicator.
    */
    ARCHSAI_VEC ArchSAI_ReadArray(ARCHSAI_PARAMS params, ARCHSAI_INT *rowdist, MPI_Comm context);


#endif // __ArchSAI_Read_h__




