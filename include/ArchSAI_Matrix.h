
 /*! \file ArchSAI_Matrix.h
    \brief This file contains the Matrix utilities and functions used by ArchSAI.

    ArchSAI matrix utilities.
*/

 #ifndef ArchSAI_Matrix_h_
    #define ArchSAI_Matrix_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Utils.h>
    #include <ArchSAI_Print.h>
    #include <ArchSAI_Random.h>
    #include <ArchSAI_LinAlg.h>
    #include <math.h>
    #include <omp.h>

    #ifdef ARCHSAI_WITH_METIS
        #include <metis.h>
    #endif


    /*! \fn ARCHSAI_MATCSR *ArchSAI_DAllocateMatCSR(int M, int N, int nz)
        \brief Allocates memory space for an ARCHSAI_MATCSR structure.

        \param M Rows of matrix
        \param N Columns of matrix
        \param nz Non-zeroes of matrix

    */
    ARCHSAI_MATCSR *ArchSAI_DAllocateMatCSR(ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz);

    /*! \fn ARCHSAI_MATCSR ArchSAI_DAllocCreateSymmetricMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz)
        \brief Given a lower/upper triangular matrix in COO format creates full symmetric ARCHSAI_MATCSR matrix.

        \param I Column values of triangular matrix
        \param J Row values of triangular matrix
        \param val Values of triangular matrix
        \param M Number of rows of triangular matrix
        \param N Number of columns of triangular matrix
        \param nz Number of non-zero entries of triangular matrix
    */
    ARCHSAI_MATCSR ArchSAI_DAllocCreateSymmetricMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz);

    /*! \fn ARCHSAI_MATCSR ArchSAI_DAllocCreateMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz);
        \brief Given a matrix in COO format creates ARCHSAI_MATCSR matrix.

        \param I Column values of triangular matrix
        \param J Row values of triangular matrix
        \param val Values of triangular matrix
        \param M Number of rows of triangular matrix
        \param N Number of columns of triangular matrix
        \param nz Number of non-zero entries of triangular matrix
    */
    ARCHSAI_MATCSR ArchSAI_DAllocCreateMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz);

    /*! \fn ARCHSAI_MATCSR ArchSAI_InitCopyBasePatternFromMat(ARCHSAI_MATCSR A)
        \brief Initializes and copies an ARCHSAI_MATCSR matrix with the pattern of A

        \param A Input matrix.

    */
    ARCHSAI_MATCSR ArchSAI_InitCopyBasePatternFromMat(ARCHSAI_MATCSR A);

    /*! \fn ARCHSAI_MATCSR ArchSAI_InitBasePatternFromMat(ARCHSAI_MATCSR A)
        \brief Initializes an ARCHSAI_MATCSR matrix with the sizes of the pattern of A

        \param A Input matrix.

    */
    ARCHSAI_MATCSR ArchSAI_InitBasePatternFromMat(ARCHSAI_MATCSR A);


    /*! \fn ARCHSAI_MATCSR ArchSAI_InitLTPPatternFromMat(ARCHSAI_MATCSR A)
        \brief Initializes and copies an ARCHSAI_MATCSR matrix with the lower triangular pattern of A

        \param A Input matrix.

    */
    ARCHSAI_MATCSR ArchSAI_InitLTPPatternFromMat(ARCHSAI_MATCSR A);


    /*! \fn ARCHSAI_INT ArchSAI_TransposeMatCSR(ARCHSAI_MATCSR *mat)
        \brief Transposes an ARCHSAI_MATCSR matrix

        \param mat Input matrix.

    */
    ARCHSAI_INT ArchSAI_TransposeMatCSR(ARCHSAI_MATCSR *mat);

    /*! \fn ARCHSAI_MATCSR ArchSAI_CreateTransposedMatCSR(ARCHSAI_MATCSR mat)
        \brief Transposes an ARCHSAI_MATCSR matrix in a new structure

        \param mat Input matrix.

    */
    ARCHSAI_MATCSR ArchSAI_CreateTransposedMatCSR(ARCHSAI_MATCSR ptr);

    ARCHSAI_INT ARCHSAI_MATCSRBalancer(ARCHSAI_MATCSR *A, ARCHSAI_INT **limits, ARCHSAI_INT nthreads, ARCHSAI_INT stride);

    /*! \fn ARCHSAI_DOUBLE ArchSAI_GetValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);
        \brief Searches for an entry in double-precision storage in an ARCHSAI_MATCSR matrix.

        \param start Pointer to provide extra information about search position
        \param row Row index
        \param col Column index
        \param mat Matrix to search

    */
    ARCHSAI_DOUBLE ArchSAI_GetValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);
    ARCHSAI_DOUBLE ArchSAI_GetValueMatCSR_nolim(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);

    /*! \fn ARCHSAI_FLOAT ArchSAI_GetFValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);
        \brief Searches for an entry in single-precision storage in an ARCHSAI_MATCSR matrix.

        \param start Pointer to provide extra information about search position
        \param row Row index
        \param col Column index
        \param mat Matrix to search

    */
    ARCHSAI_FLOAT ArchSAI_GetFValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);
    ARCHSAI_FLOAT ArchSAI_GetFValueMatCSR_nolim(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat);

    /*! \fn ARCHSAI_INT ArchSAI_IsColMatCSR(ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR *mat);
        \brief Searches for a pattern entry in an ARCHSAI_MATCSR matrix.

        \param row Row index
        \param col Column index
        \param mat Pattern to search

    */
    ARCHSAI_INT ArchSAI_IsColMatCSR(ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR *mat);

    /*! \fn void ArchSAI_DestroyMatCSR(ARCHSAI_MATCSR *ptr, ArchSAI_MemoryLocation location);
        \brief Frees a matrix structure.

        \param ptr Matrix to free
        \param location Memory location

    */
    void ArchSAI_DestroyMatCSR(ARCHSAI_MATCSR *ptr, ArchSAI_MemoryLocation location);
    void ArchSAI_InitMatCSR(ARCHSAI_MATCSR *ptr);

    /*! \fn ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm1(ARCHSAI_MATCSR mat);
        \brief Computes the norm-1 of a matrix

        \param mat Input matrix

    */
    ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm1(ARCHSAI_MATCSR mat);

    /*! \fn ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm2(ARCHSAI_MATCSR mat);
        \brief Computes the norm-2 of a matrix

        \param mat Input matrix

    */
    ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm2(ARCHSAI_MATCSR mat);

    /*! \fn ARCHSAI_MATCSR ArchSAI_AllocCreateDistMatCSRFromFullMatCSR(ARCHSAI_MATCSR FullMat, ARCHSAI_INT *rowdist, MPI_Comm context)
        \brief Creates a distributed matrix from a full matrix.

        \param FullMat Full matrix in MATCSR format.
        \param rowdist Array that specifies to which process belongs each row of the matrix.
        \param context MPI Communicator.
    */
    ARCHSAI_MATCSR ArchSAI_AllocCreateDistMatCSRFromFullMatCSR(ARCHSAI_MATCSR FullMat, MPI_Comm context);

    ARCHSAI_INT ArchSAI_GetDistMatCSRConverters(ARCHSAI_MATCSR *ptr);
    ARCHSAI_COMM *ArchSAI_CreateCommSchemeMatCSR(ARCHSAI_MATCSR ptr);

    void ArchSAI_DestroyCommScheme(ARCHSAI_COMM *ptr, ArchSAI_MemoryLocation location);
    void ArchSAI_DestroyDistMatCSRConverters(ARCHSAI_MATCSR *ptr);

    ARCHSAI_INT ArchSAI_ToLocalMatCSR(ARCHSAI_MATCSR *ptr);
    ARCHSAI_INT ArchSAI_ToGlobalMatCSR(ARCHSAI_MATCSR *ptr);

    ARCHSAI_INT *ArchSAI_CreateRowDistributionMatCSR(ARCHSAI_MATCSR ptr, ARCHSAI_PARAMS params, ARCHSAI_INT nprocs);
    ARCHSAI_COMM *ArchSAI_CopyCommSchemeMatCSR(ARCHSAI_MATCSR ptr);


    /*! \fn ARCHSAI_MATCSR ArchSAI_OuterMatrix(ARCHSAI_MATCSR ptr, ARCHSAI_MATCSR matrix, ARCHSAI_INT **gl_idx);
        \brief Obtains outer pattern (matrix of rows of other processes corresponding to local columns)

        \param ptr Input matrix
        \param matrix Matrix to search
        \param gl_idx Global indices of outer matrix rows.
    */
    ARCHSAI_MATCSR ArchSAI_OuterMatrix(ARCHSAI_MATCSR ptr, ARCHSAI_MATCSR matrix, ARCHSAI_INT **gl_idx);


    ARCHSAI_DOUBLE ArchSAI_MatCSR_MinSingularValue(ARCHSAI_MATCSR mat);
    ARCHSAI_DOUBLE ArchSAI_MatCSR_MaxSingularValue(ARCHSAI_MATCSR mat);
    ARCHSAI_DOUBLE ArchSAI_MatCSR_ConditionNumber(ARCHSAI_MATCSR mat);
    void ArchSAI_MatCSR_SVD(ARCHSAI_MATCSR mat, ARCHSAI_DOUBLE *S2);

#endif // __ArchSAI_Matrix_h__

