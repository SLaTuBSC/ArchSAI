#include <ArchSAI_Random.h>

static ARCHSAI_BIGINT MULTIPLIER  = 1366;
static ARCHSAI_BIGINT ADDEND      = 150889;
static ARCHSAI_BIGINT PMOD        = 714025;

ARCHSAI_BIGINT random_last = 0;
ARCHSAI_DOUBLE random_low, random_hi;

ARCHSAI_DOUBLE ArchSAI_dRandom(){

    ARCHSAI_BIGINT random_next;
    ARCHSAI_DOUBLE ret_val;
    //
    // compute an integer random number from zero to mod
    //
    random_next = (MULTIPLIER  * random_last + ADDEND)% PMOD;
    random_last = random_next;
    //
    // shift into preset range
    //
    ret_val = ((ARCHSAI_DOUBLE) random_next / (ARCHSAI_DOUBLE)PMOD)*(random_hi-random_low)+random_low;
    return ret_val;
}

ARCHSAI_INT ArchSAI_iRandom(){

    ARCHSAI_BIGINT random_next;
    ARCHSAI_INT ret_val;
    //
    // compute an integer random number from zero to mod
    //
    random_next = (MULTIPLIER  * random_last + ADDEND)% PMOD;
    random_last = random_next;
    //
    // shift into preset range
    //
    ret_val = ((ARCHSAI_INT) random_next / (ARCHSAI_INT)PMOD)*(random_hi-random_low)+random_low;
    return ret_val;
}

//
// set the seed and the range
//
void ArchSAI_Seed(ARCHSAI_DOUBLE low_in, ARCHSAI_DOUBLE hi_in){
    if(low_in < hi_in)
    {
        random_low = low_in;
        random_hi  = hi_in;
    }
    else
    {
        random_low = hi_in;
        random_hi  = low_in;
    }
    random_last = PMOD/ADDEND;  // just pick something
}
