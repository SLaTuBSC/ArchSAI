#include <ArchSAI_LinAlg.h>

// Single Precision

ARCHSAI_FLOAT archsai_sDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2){
    ARCHSAI_FLOAT res = 0.0;
    #pragma omp parallel for reduction(+:res)
    for (int i = 0; i < size; i++){res += v1[i] * v2[i];}
    return res;}

void archsai_sAxpy(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2){
    ARCHSAI_INT i, ix, iy;
    #pragma omp parallel for
    for(i = 0; i < size; i++){v2[i*inc2] += alpha*v1[i*inc1];}}

void archsai_sCopy(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2){
    #pragma omp parallel for
    for(int i = 0; i < size; i++){v2[i] = v1[i];}}

void archsai_sScal(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1){
    #pragma omp parallel for
    for(int i = 0; i < size; i++){v1[i] = alpha * v1[i];}}

void archsai_sCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_FLOAT *Bptr, ARCHSAI_FLOAT beta, ARCHSAI_FLOAT *Cptr){
    if ( strcmp(trans, "N") || strcmp(trans, "n") ) {
        #pragma omp parallel for
        for ( int i = 0; i < m; i++ ) {
            double c = Cptr[i];
            c = beta * c;
            for ( int v = avptr[i]; v < avptr[i+1]; v++ ) {
                int pos = avpos[v];
                double val = avval[v];
                c += alpha * val * Bptr[pos];
            }
            Cptr[i] = c;
        }
    } else if ( strcmp(trans, "T") || strcmp(trans, "t") ) {
        int count = 0;
        for ( int i = 0; i < n; i++, count++ ) {
            for ( int v = avptr[i]; v < avptr[i+1]; v++ ) {
                int pos = avpos[v];
                double val = avval[v];
                if ( ! count ) {
                    Cptr[pos] = alpha * val * Bptr[i] + beta * Cptr[pos];
                } else {
                    Cptr[pos] += alpha * val * Bptr[i];
                }
            }
        }
    }
}

// Double Precision

ARCHSAI_DOUBLE archsai_dDotP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2){
    ARCHSAI_DOUBLE res = 0.0;
    #pragma omp parallel for reduction(+:res)
    for (int i = 0; i < size; i++){res += v1[i] * v2[i];}
    return res;}

void archsai_dAxpy(ARCHSAI_INT size, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2){
    ARCHSAI_INT i, ix, iy;
    #pragma omp parallel for
    for(i = 0; i < size; i++){v2[i*inc2] += alpha*v1[i*inc1];}}

void archsai_dCopy(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2){
    #pragma omp parallel for
    for(int i = 0; i < size; i++){v2[i] = v1[i];}}

void archsai_dScal(ARCHSAI_INT size, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1){
    #pragma omp parallel for
    for(int i = 0; i < size; i++){v1[i] = alpha * v1[i];}}

void archsai_dCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_DOUBLE *Bptr, ARCHSAI_DOUBLE beta, ARCHSAI_DOUBLE *Cptr){
    if ( strcmp(trans, "N") || strcmp(trans, "n") ) {
        #pragma omp parallel for
        for ( int i = 0; i < m; i++ ) {
            double c = Cptr[i];
            c = beta * c;
            for ( int v = avptr[i]; v < avptr[i+1]; v++ ) {
                int pos = avpos[v];
                double val = avval[v];
                c += alpha * val * Bptr[pos];
            }
            Cptr[i] = c;
        }
    } else if ( strcmp(trans, "T") || strcmp(trans, "t") ) {
        int count = 0;
        for ( int i = 0; i < n; i++, count++ ) {
            for ( int v = avptr[i]; v < avptr[i+1]; v++ ) {
                int pos = avpos[v];
                double val = avval[v];
                if ( ! count ) {
                    Cptr[pos] = alpha * val * Bptr[i] + beta * Cptr[pos];
                } else {
                    Cptr[pos] += alpha * val * Bptr[i];
                }
            }
        }
    }
}











void archsai_dgels(ARCHSAI_CHAR *order, ARCHSAI_INT trans, ARCHSAI_INT rows, ARCHSAI_INT cols, ARCHSAI_INT nrhs, ARCHSAI_DOUBLE *mat, ARCHSAI_INT lda, ARCHSAI_DOUBLE *rhs, ARCHSAI_INT ldb){
    ArchSAI_ParPrintf(MPI_COMM_WORLD, "Own implementation of DGELS is missing. Please use LAPACK library.\n");
    ArchSAI_Error(10);
}
void archsai_sgels(ARCHSAI_CHAR *order, ARCHSAI_INT trans, ARCHSAI_INT rows, ARCHSAI_INT cols, ARCHSAI_INT nrhs, ARCHSAI_FLOAT *mat, ARCHSAI_INT lda, ARCHSAI_FLOAT *rhs, ARCHSAI_INT ldb){
    ArchSAI_ParPrintf(MPI_COMM_WORLD, "Own implementation of SGELS is missing. Please use LAPACK library.\n");
    ArchSAI_Error(10);
}
void archsai_dgesv(ARCHSAI_CHAR *layout, ARCHSAI_INT size, ARCHSAI_INT nrhs, ARCHSAI_DOUBLE *mat, ARCHSAI_INT lda, ARCHSAI_INT *ipiv, ARCHSAI_DOUBLE *rhs, ARCHSAI_INT ldb){
    ArchSAI_ParPrintf(MPI_COMM_WORLD, "Own implementation of DGESV is missing. Please use LAPACK library.\n");
    ArchSAI_Error(10);
}
void archsai_sgesv(ARCHSAI_CHAR *layout, ARCHSAI_INT size, ARCHSAI_INT nrhs, ARCHSAI_FLOAT *mat, ARCHSAI_INT lda, ARCHSAI_INT *ipiv, ARCHSAI_FLOAT *rhs, ARCHSAI_INT ldb){
    ArchSAI_ParPrintf(MPI_COMM_WORLD, "Own implementation of SGESV is missing. Please use LAPACK library.\n");
    ArchSAI_Error(10);
}



















////////////////////// WRAPPERS FOR DISTRIBUTED MEMORY ////////////////////////



void ArchSAI_CSRMV(ARCHSAI_CHAR *trans,  ARCHSAI_CHAR *matdescra, ARCHSAI_INT nprocs, ARCHSAI_DOUBLE alpha, ARCHSAI_MATCSR mat, ARCHSAI_DOUBLE *x, ARCHSAI_DOUBLE beta, ARCHSAI_DOUBLE *y){

    ARCHSAI_INT comms = mat.commscheme->nrecv + mat.commscheme->nsend;
    MPI_Request req[comms];

    if (nprocs > 1){

        for (int i = 0; i < mat.commscheme->nrecv; i++){
            ARCHSAI_INT size = mat.commscheme->recvptr[i + 2] - mat.commscheme->recvptr[i + 1];
            MPI_Irecv(x + mat.commscheme->recvptr[i + 1], size, ARCHSAI_MPI_DOUBLE, mat.commscheme->recvproc[i], 0, mat.comm, &req[i]);
        }

        for (int i = 0; i < mat.commscheme->sendsize; i++) {
            mat.commscheme->send[i] = x[mat.commscheme->sendpos[i]];
        }

        for (int i = 0; i < mat.commscheme->nsend; i++){
            ARCHSAI_INT size = mat.commscheme->sendptr[i + 1] - mat.commscheme->sendptr[i];
            MPI_Isend(mat.commscheme->send + mat.commscheme->sendptr[i], size, ARCHSAI_MPI_DOUBLE, mat.commscheme->sendproc[i], 0, mat.comm, &req[mat.commscheme->nrecv + i]);
        }

        MPI_Waitall(comms, req, MPI_STATUSES_IGNORE);
    }

    ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y);
}


ARCHSAI_DOUBLE ArchSAI_DOTP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, MPI_Comm comm, ARCHSAI_INT nprocs){

    ARCHSAI_DOUBLE global = 0.0;
    ARCHSAI_DOUBLE local = ArchSAI_DotP(size, v1, inc1, v2, inc2);

    if (nprocs == 1) return local;

    MPI_Allreduce(&local, &global, 1, ARCHSAI_MPI_DOUBLE, MPI_SUM, comm);

    return global;
}






















