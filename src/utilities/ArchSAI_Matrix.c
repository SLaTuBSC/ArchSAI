
#include <ArchSAI_Matrix.h>

ARCHSAI_MATCSR ArchSAI_DAllocCreateSymmetricMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz){

    ARCHSAI_MATCSR ptr;

    ArchSAI_InitMatCSR(&ptr);

    ptr.size   = M;
    ptr.nnz    = nz;
    ptr.nrows  = M;
    ptr.ncols  = M;
    ptr.sym    = 1;
    ptr.fulldiag = 0;
    ptr.gsize  = M;
    ptr.gnnz   = nz;
    ptr.gnrows = M;
    ptr.gncols = M;

    ptr.locgl = 1;

    ptr.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.size + 1, ArchSAI_MEMORY_HOST);

    // Loop over I/J to get entries in each row

    ARCHSAI_INT *full_rowentries = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.size, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < nz; i++){
        full_rowentries[col_indx[i]]++;
        if (col_indx[i] != row_indx[i]) full_rowentries[row_indx[i]]++;
    }

    // Create rowind vector

    for (int i = 0; i < ptr.size; i++){
        ptr.rowind[i + 1] = ptr.rowind[i] + full_rowentries[i];

    }

    ptr.nnz = ptr.rowind[ptr.size];

    if (ptr.nnz != ((nz - N) * 2 + N)) ptr.fulldiag = 0;
    else ptr.fulldiag = 1;

    ptr.colind = ArchSAI_TAlloc(ARCHSAI_INT, ptr.nnz, ArchSAI_MEMORY_HOST);
    ptr.val    = ArchSAI_TAlloc(ARCHSAI_DOUBLE, ptr.nnz, ArchSAI_MEMORY_HOST);
    ptr.fval   = NULL;

    // Loop over COO entries and add them to ptr

    ARCHSAI_INT *row_counter = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.size, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < nz; i++){

        ARCHSAI_INT row = col_indx[i];
        ARCHSAI_INT col = row_indx[i];
        ARCHSAI_DOUBLE vals = val[i];

        ptr.colind[ptr.rowind[row] + row_counter[row]] = col;
        ptr.val[ptr.rowind[row] + row_counter[row]] = vals;

        row_counter[row]++;

        if (col_indx[i] != row_indx[i]) {

            row = row_indx[i];
            col = col_indx[i];

            ptr.colind[ptr.rowind[row] + row_counter[row]] = col;
            ptr.val[ptr.rowind[row] + row_counter[row]] = vals;
            row_counter[row]++;

        }
    }

    ArchSAI_TFree(full_rowentries, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(row_counter, ArchSAI_MEMORY_HOST);
    return ptr;
}


ARCHSAI_MATCSR ArchSAI_DAllocCreateMatCSRfromCOO(ARCHSAI_INT *col_indx, ARCHSAI_INT *row_indx, ARCHSAI_DOUBLE *val, ARCHSAI_INT M, ARCHSAI_INT N, ARCHSAI_INT nz){

    ARCHSAI_MATCSR ptr;
    ArchSAI_InitMatCSR(&ptr);

    ptr.size   = N;
    ptr.nnz    = nz;
    ptr.nrows  = M;
    ptr.ncols  = N;
    ptr.sym    = 0;
    ptr.fulldiag = 0;
    ptr.gsize  = 0;
    ptr.gnnz   = 0;
    ptr.gnrows = 0;
    ptr.gncols = 0;
    ptr.locgl = 1;

    ptr.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nrows + 1, ArchSAI_MEMORY_HOST);
    ptr.colind = ArchSAI_TAlloc(ARCHSAI_INT, ptr.nnz, ArchSAI_MEMORY_HOST);
    ptr.val    = ArchSAI_TAlloc(ARCHSAI_DOUBLE, ptr.nnz, ArchSAI_MEMORY_HOST);
    ptr.fval   = NULL;

    ARCHSAI_INT i = 0;
    ARCHSAI_INT counter = 0;
    ARCHSAI_INT position = 0;

    ARCHSAI_INT *row_counter = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nrows + 1, ArchSAI_MEMORY_HOST);

    for (i = 0; i < nz; i++){
        row_counter[col_indx[i]]++;
    }

    for(i = 0; i < M; i++){
        ptr.rowind[i + 1] = row_counter[i];
        row_counter[i] = 0;
    }

    for(i = 0; i < M; i++){
        ptr.rowind[i + 1] = ptr.rowind[i + 1] + ptr.rowind[i];
    }

    for(i = 0; i < nz; i++){
        position = ptr.rowind[col_indx[i]] + row_counter[col_indx[i]];

        ptr.colind[position] = row_indx[i];
        ptr.val[position]    = val[i];

        row_counter[col_indx[i]]++;
    }


    ptr.fulldiag = 1;
    for (int i = 0; i < ptr.nrows; i++){
        if (ArchSAI_IsColMatCSR(i, i, &ptr) == -1){
            ptr.fulldiag = 0;
            break;
        }
    }

    return ptr;
}

ARCHSAI_INT *ArchSAI_CreateRowDistributionMatCSR(ARCHSAI_MATCSR ptr, ARCHSAI_PARAMS params, ARCHSAI_INT nprocs){

    ARCHSAI_INT *rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nrows, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT N = ptr.nrows;

    if (nprocs == 1) return rowdist;

    #ifdef ARCHSAI_WITH_METIS

    idx_t nvtxs = N;
    idx_t ncon = 1;
    idx_t *xadj = ptr.rowind;
    idx_t *adjncy = ptr.colind;
    idx_t *vwgt = NULL;
    idx_t *vsize = NULL;
    idx_t nparts = nprocs;
    idx_t objval;
    idx_t options[METIS_NOPTIONS];

    METIS_SetDefaultOptions(options);
//     options[METIS_OPTION_CONTIG] = 1;

    ARCHSAI_INT ierr = METIS_PartGraphRecursive(&nvtxs, &ncon, xadj, adjncy, vwgt, vsize, NULL, &nparts, NULL, NULL, options, &objval, rowdist);

    if (ierr != METIS_OK){
        ArchSAI_ParPrintf(ptr.comm, "METIS partitioning failed with error code %d\n", ierr);
        ArchSAI_Error(1);
    }

    #else

    for (int i = 0; i < nprocs; i++){
        ARCHSAI_INT begin = N * i / nprocs;
        ARCHSAI_INT end = N * (i + 1) / nprocs;
        if (i == nprocs - 1) end = N;
        for (int j = begin; j < end; j++){
            rowdist[j] = i;
        }
    }

    #endif

    return rowdist;
}


ARCHSAI_INT ARCHSAI_MATCSRBalancer(ARCHSAI_MATCSR *A, ARCHSAI_INT **limits, ARCHSAI_INT nthreads, ARCHSAI_INT stride){

    if (stride < 1) stride = 1;

    (*limits)   = ArchSAI_CTAlloc(ARCHSAI_INT, (nthreads + 1), ArchSAI_MEMORY_HOST);

    for (int i = 0; i < nthreads + 1; i++) (*limits)[i] = 0;

    int     elems = (ARCHSAI_INT) ceil(((ARCHSAI_DOUBLE) (A)->nnz) / ((ARCHSAI_DOUBLE) nthreads)),
            sum = 0,
            counter = 0,
            added = 0,
            top_limit = 0,
            bott_limit = 0;

    for (int i = 0; i < (A)->nrows; i+=stride){
        if ((i + stride) >= (A)->nrows) top_limit = (A)->nrows;
        else top_limit = (i + stride);
        bott_limit = i;
        sum += (A)->rowind[top_limit] - (A)->rowind[bott_limit];
        if ((sum >= elems) || (top_limit >= (A)->nrows)){
            counter++;
            (*limits)[counter] = top_limit;
            added += sum;
            elems = (ARCHSAI_INT) ceil(((ARCHSAI_DOUBLE) (A)->nnz - (ARCHSAI_DOUBLE) added) / ((ARCHSAI_DOUBLE) nthreads - (ARCHSAI_DOUBLE) counter));
            sum = 0;
        }
    }

    for (int i = counter; i < nthreads + 1; i++) (*limits)[i] = top_limit;

    return 0;
}






/*
    Data Structures memory allocation
*/


ARCHSAI_MATCSR ArchSAI_InitBasePatternFromMat(ARCHSAI_MATCSR A){

    ARCHSAI_MATCSR base;

    base.size   = A.size;
    base.nnz    = A.nnz;
    base.nrows  = A.nrows;
    base.ncols  = A.ncols;
    base.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, base.nrows + 1,   ArchSAI_MEMORY_HOST);
    base.colind = ArchSAI_TAlloc(ARCHSAI_INT,  base.nnz,         ArchSAI_MEMORY_HOST);

    return base;
}


ARCHSAI_MATCSR ArchSAI_InitCopyBasePatternFromMat(ARCHSAI_MATCSR ptr){

    ARCHSAI_MATCSR patt;

    ArchSAI_InitMatCSR(&patt);

    patt.size       = ptr.size;
    patt.nnz        = ptr.nnz;
    patt.nrows      = ptr.nrows;
    patt.ncols      = ptr.ncols;
    patt.sym        = ptr.sym;
    patt.fulldiag   = ptr.fulldiag;
    patt.gsize      = ptr.gsize;
    patt.gnnz       = ptr.gnnz;
    patt.gnrows     = ptr.gnrows;
    patt.gncols     = ptr.gncols;
    patt.locgl      = ptr.locgl;
    patt.comm       = ptr.comm;


    /* Allocate */
    patt.rowind = ArchSAI_CTAlloc(ARCHSAI_INT,  patt.nrows + 1,   ArchSAI_MEMORY_HOST);
    patt.colind = ArchSAI_CTAlloc(ARCHSAI_INT,  patt.nnz,         ArchSAI_MEMORY_HOST);
    patt.rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, patt.gnrows,     ArchSAI_MEMORY_HOST);
    patt.gl2loc = ArchSAI_CTAlloc(ARCHSAI_INT, patt.gnrows,     ArchSAI_MEMORY_HOST);
    patt.loc2gl = ArchSAI_CTAlloc(ARCHSAI_INT, patt.gnrows,     ArchSAI_MEMORY_HOST);


    /* Copy */
    ArchSAI_TMemcpy(patt.rowind, ptr.rowind,      ARCHSAI_INT,   patt.nrows + 1,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(patt.colind, ptr.colind,      ARCHSAI_INT,   patt.nnz,          ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(patt.rowdist, ptr.rowdist,    ARCHSAI_INT,   patt.gnrows,       ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(patt.gl2loc, ptr.gl2loc,      ARCHSAI_INT,   patt.gnrows,       ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(patt.loc2gl, ptr.loc2gl,      ARCHSAI_INT,   patt.gnrows,       ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

    /* Obtain comm scheme */
    patt.commscheme = ArchSAI_CopyCommSchemeMatCSR(ptr);

    return patt;
}


ARCHSAI_MATCSR ArchSAI_InitLTPPatternFromMat(ARCHSAI_MATCSR ptr){

    ARCHSAI_MATCSR patt;

    ArchSAI_InitMatCSR(&patt);

    patt.sym        = ptr.sym;
    patt.fulldiag   = ptr.fulldiag;
    patt.gsize      = ptr.gsize;
    patt.gnrows     = ptr.gnrows;
    patt.gncols     = ptr.gncols;
    patt.locgl      = ptr.locgl;
    patt.comm       = ptr.comm;

    patt.nrows      = ptr.nrows;
    patt.ncols      = ptr.ncols;
    patt.size       = ptr.size;

    patt.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, patt.nrows + 1,     ArchSAI_MEMORY_HOST);

    patt.rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, patt.gnrows,     ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(patt.rowdist, ptr.rowdist,    ARCHSAI_INT,   patt.gnrows,       ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

    if (patt.sym == 0) ArchSAI_ParPrintf(patt.comm, "\nWarning: Attempting to get LTP of Non-Symmetric Matrix.\n");
    if (patt.fulldiag == 0) ArchSAI_ParPrintf(patt.comm, "\nWarning: Attempting to get LTP of Matrix without full diagonal.\n");

    ARCHSAI_INT     *limits,
                    nthreads    = omp_get_max_threads(),
                    *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   patt.nrows + 1, ArchSAI_MEMORY_HOST),
                    *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_MATCSRBalancer(&ptr, &limits, nthreads, 1);

    #pragma omp parallel num_threads(nthreads)
    {
        int     thid              = omp_get_thread_num(),
                *colsthread       = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.rowind[limits[thid + 1]] - ptr.rowind[limits[thid]], ArchSAI_MEMORY_HOST),
                counter = 0,
                rowcount = 0;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
                if (ptr.loc2gl[ptr.colind[j]] <= ptr.loc2gl[i]){
                    colsthread[counter] = ptr.loc2gl[ptr.colind[j]];
                    counter++;
                    rowcount++;
                }
            }
            tmprows[i + 1] = rowcount;
            qsort(colsthread + counter - rowcount, rowcount, sizeof(ARCHSAI_INT), intcompare);

            rowcount = 0;
        }

        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < nthreads; i++) copypos[i + 1] = copypos[i + 1] + copypos[i];

            patt.nnz = copypos[nthreads];
            patt.colind = ArchSAI_CTAlloc(ARCHSAI_INT, patt.nnz,     ArchSAI_MEMORY_HOST);

        }

        ArchSAI_TMemcpy(patt.colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }


    ArchSAI_TMemcpy(patt.rowind, tmprows, ARCHSAI_INT, patt.nrows + 1, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < patt.nrows; i++) patt.rowind[i + 1] = patt.rowind[i + 1] + patt.rowind[i];

    MPI_Allreduce(&patt.nnz, &patt.gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, patt.comm);

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);

    patt.locgl      = 1;
    /* We have globally indexed LTP */

    /* Obtain converters and comm scheme */
    ArchSAI_GetDistMatCSRConverters(&patt);
    ArchSAI_ToLocalMatCSR(&patt);
    patt.commscheme = ArchSAI_CreateCommSchemeMatCSR(patt);

    return patt;
}




ARCHSAI_DOUBLE ArchSAI_GetValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE value = 0.0;
    ARCHSAI_INT init = *start == 0 ? mat.rowind[row] : *start;

    for (int i = init; i < mat.rowind[row+1]; i++){
        if (mat.colind[i] > col) break;
        if (mat.colind[i] == col){
            *start = i;
            value = mat.val[i];
            break;
        }
    }
    return value;
}

ARCHSAI_DOUBLE ArchSAI_GetValueMatCSR_nolim(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE value = 0.0;
    ARCHSAI_INT init = *start == 0 ? mat.rowind[row] : *start;

    for (int i = init; i < mat.rowind[row+1]; i++){
        if (mat.colind[i] == col){
            *start = i;
            value = mat.val[i];
            break;
        }
    }
    return value;
}

ARCHSAI_FLOAT ArchSAI_GetFValueMatCSR(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat){

    ARCHSAI_FLOAT value = 0.0;
    ARCHSAI_INT init = *start == 0 ? mat.rowind[row] : *start;

    for (int i = init; i < mat.rowind[row+1]; i++){
        if (mat.colind[i] > col) break;
        if (mat.colind[i] == col){
            value = mat.fval[i];
            break;
        }
    }
    return value;
}

ARCHSAI_FLOAT ArchSAI_GetFValueMatCSR_nolim(ARCHSAI_INT *start, ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR mat){

    ARCHSAI_FLOAT value = 0.0;
    ARCHSAI_INT init = *start == 0 ? mat.rowind[row] : *start;

    for (int i = init; i < mat.rowind[row+1]; i++){
//         if (mat.colind[i] > col) break;
        if (mat.colind[i] == col){
            value = mat.fval[i];
            break;
        }
    }
    return value;
}


ARCHSAI_INT ArchSAI_IsColMatCSR(ARCHSAI_INT row, ARCHSAI_INT col, ARCHSAI_MATCSR *mat){
    ARCHSAI_INT outcol = -1;
    for (int i = mat->rowind[row]; i < mat->rowind[row+1]; i++){
        if (mat->colind[i] > col) break;
        if (mat->colind[i] == col) {outcol = 1; break;}
    }
    return outcol;
}




ARCHSAI_INT ArchSAI_TransposeMatCSR(ARCHSAI_MATCSR *ptr){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr->comm, &rank);
    MPI_Comm_size(ptr->comm, &nprocs);

    /* Prepare buffers to send and receive info */

    ARCHSAI_INT *send_counter = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *recv_counter = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);

    /* Count entries to be sent into send_counter */

    for (int i = 0; i < ptr->nrows; i++){
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            ARCHSAI_INT gl_col = ptr->loc2gl[ptr->colind[j]];
            ARCHSAI_INT proc = ptr->rowdist[gl_col];

            send_counter[proc]++;
        }
    }

	/* Alltoall to get entries to receive */

    if (nprocs == 1) recv_counter[0] = send_counter[0];
	else MPI_Alltoall(send_counter, 1, ARCHSAI_MPI_INT, recv_counter, 1, ARCHSAI_MPI_INT, MPI_COMM_WORLD);

    /* Prepare buffers to send and receive info */

    ARCHSAI_INT send_buffer_size = 0;
    ARCHSAI_INT recv_buffer_size = recv_counter[rank]; // In this buffer I will also store local entries

    for (int i = 0; i < nprocs; i++){
        if (i != rank){
            send_buffer_size += send_counter[i];
            recv_buffer_size += recv_counter[i];
        }
    }

    /* I send row, col and value */
    ARCHSAI_DOUBLE  *send_buffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, send_buffer_size * 3, ArchSAI_MEMORY_HOST);
    ARCHSAI_DOUBLE  *recv_buffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, recv_buffer_size * 3, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT *send_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *recv_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);

    recv_ptr[0] = recv_counter[rank];

    send_counter[rank] = 0;
    recv_counter[rank] = 0;

    for (int i = 0; i < nprocs; i++){
            send_ptr[i + 1] = send_ptr[i] + send_counter[i];
            recv_ptr[i + 1] = recv_ptr[i] + recv_counter[i];
    }


    /* Put own entries in recv buffer already transposed in global index */
    /* And fill sending buffers */
    ARCHSAI_INT *proc_cnt = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT cnt = 0;
    ARCHSAI_INT data = 0;

    if (ptr->val){
        data = 2;
        for (int i = 0; i < ptr->nrows; i++){
            for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr->loc2gl[ptr->colind[j]];
                ARCHSAI_INT gl_row = ptr->loc2gl[i];
                ARCHSAI_DOUBLE val = ptr->val[j];
                ARCHSAI_INT proc = ptr->rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    recv_buffer[3 * cnt + 2] = (ARCHSAI_DOUBLE) val;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 2] = (ARCHSAI_DOUBLE) val;
                    proc_cnt[proc]++;
                }
            }
        }
    }
    else if (ptr->fval) {
        data = 1;
        for (int i = 0; i < ptr->nrows; i++){
            for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr->loc2gl[ptr->colind[j]];
                ARCHSAI_INT gl_row = ptr->loc2gl[i];
                ARCHSAI_FLOAT val = ptr->fval[j];
                ARCHSAI_INT proc = ptr->rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    recv_buffer[3 * cnt + 2] = (ARCHSAI_DOUBLE) val;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 2] = (ARCHSAI_DOUBLE) val;
                    proc_cnt[proc]++;
                }
            }
        }
    }
    else {
        for (int i = 0; i < ptr->nrows; i++){
            for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr->loc2gl[ptr->colind[j]];
                ARCHSAI_INT gl_row = ptr->loc2gl[i];
                ARCHSAI_INT proc = ptr->rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    proc_cnt[proc]++;
                }
            }
        }
    }


    /* Buffers are prepared: Comms */

    MPI_Request *req = ArchSAI_TAlloc(MPI_Request, 2*nprocs, ArchSAI_MEMORY_HOST);
    cnt = 0;

    for (int i = 0; i < nprocs; i++){
        if (recv_counter[i] > 0){
            ARCHSAI_INT size = (recv_ptr[i + 1] - recv_ptr[i]) * 3;
            MPI_Irecv(recv_buffer + recv_ptr[i] * 3, size, ARCHSAI_MPI_DOUBLE, i, 0, ptr->comm, &req[cnt]);
            cnt++;
        }
    }

    for (int i = 0; i < nprocs; i++){
        if (send_counter[i] > 0){
            ARCHSAI_INT size = (send_ptr[i + 1] - send_ptr[i]) * 3;
            MPI_Isend(send_buffer + send_ptr[i] * 3, size, ARCHSAI_MPI_DOUBLE, i, 0, ptr->comm, &req[cnt]);
            cnt++;
        }
    }

    MPI_Waitall(cnt, req, MPI_STATUSES_IGNORE);

    /* Each rank has now the transposed data in recv_buffer globally indexed */
    /* Rebuild G, generate converters and new comm_scheme */

    ARCHSAI_INT size = ptr->size;
    ARCHSAI_INT nnz = recv_buffer_size;
    ARCHSAI_INT nrows = ptr->nrows;
    ARCHSAI_INT ncols = ptr->ncols;
    ARCHSAI_INT sym = ptr->sym;
    ARCHSAI_INT fulldiag = ptr->fulldiag;
    ARCHSAI_INT gsize = ptr->gsize;
    ARCHSAI_INT gnnz = ptr->gnnz;
    ARCHSAI_INT gnrows = ptr->gnrows;
    ARCHSAI_INT gncols = ptr->gncols;
    ARCHSAI_INT locgl = 1;
    MPI_Comm comm = ptr->comm;

    ARCHSAI_INT *rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gnrows, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(rowdist, ptr->rowdist, ARCHSAI_INT,   ptr->gnrows,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);


    ArchSAI_DestroyMatCSR(ptr, ArchSAI_MEMORY_HOST);
    ArchSAI_InitMatCSR(ptr);

    ptr->size       = size;
    ptr->nnz        = nnz;
    ptr->nrows      = nrows;
    ptr->ncols      = ncols;
    ptr->sym        = sym;
    ptr->fulldiag   = fulldiag;
    ptr->gsize      = gsize;
    ptr->gnnz       = gnnz;
    ptr->gnrows     = gnrows;
    ptr->gncols     = gncols;
    ptr->locgl      = locgl;
    ptr->comm       = comm;

    ptr->rowind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->nrows + 1, ArchSAI_MEMORY_HOST);
    ptr->colind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->nnz, ArchSAI_MEMORY_HOST);
    ptr->rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gnrows, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(ptr->rowdist, rowdist, ARCHSAI_INT,   ptr->gnrows,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);


    if (data == 2) ptr->val = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ptr->nnz, ArchSAI_MEMORY_HOST);
    if (data == 1) ptr->fval = ArchSAI_CTAlloc(ARCHSAI_FLOAT, ptr->nnz, ArchSAI_MEMORY_HOST);

    /* Construct the rowind array */
    ARCHSAI_INT *rowconv = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gnrows, ArchSAI_MEMORY_HOST);

    cnt = 0;
    for (int i = 0; i < ptr->gnrows; i++){
        if (ptr->rowdist[i] == rank){
            rowconv[i] = cnt;
            cnt++;
        }
    }

    for (int i = 0; i < recv_buffer_size; i++){
        ARCHSAI_INT row = (ARCHSAI_INT) recv_buffer[i * 3];
        ptr->rowind[rowconv[row] + 1]++;
    }

    for (int i = 0; i < ptr->nrows; i++){
        ptr->rowind[i + 1] = ptr->rowind[i + 1] + ptr->rowind[i];
    }


    ARCHSAI_INT *rowpos = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->nrows, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < recv_buffer_size; i++){
        ARCHSAI_INT row = (ARCHSAI_INT) recv_buffer[i * 3];
        ARCHSAI_INT col = (ARCHSAI_INT) recv_buffer[i * 3 + 1];
        ARCHSAI_DOUBLE val = (ARCHSAI_DOUBLE) recv_buffer[i * 3 + 2];

        ptr->colind[ptr->rowind[rowconv[row]] + rowpos[rowconv[row]]] = col;
        if (data == 2) ptr->val[ptr->rowind[rowconv[row]] + rowpos[rowconv[row]]] = val;
        if (data == 1) ptr->fval[ptr->rowind[rowconv[row]] + rowpos[rowconv[row]]] = (ARCHSAI_FLOAT) val;

        rowpos[rowconv[row]]++;
    }


    ArchSAI_GetDistMatCSRConverters(ptr);
    ArchSAI_ToLocalMatCSR(ptr);
    ptr->commscheme = ArchSAI_CreateCommSchemeMatCSR(*ptr);

    ArchSAI_TFree(send_counter, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_counter, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(send_buffer, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_buffer, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(send_ptr, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_ptr, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(proc_cnt, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(rowdist, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(rowconv, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}




ARCHSAI_MATCSR ArchSAI_CreateTransposedMatCSR(ARCHSAI_MATCSR ptr){

    ARCHSAI_MATCSR transp;

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr.comm, &rank);
    MPI_Comm_size(ptr.comm, &nprocs);

    /* Prepare buffers to send and receive info */

    ARCHSAI_INT *send_counter = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *recv_counter = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);

    /* Count entries to be sent into send_counter */

    for (int i = 0; i < ptr.nrows; i++){
        for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
            ARCHSAI_INT gl_col = ptr.loc2gl[ptr.colind[j]];
            ARCHSAI_INT proc = ptr.rowdist[gl_col];

            send_counter[proc]++;
        }
    }

	/* Alltoall to get entries to receive */

    if (nprocs == 1) recv_counter[0] = send_counter[0];
	else MPI_Alltoall(send_counter, 1, ARCHSAI_MPI_INT, recv_counter, 1, ARCHSAI_MPI_INT, MPI_COMM_WORLD);

    /* Prepare buffers to send and receive info */

    ARCHSAI_INT send_buffer_size = 0;
    ARCHSAI_INT recv_buffer_size = ptr.nnz; // In this buffer I will also store local entries

    for (int i = 0; i < nprocs; i++){
        if (i != rank){
            send_buffer_size += send_counter[i];
            recv_buffer_size += recv_counter[i];
        }
    }

    /* I send row, col and value */
    ARCHSAI_DOUBLE  *send_buffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, send_buffer_size * 3, ArchSAI_MEMORY_HOST);
    ARCHSAI_DOUBLE  *recv_buffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, recv_buffer_size * 3, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT *send_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *recv_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);

    recv_ptr[0] = recv_counter[rank];

    send_counter[rank] = 0;
    recv_counter[rank] = 0;

    for (int i = 0; i < nprocs; i++){
            send_ptr[i + 1] = send_ptr[i] + send_counter[i];
            recv_ptr[i + 1] = recv_ptr[i] + recv_counter[i];
    }


    /* Put own entries in recv buffer already transposed in global index */
    /* And fill sending buffers */
    ARCHSAI_INT *proc_cnt = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT cnt = 0;
    ARCHSAI_INT data = 0;

    if (ptr.val){
        data = 2;
        for (int i = 0; i < ptr.nrows; i++){
            for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr.loc2gl[ptr.colind[j]];
                ARCHSAI_INT gl_row = ptr.loc2gl[i];
                ARCHSAI_DOUBLE val = ptr.val[j];
                ARCHSAI_INT proc = ptr.rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    recv_buffer[3 * cnt + 2] = (ARCHSAI_DOUBLE) val;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 2] = (ARCHSAI_DOUBLE) val;
                    proc_cnt[proc]++;
                }
            }
        }
    }
    else if (ptr.fval) {
        data = 1;
        for (int i = 0; i < ptr.nrows; i++){
            for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr.loc2gl[ptr.colind[j]];
                ARCHSAI_INT gl_row = ptr.loc2gl[i];
                ARCHSAI_FLOAT val = ptr.fval[j];
                ARCHSAI_INT proc = ptr.rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    recv_buffer[3 * cnt + 2] = (ARCHSAI_DOUBLE) val;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 2] = (ARCHSAI_DOUBLE) val;
                    proc_cnt[proc]++;
                }
            }
        }
    }
    else {
        for (int i = 0; i < ptr.nrows; i++){
            for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
                ARCHSAI_INT gl_col = ptr.loc2gl[ptr.colind[j]];
                ARCHSAI_INT gl_row = ptr.loc2gl[i];
                ARCHSAI_INT proc = ptr.rowdist[gl_col];
                if (proc == rank){
                    recv_buffer[3 * cnt + 0] = (ARCHSAI_DOUBLE) gl_col;
                    recv_buffer[3 * cnt + 1] = (ARCHSAI_DOUBLE) gl_row;
                    cnt++;
                }
                else {
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 0] = (ARCHSAI_DOUBLE) gl_col;
                    send_buffer[send_ptr[proc] * 3 + proc_cnt[proc] * 3 + 1] = (ARCHSAI_DOUBLE) gl_row;
                    proc_cnt[proc]++;
                }
            }
        }
    }


    /* Buffers are prepared: Comms */

    MPI_Request *req = ArchSAI_TAlloc(MPI_Request, 2*nprocs, ArchSAI_MEMORY_HOST);
    cnt = 0;

    for (int i = 0; i < nprocs; i++){
        if (recv_counter[i] > 0){
            ARCHSAI_INT size = (recv_ptr[i + 1] - recv_ptr[i]) * 3;
            MPI_Irecv(recv_buffer + recv_ptr[i] * 3, size, ARCHSAI_MPI_DOUBLE, i, 0, ptr.comm, &req[cnt]);
            cnt++;
        }
    }

    for (int i = 0; i < nprocs; i++){
        if (send_counter[i] > 0){
            ARCHSAI_INT size = (send_ptr[i + 1] - send_ptr[i]) * 3;
            MPI_Isend(send_buffer + send_ptr[i] * 3, size, ARCHSAI_MPI_DOUBLE, i, 0, ptr.comm, &req[cnt]);
            cnt++;
        }
    }

    MPI_Waitall(cnt, req, MPI_STATUSES_IGNORE);

    /* Each rank has now the transposed data in recv_buffer globally indexed */
    /* Build transp, generate converters and new comm_scheme */

    ArchSAI_InitMatCSR(&transp);

    transp.size = ptr.size;
    transp.nnz = recv_buffer_size;
    transp.nrows = ptr.nrows;
    transp.ncols = ptr.ncols;
    transp.sym = ptr.sym;
    transp.fulldiag = ptr.fulldiag;
    transp.gsize = ptr.gsize;
    transp.gnnz = ptr.gnnz;
    transp.gnrows = ptr.gnrows;
    transp.gncols = ptr.gncols;
    transp.locgl = 1;
    transp.comm = ptr.comm;

    transp.rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, transp.gnrows, ArchSAI_MEMORY_HOST);
    transp.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, transp.nrows + 1, ArchSAI_MEMORY_HOST);
    transp.colind = ArchSAI_CTAlloc(ARCHSAI_INT, transp.nnz, ArchSAI_MEMORY_HOST);
    transp.rowdist = ArchSAI_CTAlloc(ARCHSAI_INT, transp.gnrows, ArchSAI_MEMORY_HOST);

    ArchSAI_TMemcpy(transp.rowdist, ptr.rowdist, ARCHSAI_INT,   transp.gnrows,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

    if (data == 2) transp.val = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, transp.nnz, ArchSAI_MEMORY_HOST);
    if (data == 1) transp.fval = ArchSAI_CTAlloc(ARCHSAI_FLOAT, transp.nnz, ArchSAI_MEMORY_HOST);

    /* Construct the rowind array */
    ARCHSAI_INT *rowconv = ArchSAI_CTAlloc(ARCHSAI_INT, transp.gnrows, ArchSAI_MEMORY_HOST);

    cnt = 0;
    for (int i = 0; i < transp.gnrows; i++){
        if (transp.rowdist[i] == rank){
            rowconv[i] = cnt;
            cnt++;
        }
    }

    for (int i = 0; i < recv_buffer_size; i++){
        ARCHSAI_INT row = (ARCHSAI_INT) recv_buffer[i * 3];
        transp.rowind[rowconv[row] + 1]++;
    }

    for (int i = 0; i < transp.nrows; i++){
        transp.rowind[i + 1] = transp.rowind[i + 1] + transp.rowind[i];
    }

    ARCHSAI_INT *rowpos = ArchSAI_CTAlloc(ARCHSAI_INT, transp.nrows, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < recv_buffer_size; i++){
        ARCHSAI_INT row = (ARCHSAI_INT) recv_buffer[i * 3];
        ARCHSAI_INT col = (ARCHSAI_INT) recv_buffer[i * 3 + 1];
        ARCHSAI_DOUBLE val = (ARCHSAI_DOUBLE) recv_buffer[i * 3 + 2];

        transp.colind[transp.rowind[rowconv[row]] + rowpos[rowconv[row]]] = col;
        if (data == 2) transp.val[transp.rowind[rowconv[row]] + rowpos[rowconv[row]]] = val;
        if (data == 1) transp.fval[transp.rowind[rowconv[row]] + rowpos[rowconv[row]]] = (ARCHSAI_FLOAT) val;

        rowpos[rowconv[row]]++;
    }


    ArchSAI_GetDistMatCSRConverters(&transp);
    ArchSAI_ToLocalMatCSR(&transp);
    transp.commscheme = ArchSAI_CreateCommSchemeMatCSR(transp);

    ArchSAI_TFree(send_counter, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_counter, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(send_buffer, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_buffer, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(send_ptr, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recv_ptr, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(proc_cnt, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(rowconv, ArchSAI_MEMORY_HOST);

    return transp;
}



void ArchSAI_DestroyCommScheme(ARCHSAI_COMM *ptr, ArchSAI_MemoryLocation location){

    if (ptr) {
        if (ptr->sendproc)
            ArchSAI_TFree(ptr->sendproc, location);
        if (ptr->recvproc)
            ArchSAI_TFree(ptr->recvproc, location);
        if (ptr->sendptr)
            ArchSAI_TFree(ptr->sendptr, location);
        if (ptr->recvptr)
            ArchSAI_TFree(ptr->recvptr, location);
        if (ptr->sendpos)
            ArchSAI_TFree(ptr->sendpos, location);
        if (ptr->send)
            ArchSAI_TFree(ptr->send, location);

//         ArchSAI_TFree(ptr, location);
    }
}

void ArchSAI_DestroyDistMatCSRConverters(ARCHSAI_MATCSR *ptr){
    if (ptr) {
        if (ptr->gl2loc)
            ArchSAI_TFree(ptr->gl2loc, ArchSAI_MEMORY_HOST);
        if (ptr->loc2gl)
            ArchSAI_TFree(ptr->loc2gl, ArchSAI_MEMORY_HOST);
    }
}


void ArchSAI_InitMatCSR(ARCHSAI_MATCSR *ptr){
    ptr->size       = 0;
    ptr->nnz        = 0;
    ptr->nrows      = 0;
    ptr->ncols      = 0;
    ptr->rowind     = NULL;
    ptr->colind     = NULL;
    ptr->val        = NULL;
    ptr->fval       = NULL;
    ptr->sym        = 0;
    ptr->fulldiag   = 0;
    ptr->gsize      = 0;
    ptr->gnnz       = 0;
    ptr->gnrows     = 0;
    ptr->gncols     = 0;
    ptr->rowdist    = NULL;
    ptr->gl2loc     = NULL;
    ptr->loc2gl     = NULL;
    ptr->locgl      = 0;
    ptr->commscheme = NULL;
	ptr->format     = 0;
//     ptr->comm       = MPI_COMM_NULL;
}

void ArchSAI_DestroyMatCSR(ARCHSAI_MATCSR *ptr, ArchSAI_MemoryLocation location){

    if (ptr) {
        if (ptr->rowind != NULL)
           ArchSAI_TFree(ptr->rowind, location);
        if (ptr->colind != NULL)
            ArchSAI_TFree(ptr->colind, location);
        if (ptr->val!= NULL)
            ArchSAI_TFree(ptr->val, location);
        if (ptr->fval!= NULL)
            ArchSAI_TFree(ptr->fval, location);

        if (ptr->rowind!= NULL)
            ArchSAI_TFree(ptr->rowind, location);
        if (ptr->gl2loc!= NULL)
            ArchSAI_TFree(ptr->fval, location);
        if (ptr->loc2gl!= NULL)
            ArchSAI_TFree(ptr->fval, location);

        ArchSAI_DestroyCommScheme(ptr->commscheme, location);
    }

}

ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm1(ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE  norm = 0.0,
                    local_norm = 0.0,
                    rowsum = 0.0;

    #pragma omp parallel for private(rowsum) reduction(max:local_norm)
    for (int i = 0; i < mat.nrows; i++){
        rowsum = 0.0;
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            rowsum += ArchSAI_Abs(mat.val[j]);
        }
        if (rowsum > local_norm){
            local_norm = rowsum;
        }
    }
    MPI_Allreduce(&local_norm, &norm, 1, ARCHSAI_MPI_DOUBLE, MPI_MAX, mat.comm);

    return norm;
}

ARCHSAI_DOUBLE ArchSAI_MatCSR_Norm2(ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE  norm = 0.0,
                    local_norm = 0.0,
                    rowsum = 0.0;

    #pragma omp parallel for private(rowsum) reduction(max:local_norm)
    for (int i = 0; i < mat.nrows; i++){
        rowsum = 0.0;
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            rowsum += mat.val[j] * mat.val[j];
        }
        if (rowsum > local_norm){
            local_norm = rowsum;
        }
    }
    MPI_Allreduce(&local_norm, &norm, 1, ARCHSAI_MPI_DOUBLE, MPI_MAX, mat.comm);

    return ArchSAI_Sqrt(norm);
}

// Function to compute the dot product of two vectors
double dotProduct(double* vec1, double* vec2, int size) {
    double result = 0.0;
    for (int i = 0; i < size; i++) {
        result += vec1[i] * vec2[i];
    }
    return result;
}

// Function to normalize a vector
void normalizeVector(double* vec, int size) {
    double norm = 0.0;
    for (int i = 0; i < size; i++) {
        norm += vec[i] * vec[i];
    }
    norm = sqrt(norm);
    for (int i = 0; i < size; i++) {
        vec[i] /= norm;
    }
}


ARCHSAI_DOUBLE ArchSAI_MatCSR_MaxSingularValue(ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE max_sv = 0.0;

    ARCHSAI_DOUBLE *densemat = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat.gnrows * mat.gncols, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < mat.nrows; i++){
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            densemat[i * mat.ncols + mat.colind[j]] = mat.val[j];
        }
    }

    ARCHSAI_INT M = mat.nrows;
    ARCHSAI_INT N = mat.ncols;
    ARCHSAI_INT LDA = N;
    ARCHSAI_INT LDU = M;
    ARCHSAI_INT LDVT = N;
    ARCHSAI_INT info;

    ARCHSAI_DOUBLE  *superb = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ArchSAI_Min(M,N)-1, ArchSAI_MEMORY_HOST),
                    *s = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, N, ArchSAI_MEMORY_HOST),
                    *u = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDU*M, ArchSAI_MEMORY_HOST),
                    *vt = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDVT*N, ArchSAI_MEMORY_HOST);

    info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', M, N, densemat, LDA, s, u, LDU, vt, LDVT, superb);

    max_sv = s[0];

    ArchSAI_TFree(densemat, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(superb, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(s, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(u, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(vt, ArchSAI_MEMORY_HOST);

    return max_sv;
}

ARCHSAI_DOUBLE ArchSAI_MatCSR_MinSingularValue(ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE min_sv = 0.0;

    ARCHSAI_DOUBLE *densemat = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat.gnrows * mat.gncols, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < mat.nrows; i++){
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            densemat[i * mat.ncols + mat.colind[j]] = mat.val[j];
        }
    }

    ARCHSAI_INT M = mat.nrows;
    ARCHSAI_INT N = mat.ncols;
    ARCHSAI_INT LDA = N;
    ARCHSAI_INT LDU = M;
    ARCHSAI_INT LDVT = N;
    ARCHSAI_INT info;

    ARCHSAI_DOUBLE  *superb = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ArchSAI_Min(M,N)-1, ArchSAI_MEMORY_HOST),
                    *s = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, N, ArchSAI_MEMORY_HOST),
                    *u = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDU*M, ArchSAI_MEMORY_HOST),
                    *vt = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDVT*N, ArchSAI_MEMORY_HOST);

    info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', M, N, densemat, LDA, s, u, LDU, vt, LDVT, superb);

    min_sv = s[N - 1];

    ArchSAI_TFree(densemat, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(superb, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(s, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(u, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(vt, ArchSAI_MEMORY_HOST);

    return min_sv;
}

ARCHSAI_DOUBLE ArchSAI_MatCSR_ConditionNumber(ARCHSAI_MATCSR mat){

    ARCHSAI_DOUBLE min_sv = 0.0;
    ARCHSAI_DOUBLE max_sv = 0.0;

    ARCHSAI_DOUBLE *densemat = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat.gnrows * mat.gncols, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < mat.nrows; i++){
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            densemat[i * mat.ncols + mat.colind[j]] = mat.val[j];
        }
    }

    ARCHSAI_INT M = mat.nrows;
    ARCHSAI_INT N = mat.ncols;
    ARCHSAI_INT LDA = N;
    ARCHSAI_INT LDU = M;
    ARCHSAI_INT LDVT = N;
    ARCHSAI_INT info;

    ARCHSAI_DOUBLE  *superb = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ArchSAI_Min(M,N)-1, ArchSAI_MEMORY_HOST),
                    *s = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, N, ArchSAI_MEMORY_HOST),
                    *u = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDU*M, ArchSAI_MEMORY_HOST),
                    *vt = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, LDVT*N, ArchSAI_MEMORY_HOST);

    info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', M, N, densemat, LDA, s, u, LDU, vt, LDVT, superb);

    min_sv = s[N - 1];
    max_sv = s[0];

    ArchSAI_TFree(densemat, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(superb, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(s, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(u, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(vt, ArchSAI_MEMORY_HOST);

    return max_sv / min_sv;
}

void ArchSAI_MatCSR_SVD(ARCHSAI_MATCSR mat, ARCHSAI_DOUBLE *S2) {

    ARCHSAI_INT n = mat.nrows;
    ARCHSAI_DOUBLE **A = ArchSAI_CTAlloc(ARCHSAI_DOUBLE*, 2*n, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < 2*n; i++){
        A[i] = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, n, ArchSAI_MEMORY_HOST);
    }
    for (int i = 0; i < mat.nrows; i++){
        for (int j = mat.rowind[i]; j < mat.rowind[i + 1]; j++){
            A[i][mat.colind[j]] = mat.val[j];
        }
    }

    int  i, j, k, EstColRank = n, RotCount = n, SweepCount = 0, slimit = (n<120) ? 30 : n/4;
    double eps = 1e-15, e2 = 10.0*n*eps*eps, tol = 0.1*eps, vt, p, x0, y0, q, r, c0, s0, d1, d2;

    for (i=0; i<n; i++) { // V = I
        for (j=0; j<n; j++)
            A[n+i][j] = 0.0;
        A[n+i][i] = 1.0;
    }

    while (RotCount != 0 && SweepCount++ <= slimit) {
        RotCount = EstColRank*(EstColRank-1)/2;
//         #pragma omp parallel for private(i, j, k, p, q, r, x0, y0, vt, c0, s0, d1, d2)
        for (j=0; j<EstColRank-1; j++) {
            for (k=j+1; k<EstColRank; k++) {
                p = q = r = 0.0;
                for (i=0; i<n; i++) {
                    x0 = A[i][j];
                    y0 = A[i][k];
                    p += x0*y0;
                    q += x0*x0;
                    r += y0*y0;
                }
                S2[j] = q;
                S2[k] = r;
                if (q >= r) {
                    if (q<=e2*S2[0] || fabs(p)<=tol*q)
                        RotCount--;
                    else {
                        p /= q;
                        r = 1.0-r/q;
                        vt = sqrt(4.0*p*p+r*r);
                        c0 = sqrt(0.5*(1.0+r/vt));
                        s0 = p/(vt*c0);
                        for (i=0; i<2*n; i++) {
                            d1 = A[i][j];
                            d2 = A[i][k];
                            A[i][j] = d1*c0+d2*s0;
                            A[i][k] = -d1*s0+d2*c0;
                        }
                    }
                }
                else {
                    p /= r;
                    q = q/r-1.0;
                    vt = sqrt(4.0*p*p+q*q);
                    s0 = sqrt(0.5*(1.0-q/vt));
                    if (p<0.0)
                        s0 = -s0;
                    c0 = p/(vt*s0);
                    for (i=0; i<2*n; i++) {
                        d1 = A[i][j];
                        d2 = A[i][k];
                        A[i][j] = d1*c0+d2*s0;
                        A[i][k] = -d1*s0+d2*c0;
                    }
                }
            }
            while (EstColRank>2 && S2[EstColRank-1]<=S2[0]*tol+tol*tol)
                EstColRank--;
        }
    }
    if (SweepCount > slimit)
        printf("Warning: Reached maximum number of sweeps (%d) in SVD routine...\n", slimit);

//     for (int i = 0; i < n; i++){
//         printf("%lf\n", ArchSAI_Sqrt(S2[i]));
//     }
    ArchSAI_TFree(A, ArchSAI_MEMORY_HOST);
}

ARCHSAI_INT ArchSAI_GetDistMatCSRConverters(ARCHSAI_MATCSR *ptr){


    if (!ptr->rowdist){
        ArchSAI_ErrorWithMsg(4, "Error: MatCSR has no row distribution: MatCSR.rowdist");
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr->comm, &rank);
    MPI_Comm_size(ptr->comm, &nprocs);

    /* Each process has a local matrix with global indices */
    /* Create converters and comm scheme */

    ptr->gl2loc = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gncols,       ArchSAI_MEMORY_HOST);
    ptr->loc2gl = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gncols,       ArchSAI_MEMORY_HOST); // Size can be reduced to maximum of recproff TODO

    /* 1st check which columns has each process */
    ARCHSAI_INT *usedglcols = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gnrows, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < ptr->nrows; i++){
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            usedglcols[ptr->colind[j]] = 1;
        }
    }

    /* Here we have a gl2loc converter for local entries and the amount of rows per process */
    ARCHSAI_INT *proc_offsets = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < ptr->gnrows; i++){
        ptr->gl2loc[i] = -1;
        if (ptr->rowdist[i] == rank) ptr->gl2loc[i] = proc_offsets[ptr->rowdist[i]];
        proc_offsets[ptr->rowdist[i]]++;
    }

    /* Find how many entries each process has to receive from other processes, i.e. halo update size */
    /* and create an offset vector */
    ARCHSAI_INT *recvproc = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *recproff = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < ptr->nrows; i++){
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            if ((ptr->rowdist[ptr->colind[j]] != rank) && (usedglcols[ptr->colind[j]] == 1)) {
                recvproc[ptr->rowdist[ptr->colind[j]]]++;
                usedglcols[ptr->colind[j]]++;
            }
        }
    }

    ARCHSAI_INT initoff = ptr->nrows;
    for (int i = 0; i < nprocs; i++){
        if (i != rank){
            recproff[i] = initoff;
            initoff += recvproc[i];
        }
    }


    /* Find how many entries each proc has to send to each other */
    ARCHSAI_INT *sendproc = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);

    if (nprocs == 1) sendproc[0] = recvproc[0];
	else MPI_Alltoall(recvproc, 1, ARCHSAI_MPI_INT, sendproc, 1, ARCHSAI_MPI_INT, MPI_COMM_WORLD);

    /* Obtain indices that have to be sent to each process */
    /* First get which process has each row I need */
    ARCHSAI_INT *rowowner = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->gnrows, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < ptr->gnrows; i++) rowowner[i] = -1;
    for (int i = 0; i < ptr->nrows; i++){
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            if (ptr->rowdist[ptr->colind[j]] != rank){
                rowowner[ptr->colind[j]] = ptr->rowdist[ptr->colind[j]];
            }
        }
    }

    ptr->ncols = 0;
    /* Global 2 local converter for halo */
    for (int i = 0; i < ptr->gnrows; i++){
        if (rowowner[i] != -1) {
            ptr->gl2loc[i] = recproff[rowowner[i]];
            recproff[rowowner[i]]++;
            ptr->ncols++;
        }
    }

    ptr->ncols += ptr->nrows;

    /* Obtain local 2 global converter */
    for (int i = 0; i < ptr->gnrows; i++) ptr->loc2gl[i] = -1;
    for (int i = 0; i < ptr->gnrows; i++){
        if (ptr->gl2loc[i] != -1) ptr->loc2gl[ptr->gl2loc[i]] = i;
    }

    ArchSAI_TFree(usedglcols, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(proc_offsets, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recvproc, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(recproff, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(sendproc, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(rowowner, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_ToLocalMatCSR(ARCHSAI_MATCSR *ptr){

    if (!ptr->rowdist){
        ArchSAI_ErrorWithMsg(4, "Error: MatCSR has no Global to Local converter: MatCSR.gl2loc");
    }

    if (ptr->locgl == 0) {
        ArchSAI_printf("Warning: Converting to local an already local matrix. Skipping.\n");
        return archsai_error_flag;
    }

    ptr->locgl = 0;

    /* Reindex entries to local and reorder */
    // 1st reindex to local and get max entries per row
    ARCHSAI_INT maxrow = 0;
    for (int i = 0; i < ptr->nrows; i++){
        if (ptr->rowind[i + 1] - ptr->rowind[i] > maxrow) maxrow = ptr->rowind[i + 1] - ptr->rowind[i];
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            ptr->colind[j] = ptr->gl2loc[ptr->colind[j]];
        }
    }


    if (ptr->val){
        // Reorder -> colind and val must be reordered both
        #pragma omp parallel
        {
            ARCHSAI_INT **pointers = ArchSAI_CTAlloc(ARCHSAI_INT*, maxrow, ArchSAI_MEMORY_HOST);
            ARCHSAI_INT k = 0, l = 0;

            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){

                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];

                /* Store pointers to column indices */
                for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                    pointers[j - ptr->rowind[i]] = &ptr->colind[j];
                }

                /* Sort pointers */
                qsort(pointers, rowelems, sizeof(ARCHSAI_INT*), intpointercompare);

                /* Reorder columns and values */
                for (int j = 0; j < rowelems; j++){
                    if (j != pointers[j] - &ptr->colind[ptr->rowind[i]]) {
                        ARCHSAI_INT col = ptr->colind[j + ptr->rowind[i]];
                        ARCHSAI_DOUBLE val = ptr->val[j + ptr->rowind[i]];
                        k = j;

                        while (j != (l = pointers[k] - &ptr->colind[ptr->rowind[i]])){
                            ptr->colind[k + ptr->rowind[i]] = ptr->colind[l + ptr->rowind[i]];
                            ptr->val[k + ptr->rowind[i]] = ptr->val[l + ptr->rowind[i]];
                            pointers[k] = &ptr->colind[k + ptr->rowind[i]];
                            k = l;
                        }
                        ptr->colind[k + ptr->rowind[i]] = col;
                        ptr->val[k + ptr->rowind[i]] = val;
                        pointers[k] = &ptr->colind[k + ptr->rowind[i]];

                    }
                }
            }

            ArchSAI_TFree(pointers, ArchSAI_MEMORY_HOST);
        }
    }
    else if (ptr->fval){
        // Reorder -> colind and val must be reordered both
        #pragma omp parallel
        {
            ARCHSAI_INT **pointers = ArchSAI_CTAlloc(ARCHSAI_INT*, maxrow, ArchSAI_MEMORY_HOST);
            ARCHSAI_INT k = 0, l = 0;

            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){

                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];

                /* Store pointers to column indices */
                for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                    pointers[j - ptr->rowind[i]] = &ptr->colind[j];
                }

                /* Sort pointers */
                qsort(pointers, rowelems, sizeof(ARCHSAI_INT*), intpointercompare);

                /* Reorder columns and values */
                for (int j = 0; j < rowelems; j++){
                    if (j != pointers[j] - &ptr->colind[ptr->rowind[i]]) {
                        ARCHSAI_INT col = ptr->colind[j + ptr->rowind[i]];
                        ARCHSAI_FLOAT val = ptr->fval[j + ptr->rowind[i]];
                        k = j;

                        while (j != (l = pointers[k] - &ptr->colind[ptr->rowind[i]])){
                            ptr->colind[k + ptr->rowind[i]] = ptr->colind[l + ptr->rowind[i]];
                            ptr->fval[k + ptr->rowind[i]] = ptr->fval[l + ptr->rowind[i]];
                            pointers[k] = &ptr->colind[k + ptr->rowind[i]];
                            k = l;
                        }
                        ptr->colind[k + ptr->rowind[i]] = col;
                        ptr->fval[k + ptr->rowind[i]] = val;
                        pointers[k] = &ptr->colind[k + ptr->rowind[i]];

                    }
                }
            }
            ArchSAI_TFree(pointers, ArchSAI_MEMORY_HOST);
        }
    }
    else {
        // Reorder -> colind must be reordered alone if no values in pattern
        #pragma omp parallel
        {
            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){
                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];
                qsort(ptr->colind + ptr->rowind[i], rowelems, sizeof(ARCHSAI_INT), intcompare);
            }
        }
    }

    return archsai_error_flag;
}


ARCHSAI_INT ArchSAI_ToGlobalMatCSR(ARCHSAI_MATCSR *ptr){

    if (!ptr->rowdist){
        ArchSAI_ErrorWithMsg(4, "Error: MatCSR has no Local to Global converter: MatCSR.loc2gl");
    }

    if (ptr->locgl == 1) {
        ArchSAI_printf("Warning: Converting to global an already global matrix. Skipping.\n");
        return archsai_error_flag;
    }

    ptr->locgl = 1;

    /* Reindex entries to local and reorder */
    // 1st reindex to local and get max entries per row
    ARCHSAI_INT maxrow = 0;
    for (int i = 0; i < ptr->nrows; i++){
        if (ptr->rowind[i + 1] - ptr->rowind[i] > maxrow) maxrow = ptr->rowind[i + 1] - ptr->rowind[i];
        for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
            ptr->colind[j] = ptr->loc2gl[ptr->colind[j]];
        }
    }


    if (ptr->val){
        // Reorder -> colind and val must be reordered both

        #pragma omp parallel
        {
            ARCHSAI_INT **pointers = ArchSAI_CTAlloc(ARCHSAI_INT*, maxrow, ArchSAI_MEMORY_HOST);
            ARCHSAI_INT k = 0, l = 0;

            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){

                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];

                /* Store pointers to column indices */
                for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                    pointers[j - ptr->rowind[i]] = &ptr->colind[j];
                }

                /* Sort pointers */
                qsort(pointers, rowelems, sizeof(ARCHSAI_INT*), intpointercompare);

                /* Reorder columns and values */
                for (int j = 0; j < rowelems; j++){
                    if (j != pointers[j] - &ptr->colind[ptr->rowind[i]]) {
                        ARCHSAI_INT col = ptr->colind[j + ptr->rowind[i]];
                        ARCHSAI_DOUBLE val = ptr->val[j + ptr->rowind[i]];
                        k = j;

                        while (j != (l = pointers[k] - &ptr->colind[ptr->rowind[i]])){
                            ptr->colind[k + ptr->rowind[i]] = ptr->colind[l + ptr->rowind[i]];
                            ptr->val[k + ptr->rowind[i]] = ptr->val[l + ptr->rowind[i]];
                            pointers[k] = &ptr->colind[k + ptr->rowind[i]];
                            k = l;
                        }
                        ptr->colind[k + ptr->rowind[i]] = col;
                        ptr->val[k + ptr->rowind[i]] = val;
                        pointers[k] = &ptr->colind[k + ptr->rowind[i]];

                    }
                }
            }
            ArchSAI_TFree(pointers, ArchSAI_MEMORY_HOST);
        }
    }
    if (ptr->fval){
        // Reorder -> colind and val must be reordered both

        #pragma omp parallel
        {
            ARCHSAI_INT **pointers = ArchSAI_CTAlloc(ARCHSAI_INT*, maxrow, ArchSAI_MEMORY_HOST);
            ARCHSAI_INT k = 0, l = 0;

            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){

                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];

                /* Store pointers to column indices */
                for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                    pointers[j - ptr->rowind[i]] = &ptr->colind[j];
                }

                /* Sort pointers */
                qsort(pointers, rowelems, sizeof(ARCHSAI_INT*), intpointercompare);

                /* Reorder columns and values */
                for (int j = 0; j < rowelems; j++){
                    if (j != pointers[j] - &ptr->colind[ptr->rowind[i]]) {
                        ARCHSAI_INT col = ptr->colind[j + ptr->rowind[i]];
                        ARCHSAI_FLOAT val = ptr->fval[j + ptr->rowind[i]];
                        k = j;

                        while (j != (l = pointers[k] - &ptr->colind[ptr->rowind[i]])){
                            ptr->colind[k + ptr->rowind[i]] = ptr->colind[l + ptr->rowind[i]];
                            ptr->fval[k + ptr->rowind[i]] = ptr->fval[l + ptr->rowind[i]];
                            pointers[k] = &ptr->colind[k + ptr->rowind[i]];
                            k = l;
                        }
                        ptr->colind[k + ptr->rowind[i]] = col;
                        ptr->fval[k + ptr->rowind[i]] = val;
                        pointers[k] = &ptr->colind[k + ptr->rowind[i]];

                    }
                }
            }
            ArchSAI_TFree(pointers, ArchSAI_MEMORY_HOST);
        }
    }
    else {
        // Reorder -> colind must be reordered alone if no values in pattern
        #pragma omp parallel
        {
            #pragma omp for
            for (int i = 0; i < ptr->nrows; i++){
                ARCHSAI_INT rowelems = ptr->rowind[i + 1] - ptr->rowind[i];
                qsort(ptr->colind + ptr->rowind[i], rowelems, sizeof(ARCHSAI_INT), intcompare);
            }
        }
    }



    return archsai_error_flag;
}


ARCHSAI_MATCSR ArchSAI_AllocCreateDistMatCSRFromFullMatCSR(ARCHSAI_MATCSR FullMat, MPI_Comm context){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(context, &rank);
    MPI_Comm_size(context, &nprocs);

    ARCHSAI_MATCSR ptr;

    ArchSAI_InitMatCSR(&ptr);

    ptr.comm        = context;

    ptr.sym         = FullMat.sym;
    ptr.fulldiag    = FullMat.fulldiag;
    ptr.gnnz        = FullMat.nnz;
    ptr.gsize       = FullMat.size;
    ptr.gnrows      = FullMat.nrows;
    ptr.gncols      = FullMat.ncols;
    ptr.nnz         = 0;
    ptr.size        = 0;
    ptr.nrows       = 0;
    ptr.ncols       = 0;
    ptr.locgl       = 1;

    ptr.rowdist     = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.gnrows, ArchSAI_MEMORY_HOST);
    ArchSAI_TMemcpy(ptr.rowdist, FullMat.rowdist, ARCHSAI_INT,   ptr.gnrows,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);


    /* Get number of rows of local matrix and local entries */

    for (int i = 0; i < ptr.gnrows; i++){
        if (ptr.rowdist[i] == rank) {
            ptr.nrows++;
            ptr.nnz = ptr.nnz + FullMat.rowind[i + 1] - FullMat.rowind[i];
        }
    }

    /* Allocate memory */

    ptr.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nrows + 1,    ArchSAI_MEMORY_HOST);
    ptr.colind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nnz,          ArchSAI_MEMORY_HOST);
    ptr.val    = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ptr.nnz,       ArchSAI_MEMORY_HOST);

    /* Loop over FullMat and get each rank local part */

    ARCHSAI_INT indcntr = 0;
    ARCHSAI_INT rowcntr = 1;

    for (int i = 0; i < FullMat.nrows; i++){
        if (ptr.rowdist[i] == rank){
            for (int j = FullMat.rowind[i]; j < FullMat.rowind[i + 1]; j++){
                ptr.colind[indcntr] = FullMat.colind[j];
                ptr.val[indcntr] = FullMat.val[j];
                indcntr++;
            }
            ptr.rowind[rowcntr] = indcntr;
            rowcntr++;
        }
    }

    ArchSAI_GetDistMatCSRConverters(&ptr);
    ArchSAI_ToLocalMatCSR(&ptr);
    ptr.commscheme = ArchSAI_CreateCommSchemeMatCSR(ptr);

    return ptr;
}


ARCHSAI_COMM *ArchSAI_CreateCommSchemeMatCSR(ARCHSAI_MATCSR ptr){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr.comm, &rank);
    MPI_Comm_size(ptr.comm, &nprocs);

    ARCHSAI_COMM *comm = ArchSAI_CTAlloc(ARCHSAI_COMM, 1, ArchSAI_MEMORY_HOST);

    comm->nrecv = 0; // Number of sends
    comm->nsend = 0; // Number of recvs

    comm->sendsize = 0; // Size of send buffer
    comm->recvsize = 0; // Size of recv buffer

    comm->sendproc = NULL; // ID of process to send
    comm->recvproc = NULL; // ID of process to recv

    comm->sendptr = NULL; // Position in send where data for proc is
    comm->recvptr = NULL; // Position in recv where data for proc must be

    comm->send = NULL;   // Data to send
                        // Data to receive is in vector

	#ifdef ARCHSAI_USING_GPU
		comm->gpusend = NULL;
	#endif

    /* RECV part of the comm scheme */
    ARCHSAI_INT *mpi_recv = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *glcol = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.gnrows, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < ptr.nrows; i++){
        for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
            ARCHSAI_INT ind = ptr.loc2gl[ptr.colind[j]];
            ARCHSAI_INT colowner = ptr.rowdist[ind];
            if (colowner == rank) continue;
            if (mpi_recv[colowner] == 0) comm->nrecv++;
            if (glcol[ind] == 0) {
                mpi_recv[colowner]++;
                comm->recvsize++;
            }
            glcol[ind] = 1;
        }
    }

    /* I know how many procs I receive data from and how many */
    /* Allocate Stuff */

    ARCHSAI_INT pos = 0;
    ARCHSAI_INT cnt = ptr.nrows;

    ARCHSAI_INT *mpi_recv_offsets = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    comm->recvproc = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nrecv, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < nprocs; i++) {
        if (mpi_recv[i] > 0){
            comm->recvproc[pos] = i;
            pos++;
        }
        if (i != rank){
            mpi_recv_offsets[i] = cnt;
            cnt += mpi_recv[i];
        }
    }

    comm->recvptr = ArchSAI_CTAlloc(ARCHSAI_INT, 2 + comm->nrecv, ArchSAI_MEMORY_HOST);
    cnt = ptr.nrows;
    comm->recvptr[1] = cnt;
    pos = 2;

    for (int i = 0; i < comm->nrecv; i++){
        comm->recvptr[pos] = comm->recvptr[pos - 1] + mpi_recv[comm->recvproc[i]];
        pos++;
    }

    /* SEND part of the comm scheme */

    ARCHSAI_INT *mpi_send = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
    if (nprocs == 1) mpi_send[0] = mpi_recv[0];
	else MPI_Alltoall(mpi_recv, 1, ARCHSAI_MPI_INT, mpi_send, 1, ARCHSAI_MPI_INT, MPI_COMM_WORLD);

    for (int i = 0; i < nprocs; i++){
        if (mpi_send[i] > 0) {
            comm->nsend++;
            comm->sendsize += mpi_send[i];
        }
    }

    /* Allocate stuff */
    comm->sendproc = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nsend, ArchSAI_MEMORY_HOST);
    pos = 0;
    for (int i = 0; i < nprocs; i++){
        if (mpi_send[i] > 0){
            comm->sendproc[pos] = i;
            pos++;
        }
    }

    ARCHSAI_INT *mpi_recv_index = ArchSAI_CTAlloc(ARCHSAI_INT, ptr.nrows + comm->recvsize, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT *mpi_send_index = ArchSAI_CTAlloc(ARCHSAI_INT, comm->sendsize, ArchSAI_MEMORY_HOST);

    for (int i = 0; i < ptr.gnrows; i++){
        if (ptr.gl2loc[i] != -1){
            mpi_recv_index[ptr.gl2loc[i]] = i;
        }
    }

    /* Communicate global indices to be sent */

    MPI_Request *req = ArchSAI_CTAlloc(MPI_Request, 2*nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT reqs = 0; pos = 0;

    for (int i = 0; i < nprocs; i++){
        if (mpi_recv[i] > 0){
            MPI_Isend(mpi_recv_index + mpi_recv_offsets[i], mpi_recv[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[reqs]);
            reqs++;
        }
        if (mpi_send[i] > 0){
            MPI_Irecv(mpi_send_index + pos, mpi_send[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[reqs]);
            pos += mpi_send[i];
            reqs++;
        }
    }

    MPI_Waitall(reqs, req, MPI_STATUSES_IGNORE);


    comm->sendptr = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nsend + 1, ArchSAI_MEMORY_HOST);
    comm->sendpos = ArchSAI_CTAlloc(ARCHSAI_INT, comm->sendsize, ArchSAI_MEMORY_HOST);
    comm->send = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, comm->sendsize, ArchSAI_MEMORY_HOST);
	#ifdef ARCHSAI_USING_GPU
		comm->gpusend = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, comm->sendsize, ArchSAI_MEMORY_DEVICE);
	#endif


    for (int i = 0; i < comm->sendsize; i++){
        comm->sendpos[i] = ptr.gl2loc[mpi_send_index[i]];
    }

    pos = 1;

    for (int i = 0; i < comm->nsend; i++){
        comm->sendptr[pos] = comm->sendptr[pos - 1] + mpi_send[comm->sendproc[i]];
        pos++;
    }

    ArchSAI_TFree(mpi_recv, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(mpi_send, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(mpi_recv_index, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(mpi_send_index, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(mpi_recv_offsets, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(glcol, ArchSAI_MEMORY_HOST);
    return comm;
}


ARCHSAI_COMM *ArchSAI_CopyCommSchemeMatCSR(ARCHSAI_MATCSR ptr){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr.comm, &rank);
    MPI_Comm_size(ptr.comm, &nprocs);

    ARCHSAI_COMM *comm = ArchSAI_CTAlloc(ARCHSAI_COMM, 1, ArchSAI_MEMORY_HOST);

    comm->nrecv = ptr.commscheme->nrecv; // Number of sends
    comm->nsend = ptr.commscheme->nsend; // Number of recvs
    comm->sendsize = ptr.commscheme->sendsize; // Size of send buffer
    comm->recvsize = ptr.commscheme->recvsize; // Size of recv buffer

    comm->sendproc = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nsend, ArchSAI_MEMORY_HOST);
    comm->recvproc = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nrecv, ArchSAI_MEMORY_HOST);
    comm->sendptr = ArchSAI_CTAlloc(ARCHSAI_INT, comm->nsend + 1, ArchSAI_MEMORY_HOST);
    comm->recvptr = ArchSAI_CTAlloc(ARCHSAI_INT, 2 + comm->nrecv, ArchSAI_MEMORY_HOST);
    comm->send = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, comm->sendsize, ArchSAI_MEMORY_HOST);

	#ifdef ARCHSAI_USING_GPU
		comm->gpusend = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, comm->sendsize, ArchSAI_MEMORY_DEVICE);
	#endif

    comm->sendpos = ArchSAI_CTAlloc(ARCHSAI_INT, comm->sendsize, ArchSAI_MEMORY_HOST);

    if (nprocs > 1){
        ArchSAI_TMemcpy(comm->sendproc, ptr.commscheme->sendproc,      ARCHSAI_INT,   comm->nsend,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TMemcpy(comm->recvproc, ptr.commscheme->recvproc,      ARCHSAI_INT,   comm->nrecv,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TMemcpy(comm->sendptr, ptr.commscheme->sendptr,      ARCHSAI_INT,   comm->nsend + 1,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TMemcpy(comm->recvptr, ptr.commscheme->recvptr,      ARCHSAI_INT,   2 + comm->nrecv,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TMemcpy(comm->send, ptr.commscheme->send,      ARCHSAI_DOUBLE,   comm->sendsize,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
		#ifdef ARCHSAI_USING_GPU
			ArchSAI_TMemcpy(comm->gpusend, ptr.commscheme->gpusend,      ARCHSAI_DOUBLE,   comm->sendsize,    ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_DEVICE);
		#endif

        ArchSAI_TMemcpy(comm->sendpos, ptr.commscheme->sendpos,      ARCHSAI_INT,   comm->sendsize,    ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    }

    return comm;
}




ARCHSAI_MATCSR ArchSAI_OuterMatrix(ARCHSAI_MATCSR ptr, ARCHSAI_MATCSR matrix, ARCHSAI_INT **gl_idx){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr.comm, &rank);
    MPI_Comm_size(ptr.comm, &nprocs);

    /* How many rows should every process send and receive */

	ARCHSAI_INT		*proc_rows_recv = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST),
                    *proc_rows_send = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
	ARCHSAI_CHAR    *row_use = ArchSAI_CTAlloc(ARCHSAI_CHAR, ptr.gnrows, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < ptr.nrows; i++){
		for (int j = ptr.rowind[i]; j < ptr.rowind[i + 1]; j++){
			row_use[ptr.loc2gl[ptr.colind[j]]] = 1;
		}
	}

	for (int i = 0; i < ptr.gnrows; i++){
		if ((row_use[i] == 1) && (ptr.rowdist[i] != rank)){
			proc_rows_recv[ptr.rowdist[i]]++;
		}
	}

    if (nprocs == 1) proc_rows_send[0] = proc_rows_recv[0];
	else MPI_Alltoall(proc_rows_recv, 1, ARCHSAI_MPI_INT, proc_rows_send, 1, ARCHSAI_MPI_INT, ptr.comm);

	/* Obtain indices of rows that I need */

	ARCHSAI_INT		*torecv_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *tosend_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *counter_procs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		torecv_ptrs[i + 1] = torecv_ptrs[i] + proc_rows_recv[i];
	}

	(*gl_idx) = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < ptr.gnrows; i++){
		if ((row_use[i] == 1) && (ptr.rowdist[i] != rank)){
			(*gl_idx)[torecv_ptrs[ptr.rowdist[i]] + counter_procs[ptr.rowdist[i]]] = i;
			counter_procs[ptr.rowdist[i]]++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		tosend_ptrs[i + 1] = tosend_ptrs[i] + proc_rows_send[i];
	}

    /* Communications of row indices to know what indices have to be sent */
	ARCHSAI_INT		*tosend_indices = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

    MPI_Request     *req = ArchSAI_TAlloc(MPI_Request, 2 * nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT     counter = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Irecv(tosend_indices + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[counter]);
			counter++;
		}
	}
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Isend((*gl_idx) + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[counter]);
			counter++;
		}
	}
	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    /* Send sizes of rows to create buffers */

    ARCHSAI_INT *send_sizes = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		for (int j = tosend_ptrs[i]; j < tosend_ptrs[i + 1]; j++){
			send_sizes[j] += (matrix.rowind[matrix.gl2loc[tosend_indices[j]] + 1] - matrix.rowind[matrix.gl2loc[tosend_indices[j]]]);
		}
	}

	/* We create a local temporary matrix to store the outer rows */

    ARCHSAI_MATCSR mat;
    ArchSAI_InitMatCSR(&mat);

    mat.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);
    mat.size   = torecv_ptrs[nprocs];

	counter = 0;
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Irecv(mat.rowind + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[counter]);
			counter++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Isend(send_sizes + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, ptr.comm, &req[counter]);
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    ARCHSAI_INT nextval = 0,
                prevval = 0;

	for (int i = 0; i < mat.size + 1; i++){
		nextval = prevval + mat.rowind[i];
		mat.rowind[i] = prevval;
		prevval = nextval;
	}

	mat.nnz = mat.rowind[mat.size];

    /* Now I know recv index sizes so we build a buffer */

    mat.colind = ArchSAI_CTAlloc(ARCHSAI_INT, mat.nnz, ArchSAI_MEMORY_HOST);
    if (matrix.val)
        mat.val = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat.nnz, ArchSAI_MEMORY_HOST);

        /* We will transfer columns and values at the same communication step with a buffer */
    ARCHSAI_DOUBLE *recvVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, mat.nnz * 2, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT sizesendbuffer = 0;
    for (int i = 0; i < tosend_ptrs[nprocs]; i++) sizesendbuffer += send_sizes[i];

    ARCHSAI_DOUBLE *sendVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, sizesendbuffer * 2, ArchSAI_MEMORY_HOST);
	ARCHSAI_INT *sendVCbuffer_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT position = 0;

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		for (int j = matrix.rowind[matrix.gl2loc[tosend_indices[i]]]; j < matrix.rowind[matrix.gl2loc[tosend_indices[i]] + 1]; j++){
			sendVCbuffer[position] = (double) matrix.loc2gl[matrix.colind[j]];
            if (matrix.val)
                sendVCbuffer[position + 1] = matrix.val[j];
			position += 2;
			sendVCbuffer_ptr[i + 1]++;
		}
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = sendVCbuffer_ptr[i + 1] + sendVCbuffer_ptr[i];
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = 2 * sendVCbuffer_ptr[i + 1];
	}


	/* Communications for actual values */

	for (int i = 0; i < mat.size; i++){
		mat.rowind[i + 1] = 2 * mat.rowind[i + 1];
	}

	counter = 0;
	int		start_pos = 0,
			end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			end_pos = start_pos + proc_rows_recv[i];
			MPI_Irecv(recvVCbuffer + mat.rowind[start_pos], (mat.rowind[end_pos] - mat.rowind[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, ptr.comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	start_pos = 0;
	end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			end_pos = start_pos + proc_rows_send[i];
			MPI_Isend(sendVCbuffer + sendVCbuffer_ptr[start_pos], (sendVCbuffer_ptr[end_pos] - sendVCbuffer_ptr[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, ptr.comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

	for (int i = 0; i < mat.size; i++){
		mat.rowind[i + 1] = mat.rowind[i + 1] / 2;
	}


	/* Create matrix with external rows */

	for (int i = 0; i < mat.rowind[mat.size]; i++){
		mat.colind[i] = (ARCHSAI_INT) recvVCbuffer[2 * i];
        if (matrix.val)
            mat.val[i] = recvVCbuffer[2 * i + 1];
	}

	ArchSAI_TFree(proc_rows_send, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(proc_rows_recv, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(row_use, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(counter_procs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_ptrs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_indices, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(send_sizes, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(recvVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer_ptr, ArchSAI_MEMORY_HOST);

    return mat;

}





















