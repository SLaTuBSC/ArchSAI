
#include <ArchSAI_GPULinAlg.h>
#include <ArchSAI_Time.h>
#include <ArchSAI_Print.h>

void archsai_GPU_sDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2, ARCHSAI_FLOAT *res){}
void archsai_GPU_dDotP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, ARCHSAI_DOUBLE *res){}

void archsai_GPU_sAxpy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2){}
void archsai_GPU_dAxpy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2){}

void archsai_GPU_sCopy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2){}
void archsai_GPU_dCopy(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2){}

void archsai_GPU_sScal(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_FLOAT *alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1){}
void archsai_GPU_dScal(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1){}

void archsai_GPU_sSpMV(){}
void archsai_GPU_dSpMV(){}

void ArchSAI_GPU_CSRMV(ARCHSAI_DOUBLE alpha, ARCHSAI_MATCSR mat, ARCHSAI_VEC x, ARCHSAI_DOUBLE beta, ARCHSAI_VEC y, void *buffer, ARCHSAI_INT nprocs){

	#if defined ARCHSAI_USING_GPU

    ARCHSAI_INT comms = mat.commscheme->nrecv + mat.commscheme->nsend;
    MPI_Request req[comms];

	ARCHSAI_DOUBLE  t1 = 0.0;
    ARCHSAI_DOUBLE  t2 = 0.0;


    if (nprocs > 1){

// 		t1 = time_getWallclockSeconds();

		ArchSAI_Memcpy(mat.commscheme->recv, x.val, x.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST_PINNED, ArchSAI_MEMORY_DEVICE);

// 		t2 = time_getWallclockSeconds();
// 		ArchSAI_ParPrintf(mat.comm, "%lf ", t2 - t1);
// 		t1 = time_getWallclockSeconds();

		ArchSAI_Memcpy((mat.d_spmvc).val, x.val, x.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_DEVICE);

// 		t2 = time_getWallclockSeconds();
// 		ArchSAI_ParPrintf(mat.comm, "%lf ", t2 - t1);
// 		t1 = time_getWallclockSeconds();

		for (int i = 0; i < mat.commscheme->nrecv; i++){
            ARCHSAI_INT size = mat.commscheme->recvptr[i + 2] - mat.commscheme->recvptr[i + 1];
            MPI_Irecv(mat.commscheme->recv + mat.commscheme->recvptr[i + 1], size, ARCHSAI_MPI_DOUBLE, mat.commscheme->recvproc[i], 0, mat.comm, &req[i]);
        }

        for (int i = 0; i < mat.commscheme->sendsize; i++) {
			mat.commscheme->send[i] = mat.commscheme->recv[mat.commscheme->sendpos[i]];
        }

        for (int i = 0; i < mat.commscheme->nsend; i++){
            ARCHSAI_INT size = mat.commscheme->sendptr[i + 1] - mat.commscheme->sendptr[i];
			MPI_Isend(mat.commscheme->send + mat.commscheme->sendptr[i], size, ARCHSAI_MPI_DOUBLE, mat.commscheme->sendproc[i], 0, mat.comm, &req[mat.commscheme->nrecv + i]);
        }

		MPI_Waitall(comms, req, MPI_STATUSES_IGNORE);

		ArchSAI_Memcpy((mat.d_spmvc).val + mat.nrows, mat.commscheme->recv + mat.nrows, (mat.commscheme->recvsize) * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST_PINNED);
		ArchSAI_GPU_Csrmv(mat, alpha, mat.d_spmvc, beta, mat.d_spmvr, buffer);
		ArchSAI_Memcpy(y.val, (mat.d_spmvr).val, (mat.d_spmvr).size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_DEVICE);

// 		t2 = time_getWallclockSeconds();
// 		ArchSAI_ParPrintf(mat.comm, "%lf\n", t2 - t1);
    }
    else {

// 		t1 = time_getWallclockSeconds();
		ArchSAI_GPU_Csrmv(mat, alpha, x, beta, y, buffer);
// 		t2 = time_getWallclockSeconds();
// 		ArchSAI_ParPrintf(mat.comm, "%lf\n", t2 - t1);
	}


	#endif

}

ARCHSAI_DOUBLE ArchSAI_GPU_DOTP(ARCHSAI_GPU_BLASHANDLE blashandle, ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, MPI_Comm comm, ARCHSAI_INT nprocs){

    ARCHSAI_DOUBLE global = 0.0;
	ARCHSAI_DOUBLE local = 0.0;

	ArchSAI_GPU_DotP(blashandle, size, v1, inc1, v2, inc2, &local);

    if (nprocs == 1) return local;

    MPI_Allreduce(&local, &global, 1, ARCHSAI_MPI_DOUBLE, MPI_SUM, comm);

    return global;

}
