# Documentation for ArchSAI library {#mainpage}

This is the documentation for the ArchSAI (Architecture-Aware Sparse Approximate Inverses) library.

Here you will find information for all files and implemented functions.

<!-- 1. It exists. -->
<!-- 2. I wrote it. -->
<!-- 3. Everything is documented (pretty easy since there's only one function) -->
 
