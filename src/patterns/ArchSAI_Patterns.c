#include <ArchSAI_Patterns.h>
#include <ArchSAI_Print.h>

#include <omp.h>

ARCHSAI_INT ArchSAI_GetPattern(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params){

    ARCHSAI_MATCSR tmp_ptr;

    // Check params to see which pattern to get
    switch(params.pat_sel){
        case 0:
            *pattern = ArchSAI_GetBasePattern(A, params.precond);
            break;
        case 1:
            tmp_ptr = ArchSAI_GetBasePattern(A, 1);
            ArchSAI_PowerPattern(&tmp_ptr, params.pat_aux);
            *pattern = ArchSAI_GetBasePattern(tmp_ptr, params.precond);
            ArchSAI_DestroyMatCSR(&tmp_ptr, ArchSAI_MEMORY_HOST);
            break;
        default:
            break;
    }


    return archsai_error_flag;
}


ARCHSAI_MATCSR ArchSAI_GetBasePattern(ARCHSAI_MATCSR A, ARCHSAI_INT pc){

    ARCHSAI_MATCSR pattern;

    switch(pc){
        case 0:
            break;
        case 2:
            pattern = ArchSAI_InitCopyBasePatternFromMat(A);
            break;
        case 3:
            pattern = ArchSAI_InitLTPPatternFromMat(A);
            break;
        default:
            break;
    }

    return pattern;
}



ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params){

    ARCHSAI_MATCSR reference_pattern;

    // Extend SAI
    if (params.precond == 2){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendPattern(pattern, params.ext_spa, params.precond, 0);
        ArchSAI_ComputeSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);
        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);

        // EXTEND TEMPORALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_TransposeMatCSR(pattern);
        if (params.epos == 0) ArchSAI_ExtendPattern(pattern, params.ext_tmp, params.precond, 1);
        else ArchSAI_ExtendPattern_NoTmpCache(pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }
    // Extend FSAI
    else if (params.precond == 3){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendPattern(pattern, params.ext_spa, params.precond, 0);
        ArchSAI_ComputeFSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);
        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);

        // EXTEND TEMPORALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_TransposeMatCSR(pattern);
        if (params.epos == 0) ArchSAI_ExtendPattern(pattern, params.ext_tmp, params.precond, 1);
        else ArchSAI_ExtendPattern_NoTmpCache(pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeFSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }

    return archsai_error_flag;
}


ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params){

    ARCHSAI_MATCSR reference_pattern;

    // Extend SAI
    if (params.precond == 2){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendPattern(pattern, params.ext_spa, params.precond, 0);

        // EXTEND TEMPORALLY
        ArchSAI_TransposeMatCSR(pattern);
        if (params.epos == 0) ArchSAI_ExtendPattern(pattern, params.ext_tmp, params.precond, 1);
        else ArchSAI_ExtendPattern_NoTmpCache(pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }
    // Extend FSAI
    else if (params.precond == 3){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendPattern(pattern, params.ext_spa, params.precond, 0);

        // EXTEND TEMPORALLY
        ArchSAI_TransposeMatCSR(pattern);
        if (params.epos == 0) ArchSAI_ExtendPattern(pattern, params.ext_tmp, params.precond, 1);
        else ArchSAI_ExtendPattern_NoTmpCache(pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeFSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }

    return archsai_error_flag;
}


ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params){

    ARCHSAI_MATCSR reference_pattern;
    ARCHSAI_MATCSR extendedref;

    // Extend SAI
    if (params.precond == 2){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_spa, params.precond, 0);
        ArchSAI_ComputeSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // EXTEND TEMPORALLY
        extendedref = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeSAIApprox(A, pattern);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_FilterMatCSR(pattern, extendedref, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
        ArchSAI_DestroyMatCSR(&extendedref, ArchSAI_MEMORY_HOST);

    }
    // Extend FSAI
    else if (params.precond == 3){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_spa, params.precond, 0);
        ArchSAI_ComputeFSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // EXTEND TEMPORALLY
        extendedref = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeFSAIApprox(A, pattern);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_FilterMatCSR(pattern, extendedref, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
        ArchSAI_DestroyMatCSR(&extendedref, ArchSAI_MEMORY_HOST);
    }

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params){

    ARCHSAI_MATCSR reference_pattern;

    // Extend SAI
    if (params.precond == 2){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_spa, params.precond, 0);

        // EXTEND TEMPORALLY
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_TransposeMatCSR(pattern);

        ArchSAI_ComputeSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }
    // Extend FSAI
    else if (params.precond == 3){

        // EXTEND SPATIALLY
        reference_pattern = ArchSAI_InitCopyBasePatternFromMat(*pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_spa, params.precond, 0);

        // EXTEND TEMPORALLY
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_ExtendReferencePattern(pattern, reference_pattern, params.ext_tmp, params.precond, 1);
        ArchSAI_TransposeMatCSR(&reference_pattern);
        ArchSAI_TransposeMatCSR(pattern);
        ArchSAI_ComputeFSAIApprox(A, pattern);
        ArchSAI_FilterMatCSR(pattern, reference_pattern, params);

        // Randomize if asked
        if (params.rand == 1) ArchSAI_RandomizePattern(pattern, reference_pattern, params);

        ArchSAI_DestroyMatCSR(&reference_pattern, ArchSAI_MEMORY_HOST);
    }

    return archsai_error_flag;
}



ARCHSAI_INT ArchSAI_ExtendPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    // VSAI -> 2 SAI / 3 FSAI. To consider for extension limits
    // lower_upper -> 0 lower pattern or full / 1 upper pattern

    if (ext_size == 0){
        ArchSAI_ParPrintf(pattern->comm, "Error: pattern extension size can not be 0.\n");
        ArchSAI_Error(1);
    }

    if (VSAI < 2 || VSAI > 3){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    if (lower_upper < 0 || lower_upper > 1){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]) * ext_size, ArchSAI_MEMORY_HOST),
                    cachepos = 0,
                    counter = 0,
                    rowcount = 0,
                    initadd = 0,
                    endadd = 0;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            ARCHSAI_INT last_colidx = -1;
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){

                ARCHSAI_INT colidx = pattern->colind[j];
                ARCHSAI_INT colidx_owner = pattern->rowdist[pattern->loc2gl[colidx]];

                /* If column is added move to next one */
                if (colidx <= last_colidx) continue;

                /* Get relative position in cache */
                cachepos = colidx % ext_size;

                /* Limits of extension */

                // Comm-Aware limits SAI/FSAI

                initadd = ((colidx - cachepos) < 0) ?  0 : colidx - cachepos;
                endadd = ((colidx + ext_size - cachepos) > pattern->ncols) ? pattern->ncols: colidx + ext_size - cachepos;

                for (int k = initadd; k < endadd; k++){
                    if (pattern->rowdist[pattern->loc2gl[k]] == colidx_owner){
                        if (VSAI == 2 ||
                            ((VSAI == 3) && (pattern->loc2gl[k] <= pattern->loc2gl[i]) && (lower_upper == 0)) ||
                            ((VSAI == 3) && (pattern->loc2gl[k] >= pattern->loc2gl[i]) && (lower_upper == 1))
                            )
                        {
                            colsthread[counter] = k;
                            counter++;
                            rowcount++;
                        }
                    }
                    else {
                        continue;
                    }
                }

                last_colidx = colsthread[counter - 1];
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}




ARCHSAI_INT ArchSAI_ExtendReferencePattern(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR ref, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    // VSAI -> 2 SAI / 3 FSAI. To consider for extension limits
    // lower_upper -> 0 lower pattern or full / 1 upper pattern

    if (ext_size == 0){
        ArchSAI_ParPrintf(pattern->comm, "Error: pattern extension size can not be 0.\n");
        ArchSAI_Error(1);
    }

    if (VSAI < 2 || VSAI > 3){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    if (lower_upper < 0 || lower_upper > 1){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]) * ext_size, ArchSAI_MEMORY_HOST),
                    cachepos = 0,
                    counter = 0,
                    rowcount = 0,
                    initadd = 0,
                    endadd = 0;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            ARCHSAI_INT last_colidx = -1;
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){

                ARCHSAI_INT colidx = pattern->colind[j];
                ARCHSAI_INT colidx_owner = pattern->rowdist[pattern->loc2gl[colidx]];

                /* If column is added move to next one */
                if (colidx <= last_colidx) continue;

                /* Get relative position in cache */
                cachepos = colidx % ext_size;

                /* Limits of extension */

                // Comm-Aware limits SAI/FSAI

                ARCHSAI_INT ptr_gl_colidx = pattern->loc2gl[colidx];
                ARCHSAI_INT ref_loc_colidx = ref.gl2loc[ptr_gl_colidx];


                /* If column is not in reference pattern do not extend */
                if (ArchSAI_IsColMatCSR(i, ref_loc_colidx, &ref) == 1){
                    initadd = ((colidx - cachepos) < 0) ?  0 : colidx - cachepos;
                    endadd = ((colidx + ext_size - cachepos) > pattern->ncols) ? pattern->ncols: colidx + ext_size - cachepos;
                    ARCHSAI_INT back = 0;
                    while ((colsthread[counter - 1] >= initadd) && ((j - back) > pattern->rowind[i]) && (pattern->rowdist[pattern->loc2gl[colsthread[counter - 1]]] == colidx_owner)){
                            back++;
                            counter--;
                            rowcount--;
                    }
                }
                else {
                    colsthread[counter] = colidx;
                    counter++;
                    rowcount++;
                    last_colidx = colsthread[counter - 1];
                    continue;
                }

                for (int k = initadd; k < endadd; k++){
                    if (pattern->rowdist[pattern->loc2gl[k]] == colidx_owner){
                        if (VSAI == 2 ||
                            ((VSAI == 3) && (pattern->loc2gl[k] <= pattern->loc2gl[i]) && (lower_upper == 0)) ||
                            ((VSAI == 3) && (pattern->loc2gl[k] >= pattern->loc2gl[i]) && (lower_upper == 1))
                            )
                        {
                            colsthread[counter] = k;
                            counter++;
                            rowcount++;
                        }
                    }
                    else {
                        continue;
                    }
                }

                last_colidx = colsthread[counter - 1];
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_ExtendPattern_NoTmpCache(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    // VSAI -> 2 SAI / 3 FSAI. To consider for extension limits
    // lower_upper -> 0 lower pattern or full / 1 upper pattern

    if (ext_size == 0){
        ArchSAI_ParPrintf(pattern->comm, "Error: pattern extension size can not be 0.\n");
        ArchSAI_Error(1);
    }

    if (VSAI < 2 || VSAI > 3){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    if (lower_upper < 0 || lower_upper > 1){
        ArchSAI_ParPrintf(pattern->comm, "Error: ArchSAI_ExtendWithRef arguments are wrong.\n");
        ArchSAI_Error(2);
    }

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]) * ext_size, ArchSAI_MEMORY_HOST),
                    cachepos = 0,
                    counter = 0,
                    rowcount = 0,
                    initadd = 0,
                    endadd = 0;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            ARCHSAI_INT last_colidx = -1;
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){

                ARCHSAI_INT colidx = pattern->colind[j];
                ARCHSAI_INT colidx_owner = pattern->rowdist[pattern->loc2gl[colidx]];

                /* If column is added move to next one */
                if (colidx <= last_colidx) continue;

                /* Get relative position in cache */
                cachepos = colidx % ext_size;

                /* Limits of extension */

                // Comm-Aware limits SAI/FSAI

                if (lower_upper == 0){
                    initadd = ((colidx - cachepos) < 0) ?  0 : colidx - cachepos;
                    endadd = ((colidx + ext_size - cachepos) > pattern->ncols) ? pattern->ncols: colidx + ext_size - cachepos;
                }
                else
                {
                    initadd = colidx;
                    endadd = ((colidx + ext_size) > pattern->ncols) ? pattern->ncols: colidx + ext_size;
                }

                for (int k = initadd; k < endadd; k++){
                    if (pattern->rowdist[pattern->loc2gl[k]] == colidx_owner){
                        if (VSAI == 2 ||
                            ((VSAI == 3) && (pattern->loc2gl[k] <= pattern->loc2gl[i]) && (lower_upper == 0)) ||
                            ((VSAI == 3) && (pattern->loc2gl[k] >= pattern->loc2gl[i]) && (lower_upper == 1))
                            )
                        {
                            colsthread[counter] = k;
                            counter++;
                            rowcount++;
                        }
                    }
                    else {
                        continue;
                    }
                }

                last_colidx = colsthread[counter - 1];
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_PowerPattern(ARCHSAI_MATCSR *ptr, ARCHSAI_INT level){

    if (level < 1){
        ArchSAI_ParPrintf(ptr->comm, "\nError: Level of pattern can not be lower than 1.");
        ArchSAI_Error(1);
    }

    if (level == 1) return archsai_error_flag;

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(ptr->comm, &rank);
    MPI_Comm_size(ptr->comm, &nprocs);

    // If level > 1 -> Power

    ARCHSAI_MATCSR base_ptr;
    base_ptr = ArchSAI_InitCopyBasePatternFromMat(*ptr);
    ArchSAI_ToGlobalMatCSR(&base_ptr);
    ARCHSAI_INT lvl = 2;

    while (lvl < level + 1){
        // First: obtain outer pattern
        ARCHSAI_INT *gl_rows;
        ARCHSAI_MATCSR outer = ArchSAI_OuterMatrix(*ptr, *ptr, &gl_rows);
        // Convert pattern to Global
        ArchSAI_ToGlobalMatCSR(ptr);
        // Merge ptr and outer matrix

        ARCHSAI_INT   *limits,
            nthreads    = omp_get_max_threads(),
            *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   ptr->nrows + 1, ArchSAI_MEMORY_HOST),
            *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

        ARCHSAI_MATCSRBalancer(&base_ptr, &limits, nthreads, 1);

        #pragma omp parallel num_threads(nthreads)
        {
            ARCHSAI_INT   thid = omp_get_thread_num();
            ARCHSAI_CHAR  *row = ArchSAI_CTAlloc(ARCHSAI_CHAR, ptr->gncols, ArchSAI_MEMORY_HOST);
            ARCHSAI_INT counter = 0;
            ARCHSAI_INT rowcounter = 0;

            for (int i = limits[thid]; i < limits[thid + 1]; i++){
                for (int j = base_ptr.rowind[i]; j < base_ptr.rowind[i + 1]; j++){
                    ARCHSAI_INT globalrow = base_ptr.colind[j];
                    if (base_ptr.rowdist[globalrow] == rank){
                        ARCHSAI_INT localrow = ptr->gl2loc[globalrow];
                        for (int k = ptr->rowind[localrow]; k < ptr->rowind[localrow + 1]; k++){
                            ARCHSAI_INT col = ptr->colind[k];
                            if (row[col] == 0) {
                                counter++;
                                row[col] = 1;
                            }
                        }
                    }
                    else {
                        ARCHSAI_INT outer_row = 0;
                        for (outer_row = 0; outer_row < outer.size; outer_row++){
                            if (gl_rows[outer_row] == globalrow) break;
                        }
                        if (outer_row == outer.size) ArchSAI_Error(1);
                        for (int k = outer.rowind[outer_row]; k < outer.rowind[outer_row + 1]; k++){
                            ARCHSAI_INT col = outer.colind[k];
                            if (row[col] == 0) {
                                counter++;
                                row[col] = 1;
                            }
                        }
                    }
                }
                ArchSAI_Memset(row, 0, ptr->gncols * sizeof(ARCHSAI_CHAR), ArchSAI_MEMORY_HOST);
            }

            ARCHSAI_INT   *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST);
            rowcounter = 0;
            counter = 0;

            for (int i = limits[thid]; i < limits[thid + 1]; i++){
                for (int j = base_ptr.rowind[i]; j < base_ptr.rowind[i + 1]; j++){
                    ARCHSAI_INT globalrow = base_ptr.colind[j];
                    if (base_ptr.rowdist[globalrow] == rank){
                        ARCHSAI_INT localrow = ptr->gl2loc[globalrow];
                        for (int k = ptr->rowind[localrow]; k < ptr->rowind[localrow + 1]; k++){
                            ARCHSAI_INT col = ptr->colind[k];
                            if (row[col] == 0) {
                                colsthread[counter] = col;
                                counter++;
                                rowcounter++;
                                row[col] = 1;
                            }
                        }
                    }
                    else {
                        ARCHSAI_INT outer_row = 0;
                        for (outer_row = 0; outer_row < outer.size; outer_row++){
                            if (gl_rows[outer_row] == globalrow) break;
                        }
                        for (int k = outer.rowind[outer_row]; k < outer.rowind[outer_row + 1]; k++){
                            ARCHSAI_INT col = outer.colind[k];
                            if (row[col] == 0) {
                                colsthread[counter] = col;
                                counter++;
                                rowcounter++;
                                row[col] = 1;
                            }
                        }
                    }
                }


                // Row is powered. Reorder colsthread and update tmprows
                qsort(colsthread + counter - rowcounter, rowcounter, sizeof(ARCHSAI_INT), intcompare);
                tmprows[i + 1] = rowcounter;
                rowcounter = 0;
                ArchSAI_Memset(row, 0, ptr->gncols * sizeof(ARCHSAI_CHAR), ArchSAI_MEMORY_HOST);
            }

            copypos[thid + 1] = counter;

            #pragma omp barrier
            #pragma omp single
            {
                for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
                ptr->nnz = copypos[nthreads];
                ArchSAI_TFree(ptr->colind, ArchSAI_MEMORY_HOST);
                ptr->colind = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->nnz, ArchSAI_MEMORY_HOST);
            }

            ArchSAI_TMemcpy(ptr->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

            ArchSAI_TFree(row, ArchSAI_MEMORY_HOST);
            ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
        }

        MPI_Allreduce(&ptr->nnz, &ptr->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, ptr->comm);

        ArchSAI_TMemcpy(ptr->rowind, tmprows, ARCHSAI_INT, (ptr->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        for (int i = 0; i < ptr->nrows; i++) ptr->rowind[i + 1] = ptr->rowind[i + 1] + ptr->rowind[i];

        ArchSAI_DestroyCommScheme(ptr->commscheme, ArchSAI_MEMORY_HOST);
        ArchSAI_DestroyDistMatCSRConverters(ptr);
        ArchSAI_GetDistMatCSRConverters(ptr);
        ArchSAI_ToLocalMatCSR(ptr);
        ptr->commscheme = ArchSAI_CreateCommSchemeMatCSR(*ptr);

        lvl++;

        ArchSAI_DestroyMatCSR(&outer, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(gl_rows, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);
    }

    ArchSAI_DestroyMatCSR(&base_ptr, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}




ARCHSAI_INT ArchSAI_RandomizePattern(ARCHSAI_MATCSR *ptr, ARCHSAI_MATCSR ref, ARCHSAI_PARAMS params){

    srand(time(NULL));

    ARCHSAI_INT    *limits,
                    nthreads    = omp_get_max_threads();

    ARCHSAI_MATCSRBalancer(ptr, &limits, nthreads, 1);

//     ArchSAI_Seed(0, ptr->gnrows);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT    thid = omp_get_thread_num(),
                    randcol = 0;

        ARCHSAI_INT    *row = ArchSAI_CTAlloc(ARCHSAI_INT, ptr->ncols, ArchSAI_MEMORY_HOST);

        for (int i = limits[thid]; i < limits[thid + 1]; i++){

            ArchSAI_Memset(row, 0, ptr->ncols * sizeof(ARCHSAI_INT), ArchSAI_MEMORY_HOST);

            for (int j = ref.rowind[i]; j < ref.rowind[i + 1]; j++){
                row[ref.colind[j]] = 1;
            }

            for (int j = ptr->rowind[i]; j < ptr->rowind[i + 1]; j++){
                if (ArchSAI_IsColMatCSR(i, ptr->colind[j], &ref) == 1) continue;
                else {
                    while (1){

                        if (params.precond == 2) randcol = rand() % ptr->ncols;
                        if (params.precond == 3) randcol = rand() % i;

                        if (row[randcol] == 0){
                            ptr->colind[j] = randcol;
                            row[randcol] = 1;
                            break;
                        }
                    }
                }
            }
            qsort(ptr->colind + ptr->rowind[i], ptr->rowind[i + 1] - ptr->rowind[i], sizeof(ARCHSAI_INT), intcompare);
       }
       free(row);
    }

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}
















