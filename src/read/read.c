
#include <ArchSAI_Read.h>

ARCHSAI_INT ArchSAI_ReadSystem(ARCHSAI_MATCSR *A, ARCHSAI_VEC *b, MPI_Comm context, ARCHSAI_HANDLER *handler){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(context, &rank);
    MPI_Comm_size(context, &nprocs);

    #ifdef ARCHSAI_TIMING
    ARCHSAI_INT timeindex = ArchSAI_InitializeTiming("READ");
    ArchSAI_BeginTiming(timeindex);
    #endif

    fflush(stdout);
    MPI_Barrier(context);

    #ifdef ARCHSAI_VERBOSE
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);
        ArchSAI_ParPrintf(context, "\n************** ArchSAI Read  *************\n");
    #endif

    // IMPLEMENT SELECTOR TO READ DIFFERENT FORMATS

    *A = ArchSAI_ReadMTXtoMatCSR(handler->params, context);
    *b = ArchSAI_ReadArray(handler->params, A->rowdist, context);

    if (A->nrows != b->size){
        ArchSAI_ParPrintf(context, "\nError: Dimensions of system matrix and RHS do not match.\n");
        ArchSAI_Error(1);
    }

    if (A->gnrows != b->gsize){
        ArchSAI_ParPrintf(context, "\nError: Dimensions of system matrix partition and RHS partition do not match.\n");
        ArchSAI_Error(1);
    }


    #ifdef ARCHSAI_VERBOSE

        fflush(stdout);
        MPI_Barrier(A->comm);
        ArchSAI_ParPrintf(A->comm, "\nSystem Matrix A (%i %i) Global data: %i %i %i\n", A->sym, A->fulldiag, A->gnrows, A->gnnz, A->gncols);
        ArchSAI_ParPrintf(A->comm, "System Matrix A (%i %i) Row Distribution\n\n", A->sym, A->fulldiag);
        fflush(stdout);
        MPI_Barrier(A->comm);

        for (int i = 0; i < nprocs; i++){
            if (rank == i){
                printf("\t%i\t| A:\t%i %i %i", rank, A->nrows, A->nnz, A->ncols);
                printf("\t| Comm Send: %i %i | Comm Recv: %i %i\n", ptrcomms(A)->nsend, ptrcomms(A)->sendsize, ptrcomms(A)->nrecv, ptrcomms(A)->recvsize);
                printf("\n");
                fflush(stdout);
            }
            MPI_Barrier(A->comm);
        }

    #endif // VERBOSE

    #ifdef ARCHSAI_TIMING
    ArchSAI_EndTiming(timeindex);
    handler->times[0] = ArchSAI_TimingWallTime(timeindex);
    #endif // TIMING

    fflush(stdout);
    MPI_Barrier(context);

    return archsai_error_flag;
}
