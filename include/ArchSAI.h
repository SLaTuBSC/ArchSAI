 /*
    This include file allows you to use ANY public ArchSAI function
*/

 #ifndef ArchSAI_h_
    #define ArchSAI_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_MPI.h>
    #include <ArchSAI_Utils.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Time.h>

#endif // __ArchSAI_h__
