
 /*! \file ArchSAI_Filter.h
    \brief This file contains the different filtering-out strategies used by ArchSAI.

    ArchSAI filtering-out strategies.
*/

 #ifndef ArchSAI_Filter_h_
    #define ArchSAI_Filter_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Patterns.h>
    #include <ArchSAI_Utils.h>

    /*! \fn ARCHSAI_INT ArchSAI_FilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_PARAMS params)
        \brief Filters a matrix with respect to a reference. Filtering-out strategy is selected from parameters

        \param pattern Matrix to be filtered. Matrix values must be in struct.fval.
        \param reference_pattern Pattern to be used as reference. Entries in reference_pattern will be kept in pattern.
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_FilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_INT ArchSAI_AbsValFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter)
        \brief Filters a matrix with respect to a reference. Filtering-out eliminates entries comparing their absolute value with respect to row maximum absolute value.

        \param pattern Matrix to be filtered. Matrix values must be in struct.fval.
        \param reference_pattern Pattern to be used as reference. Entries in reference_pattern will be kept in pattern.
        \param filter Filtering-out value.
    */
    ARCHSAI_INT ArchSAI_AbsValFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter);

    /*! \fn ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter)
        \brief Filters a matrix with respect to a reference. Filtering-out eliminates entries according to a scale-independent strategy. Only to be used in matrices with full diagonal.

        \param pattern Matrix to be filtered. Matrix values must be in struct.fval.
        \param reference_pattern Pattern to be used as reference. Entries in reference_pattern will be kept in pattern.
        \param filter Filtering-out value.
    */
    ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter);

    /*! \fn ARCHSAI_INT ArchSAI_AbsValFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter)
        \brief Filters a matrix. Filtering-out eliminates entries comparing their absolute value with respect to row maximum absolute value. Keeps diagonal entries.

        \param pattern Matrix to be filtered. Matrix values must be in struct.fval.
        \param filter Filtering-out value.
    */
    ARCHSAI_INT ArchSAI_AbsValFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter);

    /*! \fn ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter)
        \brief Filters a matrix. Filtering-out eliminates entries according to a scale-independent strategy. Only to be used in matrices with full diagonal. Keeps diagonal entries.

        \param pattern Matrix to be filtered. Matrix values must be in struct.fval.
        \param filter Filtering-out value.
    */
    ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter);

#endif // __ArchSAI_Filter_h__





