
 /*! \file ArchSAI_General.h
    \brief This file contains the general utilities and functions used by ArchSAI.

    ArchSAI general.
*/

 #ifndef ArchSAI_General_h_
    #define ArchSAI_General_h_

    #include <ArchSAI.h>
    #include <omp.h>

    /*! \fn ARCHSAI_INT ArchSAI_Init(ARCHSAI_INT *argc, ARCHSAI_CHAR** argv[])
        \brief Initializes ArchSAI. Must be always called before other calls.
    */
    ARCHSAI_INT ArchSAI_Init(ARCHSAI_INT *argc, ARCHSAI_CHAR** argv[]);

    /*! \fn ARCHSAI_INT ArchSAI_End(void)
        \brief ArchSAI ending. Always last call.
    */
    ARCHSAI_INT ArchSAI_End(void);

#endif // __ArchSAI_General_h__


