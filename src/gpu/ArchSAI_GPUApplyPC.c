#include <ArchSAI_GPUApplyPC.h>

const ARCHSAI_INT    gpuone = 1;

void ArchSAI_GPU_PrecondSpMV(ARCHSAI_PRECOND PC, ARCHSAI_VEC In, ARCHSAI_VEC Out, ARCHSAI_VEC tmp, void* buffer, ARCHSAI_INT nprocs){

    ARCHSAI_DOUBLE fp_one = 1.0;
    ARCHSAI_DOUBLE fp_zero = 0.0;

    if (PC.type == 0)
		ArchSAI_Memcpy(Out.val, In.val, In.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_DEVICE);
    if (PC.type == 1)
		ArchSAI_GPU_CSRMV(fp_one, M(PC), In, fp_zero, Out, buffer, nprocs);
    if (PC.type == 2){
		ArchSAI_GPU_CSRMV(fp_one, M(PC), In, fp_zero, Out, buffer, nprocs);
    }
    if (PC.type == 3){
		ArchSAI_GPU_CSRMV(fp_one, G(PC), In, fp_zero, tmp, buffer, nprocs);
		ArchSAI_GPU_CSRMV(fp_one, Gt(PC), tmp, fp_zero, Out, buffer, nprocs);
    }
}


