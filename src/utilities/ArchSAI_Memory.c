 
 /*! \file ArchSAI_Memory.c
    \brief Source file for the memory management in ArchSAI.

    ArchSAI memory management source file.
*/

#include "ArchSAI_Memory.h"


/*--------------------------------------------------------------------------
 * ArchSAI_OutOfMemory
 *--------------------------------------------------------------------------*/

static inline void ArchSAI_OutOfMemory(size_t size){
   ArchSAI_ErrorWithMsg(ARCHSAI_ERROR_MEMORY, "Out of memory trying to allocate too many bytes\n");
   ArchSAI_Assert(0);
   fflush(stdout);
}

static inline void ArchSAI_WrongMemoryLocation(void){
   ArchSAI_ErrorWithMsg(ARCHSAI_ERROR_MEMORY, "Unrecognized ArchSAI_MemoryLocation\n");
   ArchSAI_Assert(0);
   fflush(stdout);
}

void ArchSAI_CheckMemoryLocation(void *ptr, ArchSAI_MemoryLocation location){
   if (!ptr){return;}
   ArchSAI_MemoryLocation location_ptr;
   ArchSAI_GetPointerLocation(ptr, &location_ptr);
   /* do not use ArchSAI_assert, which has alloc and free;
    * will create an endless loop otherwise */
//    assert(location == location_ptr);
}

/*--------------------------------------------------------------------------
 * ArchSAI_Memset
 * "Sets the first num bytes of the block of memory pointed by ptr to the specified value
 * (*** value is interpreted as an unsigned char ***)"
 * http://www.cplusplus.com/reference/cstring/memset/
 *--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 * Memset
 *--------------------------------------------------------------------------*/
static inline void
ArchSAI_HostMemset(void *ptr, ARCHSAI_INT value, size_t num)
{
   memset(ptr, value, num);
}

static inline void
ArchSAI_DeviceMemset(void *ptr, ARCHSAI_INT value, size_t num)
{
#if defined(ARCHSAI_USING_CUDA)
   cudaMemset(ptr, value, num);
#elif defined(ARCHSAI_USING_HIP)
   hipMemset(ptr, value, num);
#endif
}

static inline void ArchSAI_UnifiedMemset(void *ptr, ARCHSAI_INT value, size_t num)
{
#if defined(ARCHSAI_USING_CUDA)
   cudaMemset(ptr, value, num);

#elif defined(ARCHSAI_USING_HIP)
   hipMemset(ptr, value, num);
#else
   ARCHSAI_UNUSED_VAR(ptr);
   ARCHSAI_UNUSED_VAR(value);
   ARCHSAI_UNUSED_VAR(num);
#endif
}

/*--------------------------------------------------------------------------
 * Memprefetch
 *--------------------------------------------------------------------------*/
static inline void ArchSAI_UnifiedMemPrefetch(void *ptr, size_t size, ArchSAI_MemoryLocation location)
{
   if (!size)
   {
      return;
   }

   ArchSAI_CheckMemoryLocation(ptr, ArchSAI_MEMORY_UNIFIED);

#if defined(ARCHSAI_USING_CUDA)
   if (location == ArchSAI_MEMORY_DEVICE)
   {
//       cudaMemPrefetchAsync(ptr, size, hypre_HandleDevice(hypre_handle()), hypre_HandleComputeStream(hypre_handle()));
   }
   else if (location == ArchSAI_MEMORY_HOST)
   {
//       cudaMemPrefetchAsync(ptr, size, cudaCpuDeviceId, hypre_HandleComputeStream(hypre_handle()));
   }

#elif defined(ARCHSAI_USING_HIP)
   ARCHSAI_UNUSED_VAR(ptr);
   ARCHSAI_UNUSED_VAR(size);
   ARCHSAI_UNUSED_VAR(location);
   // Not currently implemented for HIP, but leaving place holder
   /*
    *if (location == hypre_MEMORY_DEVICE)
    *{
    *  ARCHSAI_HIP_CALL( hipMemPrefetchAsync(ptr, size, hypre_HandleDevice(hypre_handle()),
    *                   hypre_HandleComputeStream(hypre_handle())) );
    *}
    *else if (location == hypre_MEMORY_HOST)
    *{
    *   ARCHSAI_CUDA_CALL( hipMemPrefetchAsync(ptr, size, cudaCpuDeviceId,
    *                    hypre_HandleComputeStream(hypre_handle())) );
    *}
    */
#else
   ARCHSAI_UNUSED_VAR(ptr);
   ARCHSAI_UNUSED_VAR(size);
   ARCHSAI_UNUSED_VAR(location);
#endif
}


/*--------------------------------------------------------------------------
 * C Malloc
 *--------------------------------------------------------------------------*/

static inline void * ArchSAI_DeviceMalloc(size_t size, ARCHSAI_INT zeroinit){
    void *ptr = NULL;
    #if defined ARCHSAI_USING_CUDA
        cudaMalloc(&ptr, size);
	#elif defined ARCHSAI_USING_HIP
		hipMalloc(&ptr, size);
    #endif
    return ptr;
}

static inline void * ArchSAI_HostMalloc(size_t size, ARCHSAI_INT zeroinit){
   void *ptr = NULL;
   if (zeroinit){ptr = calloc(size, 1);}
   else{ptr = malloc(size);}
   return ptr;
}

static inline void * ArchSAI_HostAlignedAlloc(ARCHSAI_INT align, size_t size){
    void *ptr = NULL;
    ptr = aligned_alloc(align, size);
    return ptr;
}

static inline void * ArchSAI_HostPinnedMalloc(size_t size, ARCHSAI_INT zeroinit)
{
	void *ptr = NULL;
	#if defined(ARCHSAI_USING_CUDA)
	cudaMallocHost(&ptr, size);
	#endif
	#if defined(ARCHSAI_USING_HIP)
	hipHostMalloc(&ptr, size);
	#endif
	if (ptr && zeroinit) ArchSAI_HostMemset(ptr, 0, size);
	return ptr;
}

static inline void *ArchSAI_UnifiedMalloc(size_t size, ARCHSAI_INT zeroinit)
{
	void *ptr = NULL;

	#if defined(ARCHSAI_USING_CUDA)
	cudaMallocManaged(&ptr, size, cudaMemAttachGlobal);
	#endif

	#if defined(ARCHSAI_USING_HIP)
	hipMallocManaged(&ptr, size, hipMemAttachGlobal);
	#endif

	/* prefecth to device */
	if (ptr)
	{
		ArchSAI_UnifiedMemPrefetch(ptr, size, ArchSAI_MEMORY_DEVICE);
	}

	if (ptr && zeroinit)
	{
		ArchSAI_UnifiedMemset(ptr, 0, size);
	}

   return ptr;
}

static inline void * ArchSAI_MAlloc_core(size_t size, ARCHSAI_INT zeroinit, ArchSAI_MemoryLocation location){
    if (size == 0){return NULL;}
    void *ptr = NULL;
    switch (location){
        case ArchSAI_MEMORY_HOST :
            ptr = ArchSAI_HostMalloc(size, zeroinit);
        break;
        case ArchSAI_MEMORY_DEVICE :
            ptr = ArchSAI_DeviceMalloc(size, zeroinit);
            break;
        case ArchSAI_MEMORY_UNIFIED :
            ptr = ArchSAI_UnifiedMalloc(size, zeroinit);
            break;
        case ArchSAI_MEMORY_HOST_PINNED :
            ptr = ArchSAI_HostPinnedMalloc(size, zeroinit);
            break;
        default :
            ArchSAI_WrongMemoryLocation();
    }

    if (!ptr){
        ArchSAI_OutOfMemory(size);
//         ArchSAI_MPI_Abort(ArchSAI_MPI_COMM_WORLD, -1);
    }

    return ptr;
}

static inline void * ArchSAI_AlignedAlloc_core(size_t count, size_t elt_size, ARCHSAI_INT align, ArchSAI_MemoryLocation location){

    ARCHSAI_INT size = (count / align + 1) * align * sizeof(elt_size);

    if ((size) == 0){return NULL;}
    void *ptr = NULL;
    switch (location){
        case ArchSAI_MEMORY_HOST :
            ptr = ArchSAI_HostAlignedAlloc(align, size);
        break;
        case ArchSAI_MEMORY_DEVICE :
            ptr = ArchSAI_DeviceMalloc(size, 0);
            break;
        case ArchSAI_MEMORY_UNIFIED :
            ptr = ArchSAI_UnifiedMalloc(size, 0);
            break;
        case ArchSAI_MEMORY_HOST_PINNED :
            ptr = ArchSAI_HostPinnedMalloc(size, 0);
            break;
        default :
            ArchSAI_WrongMemoryLocation();
    }

    if (!ptr){
        ArchSAI_OutOfMemory(size);
//         ArchSAI_MPI_Abort(ArchSAI_MPI_COMM_WORLD, -1);
    }

    return ptr;
}

void * _ArchSAI_MAlloc(size_t size, ArchSAI_MemoryLocation location){
   return ArchSAI_MAlloc_core(size, 0, location);
}

/*--------------------------------------------------------------------------*
 * ArchSAI_MAlloc, ArchSAI_CAlloc, ArchSAI_AlignedAlloc
 *--------------------------------------------------------------------------*/

void * ArchSAI_MAlloc(size_t size, ArchSAI_MemoryLocation location){return ArchSAI_MAlloc_core(size, 0, ArchSAI_GetActualMemLocation(location));}

void * ArchSAI_CAlloc( size_t count, size_t elt_size, ArchSAI_MemoryLocation location){return ArchSAI_MAlloc_core(count * elt_size, 1, ArchSAI_GetActualMemLocation(location));}

void * ArchSAI_AlignedAlloc( size_t count, size_t elt_size, ARCHSAI_INT align, ArchSAI_MemoryLocation location){return ArchSAI_AlignedAlloc_core(count, elt_size, align, ArchSAI_GetActualMemLocation(location));}



/*--------------------------------------------------------------------------*
 * ArchSAI_ReAlloc, ArchSAI_ReAlloc_v2
 *--------------------------------------------------------------------------*/

void * ArchSAI_ReAlloc(void *ptr, size_t size, ArchSAI_MemoryLocation location){
    if (size == 0){
        ArchSAI_Free(ptr, location);
        return NULL;
    }

    if (ptr == NULL){
        return ArchSAI_MAlloc(size, location);
    }

    if (ArchSAI_GetActualMemLocation(location) != ArchSAI_MEMORY_HOST){
        printf("ArchSAI_TReAlloc only works with ArchSAI_MEMORY_HOST; Use ArchSAI_TReAlloc_v2 instead!\n");
        assert(0);
        return NULL;
    }

    ptr = realloc(ptr, size);

    if (!ptr){
        ArchSAI_OutOfMemory(size);
    }

    return ptr;
}

void * ArchSAI_ReAlloc_v2(void *ptr, size_t old_size, size_t new_size, ArchSAI_MemoryLocation location){
    if (new_size == 0){
        ArchSAI_Free(ptr, location);
        return NULL;
    }

    if (ptr == NULL){
        return ArchSAI_MAlloc(new_size, location);
    }

    if (old_size == new_size){
        return ptr;
    }

    void *new_ptr = ArchSAI_MAlloc(new_size, location);
    size_t smaller_size = new_size > old_size ? old_size : new_size;
    ArchSAI_Memcpy(new_ptr, ptr, smaller_size, location, location);
    ArchSAI_Free(ptr, location);
    ptr = new_ptr;

    if (!ptr){
        ArchSAI_OutOfMemory(new_size);
    }

    return ptr;
}


/*--------------------------------------------------------------------------
 * Memcpy
 *--------------------------------------------------------------------------*/
static inline void ArchSAI_Memcpy_core(void *dst, void *src, size_t size, ArchSAI_MemoryLocation loc_dst, ArchSAI_MemoryLocation loc_src){

    if (dst == NULL || src == NULL){
        if (size){
            printf("ArchSAI_Memcpy warning: copy %ld bytes from %p to %p !\n", size, src, dst);
            assert(0);
        }

        return;
    }

    if (dst == src) return;

    if (size > 0){
        ArchSAI_CheckMemoryLocation(dst, loc_dst);
        ArchSAI_CheckMemoryLocation(src, loc_src);
    }

    /* Totally 4 x 4 = 16 cases */
    /*
    /* 4: Host   <-- Host, Host   <-- Pinned,
    *    Pinned <-- Host, Pinned <-- Pinned.
    */
    if ( loc_dst != ArchSAI_MEMORY_DEVICE && loc_dst != ArchSAI_MEMORY_UNIFIED && loc_src != ArchSAI_MEMORY_DEVICE && loc_src != ArchSAI_MEMORY_UNIFIED ){
        memcpy(dst, src, size);
        return;
    }


   /* 3: UVM <-- Device, Device <-- UVM, UVM <-- UVM */
   if ( (loc_dst == ArchSAI_MEMORY_UNIFIED && loc_src == ArchSAI_MEMORY_DEVICE)  ||
        (loc_dst == ArchSAI_MEMORY_DEVICE  && loc_src == ArchSAI_MEMORY_UNIFIED) ||
        (loc_dst == ArchSAI_MEMORY_UNIFIED && loc_src == ArchSAI_MEMORY_UNIFIED) )
   {
	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy(dst, src, size, cudaMemcpyDeviceToDevice);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyDeviceToDevice);
	#endif
      return;
   }


   /* 2: UVM <-- Host, UVM <-- Pinned */
   if (loc_dst == ArchSAI_MEMORY_UNIFIED)
   {
	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyHostToDevice);
	#endif
      return;
   }


   /* 2: Host <-- UVM, Pinned <-- UVM */
   if (loc_src == ArchSAI_MEMORY_UNIFIED)
   {
	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy(dst, src, size, cudaMemcpyDeviceToHost);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyDeviceToHost);
	#endif
		return;
   }


   /* 2: Device <-- Host, Device <-- Pinned */
   if ( loc_dst == ArchSAI_MEMORY_DEVICE && (loc_src == ArchSAI_MEMORY_HOST ||
                                           loc_src == ArchSAI_MEMORY_HOST_PINNED) )
   {
	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyHostToDevice);
	#endif
      return;
   }


   /* 2: Host <-- Device, Pinned <-- Device */
   if ( (loc_dst == ArchSAI_MEMORY_HOST || loc_dst == ArchSAI_MEMORY_HOST_PINNED) &&
        loc_src == ArchSAI_MEMORY_DEVICE )
   {

	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy( dst, src, size, cudaMemcpyDeviceToHost);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyDeviceToHost);
	#endif
      return;
   }


   /* 1: Device <-- Device */
   if (loc_dst == ArchSAI_MEMORY_DEVICE && loc_src == ArchSAI_MEMORY_DEVICE)
   {
	#if defined(ARCHSAI_USING_CUDA)
		cudaMemcpy(dst, src, size, cudaMemcpyDeviceToDevice);
	#endif

	#if defined(ARCHSAI_USING_HIP)
		hipMemcpy(dst, src, size, hipMemcpyDeviceToDevice);
	#endif
      return;
   }

    ArchSAI_WrongMemoryLocation();
}


/*--------------------------------------------------------------------------
 * ArchSAI_Memcpy
 *--------------------------------------------------------------------------*/

void ArchSAI_Memcpy(void *dst, void *src, size_t size, ArchSAI_MemoryLocation loc_dst, ArchSAI_MemoryLocation loc_src)
{
   ArchSAI_Memcpy_core( dst, src, size, ArchSAI_GetActualMemLocation(loc_dst),
                      ArchSAI_GetActualMemLocation(loc_src) );
}

void *
ArchSAI_Memset(void *ptr, ARCHSAI_INT value, size_t num, ArchSAI_MemoryLocation location)
{
   if (num == 0)
   {
      return ptr;
   }

   if (ptr == NULL)
   {
      if (num)
      {
         printf("ArchSAI_Memset warning: set values for %ld bytes at %p !\n", num, ptr);
      }
      return ptr;
   }

   ArchSAI_CheckMemoryLocation(ptr, ArchSAI_GetActualMemLocation(location));

   switch (ArchSAI_GetActualMemLocation(location))
   {
      case ArchSAI_MEMORY_HOST :
      case ArchSAI_MEMORY_HOST_PINNED :
         ArchSAI_HostMemset(ptr, value, num);
         break;
      case ArchSAI_MEMORY_DEVICE :
         ArchSAI_DeviceMemset(ptr, value, num);
         break;
      case ArchSAI_MEMORY_UNIFIED :
         ArchSAI_UnifiedMemset(ptr, value, num);
         break;
      default :
         ArchSAI_WrongMemoryLocation();
   }

   return ptr;
}



/*--------------------------------------------------------------------------
 * C Free
 *--------------------------------------------------------------------------*/

static inline void ArchSAI_HostFree(void *ptr){free(ptr);}
static inline void ArchSAI_DeviceFree(void *ptr){
    #if defined ARCHSAI_USING_CUDA
        cudaFree(ptr);
	#elif defined ARCHSAI_USING_AMD
		hipFree(ptr);
    #endif
}

static inline void ArchSAI_UnifiedFree(void *ptr){
#if defined(ARCHSAI_USING_CUDA)
   cudaFree(ptr);
#elif defined(ARCHSAI_USING_HIP)
   hipFree(ptr);
#else
   ARCHSAI_UNUSED_VAR(ptr);

#endif
}

static inline void ArchSAI_HostPinnedFree(void *ptr)
{
#if defined(ARCHSAI_USING_CUDA)
   cudaFreeHost(ptr);

#elif defined(ARCHSAI_USING_HIP)
   hipHostFree(ptr);
#else
   ARCHSAI_UNUSED_VAR(ptr);

#endif
}

static inline void ArchSAI_Free_core(void *ptr, ArchSAI_MemoryLocation location){
    if (!ptr){return;}
    ArchSAI_CheckMemoryLocation(ptr, location);
    switch (location){
        case ArchSAI_MEMORY_HOST :
            ArchSAI_HostFree(ptr);
        break;
          case ArchSAI_MEMORY_DEVICE :
             ArchSAI_DeviceFree(ptr);
             break;
          case ArchSAI_MEMORY_UNIFIED :
             ArchSAI_UnifiedFree(ptr);
             break;
          case ArchSAI_MEMORY_HOST_PINNED :
             ArchSAI_HostPinnedFree(ptr);
             break;
        default :
            ArchSAI_WrongMemoryLocation();
    }
}


/*--------------------------------------------------------------------------
 * ArchSAI_Free
 *--------------------------------------------------------------------------*/

void ArchSAI_Free(void *ptr, ArchSAI_MemoryLocation location){ArchSAI_Free_core(ptr, ArchSAI_GetActualMemLocation(location));}




/*--------------------------------------------------------------------------
 * Query the actual memory location pointed by ptr
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_GetPointerLocation(const void *ptr, ArchSAI_MemoryLocation *memory_location){
   ARCHSAI_INT ierr = 0;

	#if defined(ARCHSAI_USING_GPU)
		*memory_location = ArchSAI_MEMORY_UNDEFINED;
		#if defined(ARCHSAI_USING_CUDA)
			struct cudaPointerAttributes attr;
			#if (CUDART_VERSION >= 10000)
				#if (CUDART_VERSION >= 11000)
					ARCHSAI_CUDA_CALL( cudaPointerGetAttributes(&attr, ptr) );
				#else
					cudaError_t err = cudaPointerGetAttributes(&attr, ptr);
					if (err != cudaSuccess){
						ierr = 1;
						cudaGetLastError();
					}
				#endif

				if (attr.type == cudaMemoryTypeUnregistered) *memory_location = ArchSAI_MEMORY_HOST;
				else if (attr.type == cudaMemoryTypeHost) *memory_location = ArchSAI_MEMORY_HOST_PINNED;
				else if (attr.type == cudaMemoryTypeDevice) *memory_location = ArchSAI_MEMORY_DEVICE;
				else if (attr.type == cudaMemoryTypeManaged) *memory_location = ArchSAI_MEMORY_UNIFIED;

			#else
				cudaError_t err = cudaPointerGetAttributes(&attr, ptr);
				if (err != cudaSuccess){
					ierr = 1;
					cudaGetLastError();
					if (err == cudaErrorInvalidValue) *memory_location = ArchSAI_MEMORY_HOST;
				}
				else if (attr.isManaged) *memory_location = ArchSAI_MEMORY_UNIFIED;
				else if (attr.memoryType == cudaMemoryTypeDevice) *memory_location = ArchSAI_MEMORY_DEVICE;
				else if (attr.memoryType == cudaMemoryTypeHost) *memory_location = ArchSAI_MEMORY_HOST_PINNED;
			#endif // CUDART_VERSION >= 10000
		#endif // defined(ARCHSAI_USING_CUDA)

		#if defined(ARCHSAI_USING_HIP)
			struct hipPointerAttribute_t attr;
			*memory_location = ArchSAI_MEMORY_UNDEFINED;
			hipError_t err = hipPointerGetAttributes(&attr, ptr);
			if (err != hipSuccess){
				ierr = 1;
				hipGetLastError();
				if (err == hipErrorInvalidValue) *memory_location = ArchSAI_MEMORY_HOST;
			}
			else if (attr.isManaged) *memory_location = ArchSAI_MEMORY_UNIFIED;
			#if (HIP_VERSION_MAJOR >= 6)
				else if (attr.type == hipMemoryTypeDevice)
			#else // (HIP_VERSION_MAJOR < 6)
				else if (attr.memoryType == hipMemoryTypeDevice)
			#endif // (HIP_VERSION_MAJOR >= 6)
				{*memory_location = ArchSAI_MEMORY_DEVICE;}

			#if (HIP_VERSION_MAJOR >= 6)
				else if (attr.type == hipMemoryTypeHost)
			#else // (HIP_VERSION_MAJOR < 6)
				else if (attr.memoryType == hipMemoryTypeHost)
			#endif // (HIP_VERSION_MAJOR >= 6)
				{*memory_location = ArchSAI_MEMORY_HOST_PINNED;}
		#endif // defined(ARCHSAI_USING_HIP)


	#else /* #if defined(ARCHSAI_USING_GPU) */
	*memory_location = ArchSAI_MEMORY_HOST;
	//    ARCHSAI_UNUSED_VAR(ptr);
	#endif


   return ierr;
}





