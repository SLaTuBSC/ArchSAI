
#include <ArchSAI_Solvers.h>
#include <ArchSAI_Print.h>
#include <ArchSAI_GPU.h>
#include <ArchSAI_GPULinAlg.h>
#include <ArchSAI_GPUApplyPC.h>

ARCHSAI_VEC ArchSAI_PCG_gpu(ARCHSAI_SOLVER *PCG, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params){

    /* Sanity Checks */
	#ifndef ARCHSAI_USING_GPU
	ArchSAI_ParPrintf(A.comm, "Error: Can not execute GPU solver without GPU.\n");
	ArchSAI_Error(1);
	#elif defined ARCHSAI_USING_GPU

    /* ****************************************************************************************** */
    /* VARIABLES FOR PCG */

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	// CHECK AMOUNT OF GPUS AND ASSIGN GPU TO PROCESS

	ARCHSAI_INT deviceCount = 0;
	ARCHSAI_INT deviceId = 0;
	#if defined ARCHSAI_USING_CUDA
	cudaError_t error = cudaGetDeviceCount(&deviceCount);
	if (error != cudaSuccess) {
		ArchSAI_ParPrintf(A.comm, "Error: Number of MPI processes must match number of GPUs.\n");
		ArchSAI_Error(1);
	}

	error = cudaSetDevice(rank);
	error = cudaGetDevice(&deviceId);

// 	printf("%i %i\n", rank, deviceId);
	#elif defined ARCHSAI_USING_AMD
// 	cudaError_t error = cudaGetDeviceCount(&deviceCount);
// 	if (error != cudaSuccess) {
// 		ArchSAI_ParPrintf(A.comm, "Error: Number of MPI processes must match number of GPUs.\n");
// 		ArchSAI_Error(1);
// 	}
	#endif

	/* ****************************************************************************************** */
	// SEND MATRICES TO GPUs AND FORMAT THEM

	ArchSAI_MatToGPU(&A, params);

    if (PC.type == 0){}
    else if (PC.type == 1){
		ArchSAI_MatToGPU(&(M(PC)), params);
    }
    else if (PC.type == 2){
		ArchSAI_MatToGPU(&(M(PC)), params);
    }
    else if (PC.type == 3){
		ArchSAI_MatToGPU(&(G(PC)), params);
		ArchSAI_MatToGPU(&(Gt(PC)), params);
    }

    *PCG = ArchSAI_InitSolver(params, A, PC);

    // LOOP HANDLERS, VARIABLES & REQUIRED ARRAYS
    ARCHSAI_INT          rep = 0;
    ARCHSAI_INT         iter = 0;
    ARCHSAI_INT      i, j, k = 0;

    // To store norms of vectors
    ARCHSAI_DOUBLE    norm_r = 0.0;
    ARCHSAI_DOUBLE  norm_r_0 = 0.0;
    ARCHSAI_DOUBLE invnorm_r = 0.0;
    ARCHSAI_DOUBLE    norm_b = 0.0;

    // Other
    ARCHSAI_DOUBLE     d_new = 0.0;
    ARCHSAI_DOUBLE     d_old = 0.0;
    ARCHSAI_DOUBLE     d_div = 0.0;
    ARCHSAI_DOUBLE     alpha = 0.0;
    ARCHSAI_DOUBLE neg_alpha = 0.0;
    ARCHSAI_DOUBLE  den_norm = 0.0;
    ARCHSAI_DOUBLE   epsilon = 0.0;

    // Time measurement
    ARCHSAI_DOUBLE   reptime = 0.0;
    ARCHSAI_DOUBLE  itertime = 0.0;
    ARCHSAI_DOUBLE  besttime = 1E09;

    // ARRAYS
    ARCHSAI_VEC            x;
                       x.val = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.d_ncols,                64, ArchSAI_MEMORY_HOST);
                      x.size = A.d_ncols;


	ARCHSAI_VEC 		d_x,
						d_b,
						d_r,
						d_d,
						d_q,
						d_s,
						spmv;


						// ARA JA NO CAL QUE SIGUIN DIFERENTS

	ARCHSAI_INT vsizes = A.nrows;

	d_x = ArchSAI_InitGPUVec(vsizes);
	d_b = ArchSAI_InitGPUVec(vsizes);
	d_r = ArchSAI_InitGPUVec(vsizes);
	d_d = ArchSAI_InitGPUVec(vsizes);
	d_q = ArchSAI_InitGPUVec(vsizes);
	d_s = ArchSAI_InitGPUVec(vsizes);
	if (params.precond == 3){
	   spmv = ArchSAI_InitGPUVec(G(PC).d_nrows);
	}

	ArchSAI_Memcpy(d_b.val, b.val, b.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE, ArchSAI_MEMORY_HOST);

    /* ****************************************************************************************** */

	// GPU BUFFERS FOR ALL POSSIBLE SpMV OPERATIONS
	size_t buffer = 0, bmax = 0;

	bmax = (buffer = ArchSAI_GetSpMVBufferSize(A, d_x, d_r, fp_one, fp_zero) > bmax) ? buffer : bmax;
	bmax = (buffer = ArchSAI_GetSpMVBufferSize(A, d_d, d_q, fp_one, fp_zero) > bmax) ? buffer : bmax;

	if (params.precond > 0) {
		bmax = (buffer = ArchSAI_GetSpMVBufferSize(G(PC), d_r, d_d, fp_one, fp_zero) > bmax) ? buffer : bmax;
		bmax = (buffer = ArchSAI_GetSpMVBufferSize(G(PC), d_r, d_s, fp_one, fp_zero) > bmax) ? buffer : bmax;
	}

	if (params.precond == 3) {
		bmax = (buffer = ArchSAI_GetSpMVBufferSize(G(PC), d_r, spmv, fp_one, fp_zero) > bmax) ? buffer : bmax;
		bmax = (buffer = ArchSAI_GetSpMVBufferSize(Gt(PC), spmv, d_d, fp_one, fp_zero) > bmax) ? buffer : bmax;
		bmax = (buffer = ArchSAI_GetSpMVBufferSize(Gt(PC), spmv, d_s, fp_one, fp_zero) > bmax) ? buffer : bmax;
	}

	void* GPU_buffer = ArchSAI_CTAlloc(size_t, bmax, ArchSAI_MEMORY_DEVICE);

	ARCHSAI_GPU_BLASHANDLE blashandle;
	ArchSAI_CreateBLASHandle(&blashandle);

    /* ****************************************************************************************** */

    /* Begin USER-DEFINED repetitions of solver */

    while (rep < params.reps){

        // Reinitialize used vectors

		ArchSAI_Memset(d_r.val, 0, d_r.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE);
		ArchSAI_Memset(d_x.val, 0, d_x.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE);
		ArchSAI_Memset(d_d.val, 0, d_d.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE);
		ArchSAI_Memset(d_q.val, 0, d_q.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE);
		ArchSAI_Memset(d_s.val, 0, d_s.size * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_DEVICE);

       // SETUP

		cudaProfilerStart();

        // Check b and get norm_b
		norm_b = ArchSAI_Sqrt(ArchSAI_GPU_DOTP(blashandle, d_b.size, d_b.val, int_one, d_b.val, int_one, A.comm, nprocs));

        // Copy RHS to r to Compute Initial Residual and preconditioned residual d
		ArchSAI_GPU_Copy(blashandle, d_b.size, d_b.val, int_one, d_r.val, int_one);
		ArchSAI_GPU_PrecondSpMV(PC, d_r, d_d, spmv, GPU_buffer, nprocs);

		d_new = ArchSAI_GPU_DOTP(blashandle, d_d.size, d_d.val, int_one, d_r.val, int_one, A.comm, nprocs);

        // Check residual
        norm_r = ArchSAI_Sqrt(ArchSAI_GPU_DOTP(blashandle, d_r.size, d_r.val, int_one, d_r.val, int_one, A.comm, nprocs));
        norm_r_0 = norm_r;

        PCG->residual[rep][0] = norm_r;
        PCG->rel_res[rep][0] = norm_r / norm_b;

        // Set convergence tolerances
        den_norm = norm_r;
        epsilon = ArchSAI_Max(params.abs_tol, params.rel_tol * den_norm);

        /* Begin PCG Iterations */

        iter = 0;
        reptime = time_getWallclockSeconds();
        itertime = time_getWallclockSeconds();
        while (iter < params.imax){
            iter++;

            /* q = Ad */
			ArchSAI_GPU_CSRMV(fp_one, A, d_d, fp_zero, d_q, GPU_buffer, nprocs);
            /* alpha = d_new / dq */
            alpha = d_new / ArchSAI_GPU_DOTP(blashandle, d_d.size, d_d.val, int_one, d_q.val, int_one, A.comm, nprocs);
            /* x = x + alpha*d */
			ArchSAI_GPU_Axpy(blashandle, d_x.size, &alpha, d_d.val, int_one, d_x.val, int_one);

//             if (iter % 5 == 0){
//                 /* r = b - Ax */
// 				ArchSAI_GPU_Copy(blashandle, d_r.size, d_b.val, int_one, d_r.val, int_one);
// 				ArchSAI_GPU_CSRMV(fp_none, A, d_x, fp_one, d_r, GPU_buffer, nprocs);
//             }
//             else {
                /* r = r - alpha * q */
                neg_alpha = -alpha;
				ArchSAI_GPU_Axpy(blashandle, d_r.size, &neg_alpha, d_q.val, int_one, d_r.val, int_one);
//             }

            /* s = M-1 * r */
			ArchSAI_GPU_PrecondSpMV(PC, d_r, d_s, spmv, GPU_buffer, nprocs);

            d_old = d_new;
			d_new = ArchSAI_GPU_DOTP(blashandle, d_s.size, d_s.val, int_one, d_r.val, int_one, A.comm, nprocs);

            /* d = s + d (d_new/d_old) */
            d_div = d_new / d_old;
			ArchSAI_GPU_Scal(blashandle, d_d.size, &d_div, d_d.val, int_one);
			ArchSAI_GPU_Axpy(blashandle, d_d.size, &fp_one, d_s.val, int_one, d_d.val, int_one);

			norm_r = ArchSAI_Sqrt(ArchSAI_GPU_DOTP(blashandle, d_r.size, d_r.val, int_one, d_r.val, int_one, A.comm, nprocs));

            PCG->residual[rep][iter] = norm_r;
            PCG->rel_res[rep][iter] = norm_r / PCG->residual[rep][0];

            if (norm_r <= epsilon){
                PCG->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
                itertime = time_getWallclockSeconds();
                break;
            }

            PCG->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
            itertime = time_getWallclockSeconds();
        }

        PCG->time[rep] = time_getWallclockSeconds() - reptime;
        PCG->iter[rep] = iter;

        if (PCG->time[rep] < besttime){
            besttime = PCG->time[rep];
            PCG->bestrep = rep;
        }
        rep++;
    }

    MPI_Barrier(A.comm);

    // Free Memory

//     ArchSAI_TFree(d,          ArchSAI_MEMORY_HOST);
//     ArchSAI_TFree(q,          ArchSAI_MEMORY_HOST);
//     ArchSAI_TFree(s,          ArchSAI_MEMORY_HOST);
//     ArchSAI_TFree(r,          ArchSAI_MEMORY_HOST);
//     ArchSAI_TFree(tmp,        ArchSAI_MEMORY_HOST);

    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(A.comm, "\nRep %i: %i %lf\n", PCG->bestrep, PCG->iter[PCG->bestrep], PCG->time[PCG->bestrep]);
    #endif // VERBOSE

    return x;
	#endif
}

