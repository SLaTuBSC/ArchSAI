
 /*! \file ArchSAI_Vector.h
    \brief This file contains the Vector utilities and functions used by ArchSAI.

    ArchSAI utilities.
*/

 #ifndef ARCHSAI_VECtor_h_
    #define ARCHSAI_VECtor_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Memory.h>

    /*! \fn ARCHSAI_VEC ArchSAI_DAllocCreateVecfromArray(ARCHSAI_DOUBLE *readvec, ARCHSAI_INT size);
        \brief Allocates and copies an ARCHSAI_VEC from an array

        \param readvec Pointer to array storing data
        \param size Size of array
    */

    ARCHSAI_VEC ArchSAI_DAllocCreateVecfromArray(ARCHSAI_DOUBLE *readvec, ARCHSAI_INT size, ARCHSAI_INT gsize);


#endif // __ARCHSAI_VECtor_h__


