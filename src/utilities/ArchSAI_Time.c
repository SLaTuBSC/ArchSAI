
#include <ArchSAI_Time.h>

ArchSAI_TimingType *ArchSAI_Timing = NULL;

archsai_double time_getWallclockSeconds(void){

    #ifdef ARCHSAI_USING_MPI
        return (MPI_Wtime());
    #else
//         clock_t cl = clock();
//         return (((archsai_double) cl) / ((archsai_double) CLOCKS_PER_SEC));
        return (omp_get_wtime());
    #endif

}

archsai_double time_getCPUSeconds(void){
   clock_t cpuclock = clock();
   return (((archsai_double) (cpuclock)) / ((archsai_double) CLOCKS_PER_SEC));
}

archsai_double time_get_wallclock_seconds_(void)
{
   return (time_getWallclockSeconds());
}

archsai_double time_get_cpu_seconds_(void)
{
   return (time_getCPUSeconds());
}


#define ArchSAI_StartTiming() \
    ArchSAI_TimingWallCount -= time_getWallclockSeconds();\
    ArchSAI_TimingCPUCount -= time_getCPUSeconds()

#define ArchSAI_StopTiming() \
    ArchSAI_TimingWallCount += time_getWallclockSeconds();\
    ArchSAI_TimingCPUCount += time_getCPUSeconds()

#define ArchSAI_Timing_ref(index,field) ArchSAI_Timing->field

/*--------------------------------------------------------------------------
 * ArchSAI_InitializeTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_InitializeTiming( const char *name ) {

   ARCHSAI_INT      time_index;

   char         **old_name;
   archsai_double  *old_wall_time;
   archsai_double  *old_cpu_time;
   archsai_double  *old_flops;
   ARCHSAI_INT     *old_state;
   ARCHSAI_INT     *old_num_regs;

   ARCHSAI_INT      new_name;
   ARCHSAI_INT      i;

   /*-------------------------------------------------------
    * Allocate global TimingType structure if needed
    *-------------------------------------------------------*/

   if (ArchSAI_Timing == NULL)
   {
      ArchSAI_Timing = ArchSAI_CTAlloc(ArchSAI_TimingType,  1, ArchSAI_MEMORY_HOST);
   }

   /*-------------------------------------------------------
    * Check to see if name has already been registered
    *-------------------------------------------------------*/

   new_name = 1;
   for (i = 0; i < (ArchSAI_Timing_ref(threadid, size)); i++)
   {
      if (ArchSAI_TimingNumRegs(i) > 0)
      {
         if (strcmp(name, ArchSAI_TimingName(i)) == 0)
         {
            new_name = 0;
            time_index = i;
            ArchSAI_TimingNumRegs(time_index) ++;
            break;
         }
      }
   }

   if (new_name)
   {
      for (i = 0; i < ArchSAI_Timing_ref(threadid, size); i++)
      {
         if (ArchSAI_TimingNumRegs(i) == 0)
         {
            break;
         }
      }
      time_index = i;
   }

   /*-------------------------------------------------------
    * Register the new timing name
    *-------------------------------------------------------*/

   if (new_name)
   {
      if (time_index == (ArchSAI_Timing_ref(threadid, size)))
      {
         old_wall_time = (ArchSAI_Timing_ref(threadid, wall_time));
         old_cpu_time  = (ArchSAI_Timing_ref(threadid, cpu_time));
         old_flops     = (ArchSAI_Timing_ref(threadid, flops));
         old_name      = (ArchSAI_Timing_ref(threadid, name));
         old_state     = (ArchSAI_Timing_ref(threadid, state));
         old_num_regs  = (ArchSAI_Timing_ref(threadid, num_regs));

         (ArchSAI_Timing_ref(threadid, wall_time)) =
            ArchSAI_CTAlloc(archsai_double,  (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, cpu_time))  =
            ArchSAI_CTAlloc(archsai_double,  (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, flops))     =
            ArchSAI_CTAlloc(archsai_double,  (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, name))      =
            ArchSAI_CTAlloc(char *,  (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, state))     =
            ArchSAI_CTAlloc(ARCHSAI_INT,     (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, num_regs))  =
            ArchSAI_CTAlloc(ARCHSAI_INT,     (time_index + 1), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, size)) ++;

         for (i = 0; i < time_index; i++)
         {
            ArchSAI_TimingWallTime(i) = old_wall_time[i];
            ArchSAI_TimingCPUTime(i)  = old_cpu_time[i];
            ArchSAI_TimingFLOPS(i)    = old_flops[i];
            ArchSAI_TimingName(i)     = old_name[i];
            ArchSAI_TimingState(i)    = old_state[i];
            ArchSAI_TimingNumRegs(i)  = old_num_regs[i];
         }

         ArchSAI_TFree(old_wall_time, ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(old_cpu_time, ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(old_flops, ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(old_name, ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(old_state, ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(old_num_regs, ArchSAI_MEMORY_HOST);
      }

      ArchSAI_TimingName(time_index) = ArchSAI_CTAlloc(char,  80, ArchSAI_MEMORY_HOST);
      strncpy(ArchSAI_TimingName(time_index), name, 79);
      ArchSAI_TimingState(time_index)   = 0;
      ArchSAI_TimingNumRegs(time_index) = 1;
      (ArchSAI_Timing_ref(threadid, num_names)) ++;
   }

   return time_index;
}


/*--------------------------------------------------------------------------
 * ArchSAI_FinalizeTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_FinalizeTiming( ARCHSAI_INT time_index ) {
   ARCHSAI_INT  ierr = 0;
   ARCHSAI_INT  i;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   if (time_index < (ArchSAI_Timing_ref(threadid, size)))
   {
      if (ArchSAI_TimingNumRegs(time_index) > 0)
      {
         ArchSAI_TimingNumRegs(time_index) --;
      }

      if (ArchSAI_TimingNumRegs(time_index) == 0)
      {
         ArchSAI_TFree(ArchSAI_TimingName(time_index), ArchSAI_MEMORY_HOST);
         (ArchSAI_Timing_ref(threadid, num_names)) --;
      }
   }

   if ((ArchSAI_Timing -> num_names) == 0)
   {
      for (i = 0; i < (ArchSAI_Timing -> size); i++)
      {
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  wall_time), ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  cpu_time), ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  flops), ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  name), ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  state), ArchSAI_MEMORY_HOST);
         ArchSAI_TFree(ArchSAI_Timing_ref(i,  num_regs), ArchSAI_MEMORY_HOST);
      }

      ArchSAI_TFree(ArchSAI_Timing, ArchSAI_MEMORY_HOST);
      ArchSAI_Timing = NULL;
   }

   return ierr;
}

ARCHSAI_INT ArchSAI_FinalizeAllTimings( void ) {
   ARCHSAI_INT time_index, ierr = 0;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   ARCHSAI_INT size = ArchSAI_Timing_ref(threadid, size);

   for (time_index = 0; time_index < size; time_index++)
   {
      ierr += ArchSAI_FinalizeTiming(time_index);
   }

   return ierr;
}


/*--------------------------------------------------------------------------
 * ArchSAI_IncFLOPCount
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_IncFLOPCount( ARCHSAI_BIGINT inc )
{
   ARCHSAI_INT  ierr = 0;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   ArchSAI_TimingFLOPCount += (archsai_double) (inc);

   return ierr;
}



/*--------------------------------------------------------------------------
 * ArchSAI_BeginTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_BeginTiming( ARCHSAI_INT time_index ) {
   ARCHSAI_INT  ierr = 0;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   if (ArchSAI_TimingState(time_index) == 0)
   {
      ArchSAI_StopTiming();
      ArchSAI_TimingWallTime(time_index) -= ArchSAI_TimingWallCount;
      ArchSAI_TimingCPUTime(time_index)  -= ArchSAI_TimingCPUCount;
      ArchSAI_TimingFLOPS(time_index)    -= ArchSAI_TimingFLOPCount;

      ArchSAI_StartTiming();
   }
   ArchSAI_TimingState(time_index) ++;

   return ierr;
}


/*--------------------------------------------------------------------------
 * ArchSAI_EndTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT ArchSAI_EndTiming( ARCHSAI_INT time_index ) {
   ARCHSAI_INT  ierr = 0;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   ArchSAI_TimingState(time_index) --;
   if (ArchSAI_TimingState(time_index) == 0)
   {
#if defined(ArchSAI_USING_GPU)
      ArchSAI_Handle *ArchSAI_handle_ = ArchSAI_handle();
      if (ArchSAI_HandleDefaultExecPolicy(ArchSAI_handle_) == ArchSAI_EXEC_DEVICE)
      {
         ArchSAI_SyncCudaDevice(ArchSAI_handle_);
      }
#endif
      ArchSAI_StopTiming();
      ArchSAI_TimingWallTime(time_index) += ArchSAI_TimingWallCount;
      ArchSAI_TimingCPUTime(time_index)  += ArchSAI_TimingCPUCount;
      ArchSAI_TimingFLOPS(time_index)    += ArchSAI_TimingFLOPCount;
      ArchSAI_StartTiming();
   }

   return ierr;
}


/*--------------------------------------------------------------------------
 * ArchSAI_ClearTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT
ArchSAI_ClearTiming( void )
{
   ARCHSAI_INT  ierr = 0;
   ARCHSAI_INT  i;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   for (i = 0; i < (ArchSAI_Timing_ref(threadid, size)); i++)
   {
      ArchSAI_TimingWallTime(i) = 0.0;
      ArchSAI_TimingCPUTime(i)  = 0.0;
      ArchSAI_TimingFLOPS(i)    = 0.0;
   }

   return ierr;
}


/*--------------------------------------------------------------------------
 * ArchSAI_PrintTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT
ArchSAI_PrintTiming( const char     *heading,
                   MPI_Comm        comm  )
{
   ARCHSAI_INT  ierr = 0;

   archsai_double  local_wall_time;
   archsai_double  local_cpu_time;
   archsai_double  wall_time;
   archsai_double  cpu_time;
   archsai_double  wall_mflops;
   archsai_double  cpu_mflops;

   ARCHSAI_INT     i;
   ARCHSAI_INT     myrank;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   MPI_Comm_rank(comm, &myrank );

   /* print heading */
   if (myrank == 0)
   {
//       ArchSAI_printf("=============================================\n");
      printf("%s:\n\n", heading);
//       ArchSAI_printf("=============================================\n");
   }

   for (i = 0; i < (ArchSAI_Timing -> size); i++)
   {
      if (ArchSAI_TimingNumRegs(i) > 0)
      {
         local_wall_time = ArchSAI_TimingWallTime(i);
         local_cpu_time  = ArchSAI_TimingCPUTime(i);
         MPI_Allreduce(&local_wall_time, &wall_time, 1,
                             MPI_DOUBLE, MPI_MAX, comm);
         MPI_Allreduce(&local_cpu_time, &cpu_time, 1,
                             MPI_DOUBLE, MPI_MAX, comm);

         if (myrank == 0)
         {
            printf("%s:\n", ArchSAI_TimingName(i));

            /* print wall clock info */
            printf("  wall clock time = %f seconds\n", wall_time);
            if (wall_time)
            {
               wall_mflops = ArchSAI_TimingFLOPS(i) / wall_time / 1.0E6;
            }
            else
            {
               wall_mflops = 0.0;
            }
            printf("  wall MFLOPS     = %f\n", wall_mflops);

            /* print CPU clock info */
            printf("  cpu clock time  = %f seconds\n", cpu_time);
            if (cpu_time)
            {
               cpu_mflops = ArchSAI_TimingFLOPS(i) / cpu_time / 1.0E6;
            }
            else
            {
               cpu_mflops = 0.0;
            }
            printf("  cpu MFLOPS      = %f\n\n", cpu_mflops);
         }
      }
   }

   return ierr;
}


/*--------------------------------------------------------------------------
 * ArchSAI_GetTiming
 *--------------------------------------------------------------------------*/

ARCHSAI_INT
ArchSAI_GetTiming( const char     *heading,
                 archsai_double     *wall_time_ptr,
                 MPI_Comm        comm  )
{
   ARCHSAI_INT  ierr = 0;

   archsai_double  local_wall_time;
   archsai_double  wall_time;

   ARCHSAI_INT     i;
   ARCHSAI_INT     myrank;

   if (ArchSAI_Timing == NULL)
   {
      return ierr;
   }

   MPI_Comm_rank(comm, &myrank );

   /* print heading */
   if (myrank == 0)
   {
//       ArchSAI_printf("=============================================\n");
      printf("%s:\n", heading);
//       ArchSAI_printf("=============================================\n");
   }

   for (i = 0; i < (ArchSAI_Timing -> size); i++)
   {
      if (ArchSAI_TimingNumRegs(i) > 0)
      {
         local_wall_time = ArchSAI_TimingWallTime(i);
         MPI_Allreduce(&local_wall_time, &wall_time, 1,
                             MPI_DOUBLE, MPI_MAX, comm);

         if (myrank == 0)
         {
            printf("%s:\n", ArchSAI_TimingName(i));

            /* print wall clock info */
            printf("  wall clock time = %f seconds\n", wall_time);
         }
      }
   }

   *wall_time_ptr = wall_time;
   return ierr;
}
